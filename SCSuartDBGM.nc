
/******************************************************************************/
/*                                                                            */
/*                    UART Communication Components in TinyOS                 */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/*                            Author : Changsu Suh                            */
/*                            {scs}@hanback.co.kr                             */
/*                                                                            */
/*                      Copyright (c) HANBACK ELECTRONICS                     */
/*                             All rights reserved.                           */
/*                                                                            */
/*                            http://www.hanback.com                          */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*============================================================================*/
/* Permission to use, copy, modify, and distribute this software and its      */
/* documentation are reserved by above authors and Hanback electronics.       */
/* The above copyright notice and authors must be described in this software. */
/*============================================================================*/
/*                                                                            */
/******************************************************************************/


module SCSuartDBGM {
  provides {
    interface SCSuartDBG;
    interface SCSuartDBGRecv;
    interface StdControl;
  } uses {
    interface HPLUART;
  }
}
implementation
{
  #define IDLE_UART	0
  #define BUSY_UART	1
  #define buff_len	128
  bool state;

  uint8_t str_buff[buff_len];
  uint16_t buff_start;
  uint16_t buff_end;
  
  command result_t StdControl.init() {
    atomic 
    {	
	state = IDLE_UART;
	buff_start = 0;
	buff_end = 0;
    }
    return SUCCESS;
  }

  command result_t StdControl.start() {
    return call HPLUART.init();
  }

  command result_t StdControl.stop() {
      
    return call HPLUART.stop();
  }

  async event result_t HPLUART.get(uint8_t data) {
    signal SCSuartDBGRecv.UARTRecv (data);
    return SUCCESS;
  }

  async event result_t HPLUART.putDone() {
    atomic {
	    buff_start++;
	    
	    if (buff_start>buff_len-1) 
	    	buff_start=0;
	    
		if (buff_end > buff_len-1)
			buff_end = 0;
					
	    if (buff_start != buff_end)
	    {
		call HPLUART.put(str_buff[buff_start]);
	  
	    }else{
		state = IDLE_UART;
	    }
    }
    return SUCCESS;
  }

  async command void SCSuartDBG.UARTSend (uint8_t* data, uint8_t len)
  {
	uint8_t i;
	atomic {
		if (buff_start>buff_len-1) 
	    	buff_start=0;
			    
		for (i=0 ; i<len ; i++)
		{
			if (buff_end > buff_len-1)
				buff_end = 0;
			str_buff[buff_end] = data[i];
			buff_end++;
		}
		
		if ( state == IDLE_UART)
		{
			state = BUSY_UART;
			call HPLUART.put(str_buff[buff_start]);
		}
	}
  }

  default async event void SCSuartDBGRecv.UARTRecv (uint8_t recv_Char){ ; }

}

