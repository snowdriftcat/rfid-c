includes RFID_Control;
configuration testRFID { }
implementation
{
  components Main, testRFIDM
           , TimerC
           , LedsC
	   , RFID_ControlC
	   , SCSuartDBGC
	   , GenericComm;

  Main.StdControl -> testRFIDM;
  Main.StdControl -> TimerC;
  
  testRFIDM.Timer -> TimerC.Timer[unique("Timer")];
  testRFIDM.Leds -> LedsC;
  testRFIDM.RFID_Control -> RFID_ControlC;
  testRFIDM.SCSuartSTD -> SCSuartDBGC;
  testRFIDM.SCSuartDBG -> SCSuartDBGC;
  testRFIDM.SCSuartDBGRecv -> SCSuartDBGC;
  testRFIDM.CommControl -> GenericComm;
  testRFIDM.ReceiveMsg -> GenericComm.ReceiveMsg[7];
  testRFIDM.SendMsg -> GenericComm.SendMsg[7];
}
