
module testRFIDM {
  provides interface StdControl;
  uses {
    interface Timer;
    interface Leds;
    interface RFID_Control;
    interface SCSuartDBG;
    interface StdControl as SCSuartSTD;
    interface StdControl as CommControl;
    interface SendMsg;
    interface ReceiveMsg;
    interface SCSuartDBGRecv;
  }

}implementation{
  void Control_RFID (uint8_t comm, uint8_t block, uint8_t* wbuff);
  char OutputUartMsg[64];
  char Inc_Flag;
  TOS_Msg Tos_Data;

  command result_t StdControl.init() {
    call Leds.init();
    call SCSuartSTD.init();
    call CommControl.init();

    atomic
    {
	outp(0,UBRR1H); 
	outp(23, UBRR1L);

	// Set UART double speed
	outp((1<<U2X),UCSR1A);

	// Set frame format: 8 data-bits, 1 stop-bit
	outp(((1 << UCSZ1) | (1 << UCSZ0)) , UCSR1C);

	// Enable reciever and transmitter and their interrupts
	outp(((1 << RXCIE) | (1 << TXCIE) | (1 << RXEN) | (1 << TXEN)) ,UCSR1B);

	Inc_Flag = 1;
    }

    return SUCCESS;
  }

  command result_t StdControl.start() {
    call SCSuartSTD.start();
    call CommControl.start();
    //call Timer.start(TIMER_REPEAT, 1000);
    return SUCCESS;
  }

  command result_t StdControl.stop() {
    call SCSuartSTD.stop();
    call Timer.stop();
    return SUCCESS;
  }

  event result_t Timer.fired() {
    call Leds.redToggle();
    return SUCCESS;
  }

  event result_t SendMsg.sendDone(TOS_MsgPtr sent, result_t success) {
    return SUCCESS;
  }

  event TOS_MsgPtr ReceiveMsg.receive(TOS_MsgPtr m) {
    if(m->addr == 0)
    {
	struct RFID_COMM_MSG *pack = (struct RFID_COMM_MSG*)m->data;
	call Leds.greenToggle();
	sprintf(OutputUartMsg, "Recv Packet from PC(C:%d, B:%d)\r\n", pack->comm, pack->block);
	call SCSuartDBG.UARTSend(OutputUartMsg, strlen(OutputUartMsg));
	Control_RFID(pack->comm, pack->block, pack->wbuff);
    }
    return m;
  }

  void Control_RFID(uint8_t comm, uint8_t block, uint8_t* buff) {
    if (comm == 1) {
	call RFID_Control.GetID_14443A();

    }else if (comm == 2) {
	call RFID_Control.GetID_15693 ();

    }else if (comm == 3) {
	call RFID_Control.RData_15693 (block); 

    }else if (comm == 4) {
	call RFID_Control.WData_15693 (block, buff, 4);
    }
  }

  #define RecvBuffSize 8
  uint8_t RecvBuff[RecvBuffSize];
  uint8_t recv_num = 0;
  bool start_flag = 0;

  async event void SCSuartDBGRecv.UARTRecv (uint8_t recv_Char)
  {
    atomic {
	    if(recv_Char == 0x7E && start_flag ==0)
	    {
		recv_num = 0;
		start_flag = 1;

	    } else if (recv_num < RecvBuffSize && start_flag ==1) {
		RecvBuff[recv_num] = recv_Char;
		recv_num++;

		if (recv_num==RecvBuffSize) {
			struct RFID_COMM_MSG *pack = (struct RFID_COMM_MSG*) RecvBuff;
			start_flag = 0;
			call Leds.greenToggle();
			//sprintf(OutputUartMsg, "Recv Packet from PC(C:%d, B:%d)\r\n", pack->comm, pack->block);
			//call SCSuartDBG.UARTSend(OutputUartMsg, strlen(OutputUartMsg));
			Control_RFID(pack->comm, pack->block, pack->wbuff);
		}
	    }
    }
  }

  async event void RFID_Control.GetID_14443A_Done(char status, uint8_t *buff, char size) {
	//*
	if(status == 0){
		char i;

		sprintf(OutputUartMsg, "Recv 14443A ID: [");
		call SCSuartDBG.UARTSend(OutputUartMsg, strlen(OutputUartMsg));

		for (i=0 ; i<5 ; i++){
			sprintf(OutputUartMsg, "%X ", buff[i]);
			call SCSuartDBG.UARTSend(OutputUartMsg, strlen(OutputUartMsg));
		}

		sprintf(OutputUartMsg, "]\r\n");
		call SCSuartDBG.UARTSend(OutputUartMsg, strlen(OutputUartMsg));

	}else{
		sprintf(OutputUartMsg, "14443A GetID Error: %d\r\n", status);
		call SCSuartDBG.UARTSend(OutputUartMsg, strlen(OutputUartMsg));
	}
	//*/
  }

  async event void RFID_Control.GetID_15693_Done (char status, uint8_t *buff, char size){
	//*
	if(status == 0){
		char i, j;
		for(j = 0; j<size ; j++)
		{
			sprintf(OutputUartMsg, "Recv 15693 ID: [");
			call SCSuartDBG.UARTSend(OutputUartMsg, strlen(OutputUartMsg));

			for (i=j*8 ; i<(j*8+8) ; i++){
				sprintf(OutputUartMsg, "%X ", buff[i]);
				call SCSuartDBG.UARTSend(OutputUartMsg, strlen(OutputUartMsg));
			}

			sprintf(OutputUartMsg, "]\r\n");
			call SCSuartDBG.UARTSend(OutputUartMsg, strlen(OutputUartMsg));
		}
	}else{
		sprintf(OutputUartMsg, "15693 GetID Error: %d\r\n", status);
		call SCSuartDBG.UARTSend(OutputUartMsg, strlen(OutputUartMsg));
	}
	//*/
  }

  async event void RFID_Control.RData_15693_Done (char status, uint8_t *buff, char size){
	//*
  	if(status == 0){
		char i;
		
		sprintf(OutputUartMsg, "Read data from 15693: Data[");
		call SCSuartDBG.UARTSend(OutputUartMsg, strlen(OutputUartMsg));

		for (i=1 ; i<5 ; i++){
			sprintf(OutputUartMsg, "%X ", buff[i]);
			call SCSuartDBG.UARTSend(OutputUartMsg, strlen(OutputUartMsg));
		}

		sprintf(OutputUartMsg, "]\r\n");
		call SCSuartDBG.UARTSend(OutputUartMsg, strlen(OutputUartMsg));
		
	}else{
		sprintf(OutputUartMsg, "Recv RData 15693 Error: %d\r\n", status);
		call SCSuartDBG.UARTSend(OutputUartMsg, strlen(OutputUartMsg));
	}
	//*/
  }

  async event void RFID_Control.WData_15693_Done (char status){
	//*
  	if(status == 0){
		sprintf(OutputUartMsg, "Write data to 15693 SUCCESS!!!\r\n");
		call SCSuartDBG.UARTSend(OutputUartMsg, strlen(OutputUartMsg));
	}else{
		sprintf(OutputUartMsg, "Write data to 15693 Error: %d\r\n", status);
		call SCSuartDBG.UARTSend(OutputUartMsg, strlen(OutputUartMsg));
	}
	//*/
  }

}
