
/******************************************************************************/
/*                                                                            */
/*                    UART Communication Components in TinyOS                 */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/*                            Author : Changsu Suh                            */
/*                            {scs}@hanback.co.kr                             */
/*                                                                            */
/*                      Copyright (c) HANBACK ELECTRONICS                     */
/*                             All rights reserved.                           */
/*                                                                            */
/*                            http://www.hanback.com                          */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/*============================================================================*/
/* Permission to use, copy, modify, and distribute this software and its      */
/* documentation are reserved by above authors and Hanback electronics.       */
/* The above copyright notice and authors must be described in this software. */
/*============================================================================*/
/*                                                                            */
/******************************************************************************/


configuration SCSuartDBGC { 
  provides interface SCSuartDBG;
  provides interface SCSuartDBGRecv;
  provides interface StdControl;
}

implementation
{
  components SCSuartDBGM, HPLUARTC;

  SCSuartDBG = SCSuartDBGM;
  SCSuartDBGRecv = SCSuartDBGM;
  StdControl = SCSuartDBGM;
  SCSuartDBGM.HPLUART -> HPLUARTC;

}
