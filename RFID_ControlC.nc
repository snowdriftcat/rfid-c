includes RFID_Control;
configuration RFID_ControlC { 

  provides interface RFID_Control;

} implementation {

  components RFID_ControlM
           , HPLUART1M
	   , SCSuartDBGC;

  RFID_Control = RFID_ControlM;

  RFID_ControlM.UART -> HPLUART1M.UART;
  RFID_ControlM.SCSuartDBG -> SCSuartDBGC;

}
