#define dbg(mode, format, ...) ((void)0)
#define dbg_clear(mode, format, ...) ((void)0)
#define dbg_active(mode) 0
# 60 "/usr/local/avr/include/inttypes.h"
typedef signed char int8_t;




typedef unsigned char uint8_t;
# 83 "/usr/local/avr/include/inttypes.h" 3
typedef int int16_t;




typedef unsigned int uint16_t;










typedef long int32_t;




typedef unsigned long uint32_t;
#line 117
typedef long long int64_t;




typedef unsigned long long uint64_t;
#line 134
typedef int16_t intptr_t;




typedef uint16_t uintptr_t;
# 213 "/usr/local/lib/gcc-lib/avr/3.3-tinyos/include/stddef.h" 3
typedef unsigned int size_t;
# 72 "/usr/local/avr/include/string.h"
extern size_t __attribute((__pure__)) strlen(const char *);
# 325 "/usr/local/lib/gcc-lib/avr/3.3-tinyos/include/stddef.h" 3
typedef int wchar_t;
# 60 "/usr/local/avr/include/stdlib.h"
typedef struct __nesc_unnamed4242 {
  int quot;
  int rem;
} div_t;


typedef struct __nesc_unnamed4243 {
  long quot;
  long rem;
} ldiv_t;


typedef int (*__compar_fn_t)(const void *, const void *);
# 151 "/usr/local/lib/gcc-lib/avr/3.3-tinyos/include/stddef.h" 3
typedef int ptrdiff_t;
# 91 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/system/tos.h"
typedef unsigned char bool;






enum __nesc_unnamed4244 {
  FALSE = 0, 
  TRUE = 1
};

uint16_t TOS_LOCAL_ADDRESS = 1;

enum __nesc_unnamed4245 {
  FAIL = 0, 
  SUCCESS = 1
};
static inline 

uint8_t rcombine(uint8_t r1, uint8_t r2);
typedef uint8_t  result_t;
static inline 






result_t rcombine(result_t r1, result_t r2);
static inline 
#line 133
result_t rcombine4(result_t r1, result_t r2, result_t r3, 
result_t r4);





enum __nesc_unnamed4246 {
  NULL = 0x0
};
# 81 "/usr/local/avr/include/avr/pgmspace.h"
typedef void __attribute((__progmem__)) prog_void;
typedef char __attribute((__progmem__)) prog_char;
typedef unsigned char __attribute((__progmem__)) prog_uchar;
typedef int __attribute((__progmem__)) prog_int;
typedef long __attribute((__progmem__)) prog_long;
typedef long long __attribute((__progmem__)) prog_long_long;
# 124 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/avrmote/avrhardware.h"
enum __nesc_unnamed4247 {
  TOSH_period16 = 0x00, 
  TOSH_period32 = 0x01, 
  TOSH_period64 = 0x02, 
  TOSH_period128 = 0x03, 
  TOSH_period256 = 0x04, 
  TOSH_period512 = 0x05, 
  TOSH_period1024 = 0x06, 
  TOSH_period2048 = 0x07
};
static inline 
void TOSH_wait(void);
static inline 




void TOSH_sleep(void);









typedef uint8_t __nesc_atomic_t;

__inline __nesc_atomic_t  __nesc_atomic_start(void );






__inline void  __nesc_atomic_end(__nesc_atomic_t oldSreg);
static 



__inline void __nesc_enable_interrupt(void);
# 235 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/CC2420Const.h"
enum __nesc_unnamed4248 {
  CP_MAIN = 0, 
  CP_MDMCTRL0, 
  CP_MDMCTRL1, 
  CP_RSSI, 
  CP_SYNCWORD, 
  CP_TXCTRL, 
  CP_RXCTRL0, 
  CP_RXCTRL1, 
  CP_FSCTRL, 
  CP_SECCTRL0, 
  CP_SECCTRL1, 
  CP_BATTMON, 
  CP_IOCFG0, 
  CP_IOCFG1
};
static 
# 101 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/hardware.h"
void __inline TOSH_uwait(int u_sec);
#line 117
static __inline void TOSH_SET_RED_LED_PIN(void);
#line 117
static __inline void TOSH_CLR_RED_LED_PIN(void);
#line 117
static __inline void TOSH_MAKE_RED_LED_OUTPUT(void);
static __inline void TOSH_SET_GREEN_LED_PIN(void);
#line 118
static __inline void TOSH_CLR_GREEN_LED_PIN(void);
#line 118
static __inline void TOSH_MAKE_GREEN_LED_OUTPUT(void);
static __inline void TOSH_SET_YELLOW_LED_PIN(void);
#line 119
static __inline void TOSH_MAKE_YELLOW_LED_OUTPUT(void);










static __inline void TOSH_CLR_SERIAL_ID_PIN(void);
#line 130
static __inline void TOSH_MAKE_SERIAL_ID_INPUT(void);
#line 152
static __inline void TOSH_SET_CC_RSTN_PIN(void);
#line 152
static __inline void TOSH_CLR_CC_RSTN_PIN(void);
#line 152
static __inline void TOSH_MAKE_CC_RSTN_OUTPUT(void);
static __inline void TOSH_SET_CC_VREN_PIN(void);
#line 153
static __inline void TOSH_MAKE_CC_VREN_OUTPUT(void);

static __inline void TOSH_MAKE_CC_FIFOP1_INPUT(void);

static __inline void TOSH_MAKE_CC_CCA_INPUT(void);
static __inline int TOSH_READ_CC_SFD_PIN(void);
#line 158
static __inline void TOSH_MAKE_CC_SFD_INPUT(void);
static __inline void TOSH_SET_CC_CS_PIN(void);
#line 159
static __inline void TOSH_CLR_CC_CS_PIN(void);
#line 159
static __inline void TOSH_MAKE_CC_CS_OUTPUT(void);
#line 159
static __inline void TOSH_MAKE_CC_CS_INPUT(void);
static __inline int TOSH_READ_CC_FIFO_PIN(void);
#line 160
static __inline void TOSH_MAKE_CC_FIFO_INPUT(void);
static __inline int TOSH_READ_RADIO_CCA_PIN(void);
#line 161
static __inline void TOSH_MAKE_RADIO_CCA_INPUT(void);
#line 189
static __inline void TOSH_SET_FLASH_SELECT_PIN(void);
#line 189
static __inline void TOSH_MAKE_FLASH_SELECT_OUTPUT(void);
static __inline void TOSH_MAKE_FLASH_CLK_OUTPUT(void);
static __inline void TOSH_MAKE_FLASH_OUT_OUTPUT(void);










static __inline void TOSH_MAKE_MOSI_OUTPUT(void);
static __inline void TOSH_MAKE_MISO_INPUT(void);

static __inline void TOSH_MAKE_SPI_SCK_OUTPUT(void);






static __inline void TOSH_MAKE_PW0_OUTPUT(void);
static __inline void TOSH_MAKE_PW1_OUTPUT(void);
static __inline void TOSH_MAKE_PW2_OUTPUT(void);
static __inline void TOSH_MAKE_PW3_OUTPUT(void);
static __inline void TOSH_MAKE_PW4_OUTPUT(void);
static __inline void TOSH_MAKE_PW5_OUTPUT(void);
static __inline void TOSH_MAKE_PW6_OUTPUT(void);
static __inline void TOSH_MAKE_PW7_OUTPUT(void);
#line 242
enum __nesc_unnamed4249 {

  TOSH_HUMIDITY_ADDR = 5, 
  TOSH_HUMIDTEMP_ADDR = 3, 
  TOSH_HUMIDITY_RESET = 0x1E
};

enum __nesc_unnamed4250 {

  INT_LOWLEVEL = 0, 
  INT_FALLING_EDGE = 2, 
  INT_RISING_EDGE = 3
};
static inline 
#line 268
void TOSH_SET_PIN_DIRECTIONS(void );
#line 324
enum __nesc_unnamed4251 {
  TOSH_ADC_PORTMAPSIZE = 12
};

enum __nesc_unnamed4252 {


  TOSH_ACTUAL_VOLTAGE_PORT = 7, 
  TOSH_ACTUAL_BANDGAP_PORT = 30, 
  TOSH_ACTUAL_GND_PORT = 31
};

enum __nesc_unnamed4253 {



  TOS_ADC_VOLTAGE_PORT = 7, 
  TOS_ADC_BANDGAP_PORT = 10, 
  TOS_ADC_GND_PORT = 11
};
# 54 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/types/dbg_modes.h"
typedef long long TOS_dbg_mode;



enum __nesc_unnamed4254 {
  DBG_ALL = ~0ULL, 


  DBG_BOOT = 1ULL << 0, 
  DBG_CLOCK = 1ULL << 1, 
  DBG_TASK = 1ULL << 2, 
  DBG_SCHED = 1ULL << 3, 
  DBG_SENSOR = 1ULL << 4, 
  DBG_LED = 1ULL << 5, 
  DBG_CRYPTO = 1ULL << 6, 


  DBG_ROUTE = 1ULL << 7, 
  DBG_AM = 1ULL << 8, 
  DBG_CRC = 1ULL << 9, 
  DBG_PACKET = 1ULL << 10, 
  DBG_ENCODE = 1ULL << 11, 
  DBG_RADIO = 1ULL << 12, 


  DBG_LOG = 1ULL << 13, 
  DBG_ADC = 1ULL << 14, 
  DBG_I2C = 1ULL << 15, 
  DBG_UART = 1ULL << 16, 
  DBG_PROG = 1ULL << 17, 
  DBG_SOUNDER = 1ULL << 18, 
  DBG_TIME = 1ULL << 19, 




  DBG_SIM = 1ULL << 21, 
  DBG_QUEUE = 1ULL << 22, 
  DBG_SIMRADIO = 1ULL << 23, 
  DBG_HARD = 1ULL << 24, 
  DBG_MEM = 1ULL << 25, 



  DBG_USR1 = 1ULL << 27, 
  DBG_USR2 = 1ULL << 28, 
  DBG_USR3 = 1ULL << 29, 
  DBG_TEMP = 1ULL << 30, 

  DBG_ERROR = 1ULL << 31, 
  DBG_NONE = 0, 

  DBG_DEFAULT = DBG_ALL
};
# 59 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/system/sched.c"
typedef struct __nesc_unnamed4255 {
  void (*tp)(void);
} TOSH_sched_entry_T;

enum __nesc_unnamed4256 {






  TOSH_MAX_TASKS = 8, 

  TOSH_TASK_BITMASK = TOSH_MAX_TASKS - 1
};

volatile TOSH_sched_entry_T TOSH_queue[TOSH_MAX_TASKS];
uint8_t TOSH_sched_full;
volatile uint8_t TOSH_sched_free;
static inline 

void TOSH_wait(void );
static inline void TOSH_sleep(void );
static inline 
void TOSH_sched_init(void );
#line 100
bool  TOS_post(void (*tp)(void));
static inline 
#line 132
bool TOSH_run_next_task(void);
static inline 
#line 155
void TOSH_run_task(void);
# 28 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/system/Ident.h"
enum __nesc_unnamed4257 {

  IDENT_MAX_PROGRAM_NAME_LENGTH = 10
};

typedef struct __nesc_unnamed4258 {

  uint32_t unix_time;
  uint32_t user_hash;
  char program_name[IDENT_MAX_PROGRAM_NAME_LENGTH];
} Ident_t;
# 43 "/usr/local/lib/gcc-lib/avr/3.3-tinyos/include/stdarg.h"
typedef __builtin_va_list __gnuc_va_list;
# 105 "/usr/local/lib/gcc-lib/avr/3.3-tinyos/include/stdarg.h" 3
typedef __gnuc_va_list va_list;
# 154 "/usr/local/avr/include/stdio.h"
struct __file;
#line 204
struct __file;

struct __file;
#line 401
extern int sprintf(char *__s, const char *__fmt, ...);
# 26 "RFID_Control.h"
struct RFID_COMM_MSG {

  uint8_t comm;
  uint8_t block;
  uint8_t reserved1;
  uint8_t reserved2;
  uint8_t wbuff[4];
};
# 39 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/Timer.h"
enum __nesc_unnamed4259 {
  TIMER_REPEAT = 0, 
  TIMER_ONE_SHOT = 1, 
  NUM_TIMERS = 2
};
# 50 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/AM.h"
enum __nesc_unnamed4260 {
  TOS_BCAST_ADDR = 0xffff, 
  TOS_UART_ADDR = 0x007e
};
#line 66
enum __nesc_unnamed4261 {
  TOS_DEFAULT_AM_GROUP = 0x77
};

uint8_t TOS_AM_GROUP = TOS_DEFAULT_AM_GROUP;
#line 92
typedef struct TOS_Msg {


  uint8_t length;
  uint8_t fcfhi;
  uint8_t fcflo;
  uint8_t dsn;
  uint16_t destpan;
  uint16_t addr;
  uint8_t type;
  uint8_t group;
  int8_t data[29];








  uint8_t strength;
  uint8_t lqi;
  bool crc;
  uint8_t ack;
  uint16_t time;
} TOS_Msg;

typedef struct TinySec_Msg {

  uint8_t invalid;
} TinySec_Msg;









enum __nesc_unnamed4262 {

  MSG_HEADER_SIZE = (size_t )& ((struct TOS_Msg *)0)->data - 1, 

  MSG_FOOTER_SIZE = 2, 

  MSG_DATA_SIZE = (size_t )& ((struct TOS_Msg *)0)->strength + sizeof(uint16_t ), 

  DATA_LENGTH = 29, 

  LENGTH_BYTE_NUMBER = (size_t )& ((struct TOS_Msg *)0)->length + 1, 

  TOS_HEADER_SIZE = 5
};

typedef TOS_Msg *TOS_MsgPtr;
# 33 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/mica128/Clock.h"
enum __nesc_unnamed4263 {
  TOS_I1000PS = 32, TOS_S1000PS = 1, 
  TOS_I100PS = 40, TOS_S100PS = 2, 
  TOS_I10PS = 101, TOS_S10PS = 3, 
  TOS_I1024PS = 0, TOS_S1024PS = 3, 
  TOS_I512PS = 1, TOS_S512PS = 3, 
  TOS_I256PS = 3, TOS_S256PS = 3, 
  TOS_I128PS = 7, TOS_S128PS = 3, 
  TOS_I64PS = 15, TOS_S64PS = 3, 
  TOS_I32PS = 31, TOS_S32PS = 3, 
  TOS_I16PS = 63, TOS_S16PS = 3, 
  TOS_I8PS = 127, TOS_S8PS = 3, 
  TOS_I4PS = 255, TOS_S4PS = 3, 
  TOS_I2PS = 15, TOS_S2PS = 7, 
  TOS_I1PS = 31, TOS_S1PS = 7, 
  TOS_I0PS = 0, TOS_S0PS = 0
};
enum __nesc_unnamed4264 {
  DEFAULT_SCALE = 3, DEFAULT_INTERVAL = 127
};
static 
# 10 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/byteorder.h"
__inline int is_host_lsb(void);
static 




__inline uint16_t toLSB16(uint16_t a);
static 



__inline uint16_t fromLSB16(uint16_t a);
# 31 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/avrmote/crc.h"
uint16_t __attribute((__progmem__)) crcTable[256] = { 
0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7, 
0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef, 
0x1231, 0x0210, 0x3273, 0x2252, 0x52b5, 0x4294, 0x72f7, 0x62d6, 
0x9339, 0x8318, 0xb37b, 0xa35a, 0xd3bd, 0xc39c, 0xf3ff, 0xe3de, 
0x2462, 0x3443, 0x0420, 0x1401, 0x64e6, 0x74c7, 0x44a4, 0x5485, 
0xa56a, 0xb54b, 0x8528, 0x9509, 0xe5ee, 0xf5cf, 0xc5ac, 0xd58d, 
0x3653, 0x2672, 0x1611, 0x0630, 0x76d7, 0x66f6, 0x5695, 0x46b4, 
0xb75b, 0xa77a, 0x9719, 0x8738, 0xf7df, 0xe7fe, 0xd79d, 0xc7bc, 
0x48c4, 0x58e5, 0x6886, 0x78a7, 0x0840, 0x1861, 0x2802, 0x3823, 
0xc9cc, 0xd9ed, 0xe98e, 0xf9af, 0x8948, 0x9969, 0xa90a, 0xb92b, 
0x5af5, 0x4ad4, 0x7ab7, 0x6a96, 0x1a71, 0x0a50, 0x3a33, 0x2a12, 
0xdbfd, 0xcbdc, 0xfbbf, 0xeb9e, 0x9b79, 0x8b58, 0xbb3b, 0xab1a, 
0x6ca6, 0x7c87, 0x4ce4, 0x5cc5, 0x2c22, 0x3c03, 0x0c60, 0x1c41, 
0xedae, 0xfd8f, 0xcdec, 0xddcd, 0xad2a, 0xbd0b, 0x8d68, 0x9d49, 
0x7e97, 0x6eb6, 0x5ed5, 0x4ef4, 0x3e13, 0x2e32, 0x1e51, 0x0e70, 
0xff9f, 0xefbe, 0xdfdd, 0xcffc, 0xbf1b, 0xaf3a, 0x9f59, 0x8f78, 
0x9188, 0x81a9, 0xb1ca, 0xa1eb, 0xd10c, 0xc12d, 0xf14e, 0xe16f, 
0x1080, 0x00a1, 0x30c2, 0x20e3, 0x5004, 0x4025, 0x7046, 0x6067, 
0x83b9, 0x9398, 0xa3fb, 0xb3da, 0xc33d, 0xd31c, 0xe37f, 0xf35e, 
0x02b1, 0x1290, 0x22f3, 0x32d2, 0x4235, 0x5214, 0x6277, 0x7256, 
0xb5ea, 0xa5cb, 0x95a8, 0x8589, 0xf56e, 0xe54f, 0xd52c, 0xc50d, 
0x34e2, 0x24c3, 0x14a0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405, 
0xa7db, 0xb7fa, 0x8799, 0x97b8, 0xe75f, 0xf77e, 0xc71d, 0xd73c, 
0x26d3, 0x36f2, 0x0691, 0x16b0, 0x6657, 0x7676, 0x4615, 0x5634, 
0xd94c, 0xc96d, 0xf90e, 0xe92f, 0x99c8, 0x89e9, 0xb98a, 0xa9ab, 
0x5844, 0x4865, 0x7806, 0x6827, 0x18c0, 0x08e1, 0x3882, 0x28a3, 
0xcb7d, 0xdb5c, 0xeb3f, 0xfb1e, 0x8bf9, 0x9bd8, 0xabbb, 0xbb9a, 
0x4a75, 0x5a54, 0x6a37, 0x7a16, 0x0af1, 0x1ad0, 0x2ab3, 0x3a92, 
0xfd2e, 0xed0f, 0xdd6c, 0xcd4d, 0xbdaa, 0xad8b, 0x9de8, 0x8dc9, 
0x7c26, 0x6c07, 0x5c64, 0x4c45, 0x3ca2, 0x2c83, 0x1ce0, 0x0cc1, 
0xef1f, 0xff3e, 0xcf5d, 0xdf7c, 0xaf9b, 0xbfba, 0x8fd9, 0x9ff8, 
0x6e17, 0x7e36, 0x4e55, 0x5e74, 0x2e93, 0x3eb2, 0x0ed1, 0x1ef0 };
static inline 

uint16_t crcByte(uint16_t oldCrc, uint8_t byte);
static  result_t PotM$Pot$init(uint8_t arg_0xa38f058);
static  result_t HPLPotC$Pot$finalise(void);
static  result_t HPLPotC$Pot$decrease(void);
static  result_t HPLPotC$Pot$increase(void);
static  result_t HPLInit$init(void);
static  TOS_MsgPtr testRFIDM$ReceiveMsg$receive(TOS_MsgPtr arg_0xa3e46d8);
static   void testRFIDM$SCSuartDBGRecv$UARTRecv(uint8_t arg_0xa3e5bc0);
static  result_t testRFIDM$SendMsg$sendDone(TOS_MsgPtr arg_0xa3df138, result_t arg_0xa3df288);
static   void testRFIDM$RFID_Control$RData_15693_Done(char arg_0xa3d66d0, uint8_t *arg_0xa3d6830, char arg_0xa3d6970);
static   void testRFIDM$RFID_Control$GetID_14443A_Done(char arg_0xa3d9940, uint8_t *arg_0xa3d9aa0, char arg_0xa3d9be0);
static   void testRFIDM$RFID_Control$WData_15693_Done(char arg_0xa3d6d90);
static   void testRFIDM$RFID_Control$GetID_15693_Done(char arg_0xa3d6010, uint8_t *arg_0xa3d6170, char arg_0xa3d62b0);
static  result_t testRFIDM$StdControl$init(void);
static  result_t testRFIDM$StdControl$start(void);
static  result_t testRFIDM$Timer$fired(void);
static   result_t TimerM$Clock$fire(void);
static  result_t TimerM$StdControl$init(void);
static  result_t TimerM$StdControl$start(void);
static  result_t TimerM$Timer$default$fired(uint8_t arg_0xa40a158);
static  result_t TimerM$Timer$start(uint8_t arg_0xa40a158, char arg_0xa3c6b70, uint32_t arg_0xa3c6cc8);
static   void HPLClock$Clock$setInterval(uint8_t arg_0xa4188c8);
static   uint8_t HPLClock$Clock$readCounter(void);
static   result_t HPLClock$Clock$setRate(char arg_0xa42fbf8, char arg_0xa42fd38);
static   uint8_t HPLPowerManagementM$PowerManagement$adjustPower(void);
static   result_t LedsC$Leds$init(void);
static   result_t LedsC$Leds$greenOff(void);
static   result_t LedsC$Leds$redOff(void);
static   result_t LedsC$Leds$greenToggle(void);
static   result_t LedsC$Leds$redToggle(void);
static   result_t LedsC$Leds$redOn(void);
static   result_t LedsC$Leds$greenOn(void);
static   result_t RFID_ControlM$UART$get(uint8_t arg_0xa4a6800);
static   result_t RFID_ControlM$UART$putDone(void);
static   void RFID_ControlM$RFID_Control$GetID_14443A(void);
static   void RFID_ControlM$RFID_Control$GetID_15693(void);
static   void RFID_ControlM$RFID_Control$WData_15693(char arg_0xa3d9288, char *arg_0xa3d93e0, char arg_0xa3d9520);
static   void RFID_ControlM$RFID_Control$RData_15693(char arg_0xa3d8e78);
static   result_t HPLUART1M$UART$put(uint8_t arg_0xa4a6300);
static   result_t SCSuartDBGM$HPLUART$get(uint8_t arg_0xa4a6800);
static   result_t SCSuartDBGM$HPLUART$putDone(void);
static   void SCSuartDBGM$SCSuartDBG$UARTSend(uint8_t *arg_0xa3d23b0, uint8_t arg_0xa3d24f8);
static  result_t SCSuartDBGM$StdControl$init(void);
static  result_t SCSuartDBGM$StdControl$start(void);
static   result_t HPLUART0M$UART$init(void);
static   result_t HPLUART0M$UART$put(uint8_t arg_0xa4a6300);
static  TOS_MsgPtr AMStandard$ReceiveMsg$default$receive(uint8_t arg_0xa50ccd0, TOS_MsgPtr arg_0xa3e46d8);
static  result_t AMStandard$ActivityTimer$fired(void);
static  result_t AMStandard$UARTSend$sendDone(TOS_MsgPtr arg_0xa524e88, result_t arg_0xa524fd8);
static  TOS_MsgPtr AMStandard$RadioReceive$receive(TOS_MsgPtr arg_0xa3e46d8);
static  result_t AMStandard$Control$init(void);
static  result_t AMStandard$Control$start(void);
static  result_t AMStandard$default$sendDone(void);
static  result_t AMStandard$RadioSend$sendDone(TOS_MsgPtr arg_0xa524e88, result_t arg_0xa524fd8);
static  result_t AMStandard$SendMsg$default$sendDone(uint8_t arg_0xa50c718, TOS_MsgPtr arg_0xa3df138, result_t arg_0xa3df288);
static  TOS_MsgPtr AMStandard$UARTReceive$receive(TOS_MsgPtr arg_0xa3e46d8);
static   result_t CC2420RadioM$BackoffTimerJiffy$fired(void);
static   void CC2420RadioM$MacControl$enableAck(void);
static   result_t CC2420RadioM$HPLChipcon$FIFOPIntr(void);
static   result_t CC2420RadioM$HPLChipconFIFO$TXFIFODone(uint8_t arg_0xa566210, uint8_t *arg_0xa566370);
static   result_t CC2420RadioM$HPLChipconFIFO$RXFIFODone(uint8_t arg_0xa56fb68, uint8_t *arg_0xa56fcc8);
static  result_t CC2420RadioM$StdControl$init(void);
static  result_t CC2420RadioM$StdControl$start(void);
static   int16_t CC2420RadioM$MacBackoff$default$congestionBackoff(TOS_MsgPtr arg_0xa543aa0);
static   result_t CC2420ControlM$HPLChipcon$FIFOPIntr(void);
static   result_t CC2420ControlM$HPLChipconRAM$writeDone(uint16_t arg_0xa5b47a8, uint8_t arg_0xa5b48f0, uint8_t *arg_0xa5b4a50);
static  result_t CC2420ControlM$StdControl$init(void);
static  result_t CC2420ControlM$StdControl$start(void);
static   result_t CC2420ControlM$CC2420Control$VREFOn(void);
static   result_t CC2420ControlM$CC2420Control$enableAddrDecode(void);
static   result_t CC2420ControlM$CC2420Control$enableAutoAck(void);
static   result_t CC2420ControlM$CC2420Control$TunePreset(uint8_t arg_0xa548930);
static   result_t CC2420ControlM$CC2420Control$RxMode(void);
static  result_t CC2420ControlM$CC2420Control$setShortAddress(uint16_t arg_0xa5602f8);
static   result_t CC2420ControlM$CC2420Control$OscillatorOn(void);
static   result_t HPLCC2420M$HPLCC2420$enableFIFOP(void);
static   uint16_t HPLCC2420M$HPLCC2420$read(uint8_t arg_0xa56b6a8);
static   uint8_t HPLCC2420M$HPLCC2420$write(uint8_t arg_0xa56b0b0, uint16_t arg_0xa56b200);
static   uint8_t HPLCC2420M$HPLCC2420$cmd(uint8_t arg_0xa56ac58);
static   result_t HPLCC2420M$HPLCC2420RAM$write(uint16_t arg_0xa5b4080, uint8_t arg_0xa5b41c8, uint8_t *arg_0xa5b4328);
static  result_t HPLCC2420M$StdControl$init(void);
static  result_t HPLCC2420M$StdControl$start(void);
static   result_t HPLCC2420FIFOM$HPLCC2420FIFO$writeTXFIFO(uint8_t arg_0xa56f4e8, uint8_t *arg_0xa56f648);
static   result_t HPLCC2420FIFOM$HPLCC2420FIFO$readRXFIFO(uint8_t arg_0xa56edb0, uint8_t *arg_0xa56ef10);
static   uint16_t RandomLFSR$Random$rand(void);
static   result_t RandomLFSR$Random$init(void);
static   result_t TimerJiffyAsyncM$TimerJiffyAsync$setOneShot(uint32_t arg_0xa567e68);
static   bool TimerJiffyAsyncM$TimerJiffyAsync$isSet(void);
static   result_t TimerJiffyAsyncM$TimerJiffyAsync$stop(void);
static  result_t TimerJiffyAsyncM$StdControl$init(void);
static  result_t TimerJiffyAsyncM$StdControl$start(void);
static   result_t TimerJiffyAsyncM$Timer$fire(void);
static   result_t HPLTimer2$Timer2$setIntervalAndScale(uint8_t arg_0xa419c48, uint8_t arg_0xa419d90);
static   void HPLTimer2$Timer2$intDisable(void);
static   result_t FramerM$ByteComm$txDone(void);
static   result_t FramerM$ByteComm$txByteReady(bool arg_0xa67c728);
static   result_t FramerM$ByteComm$rxByteReady(uint8_t arg_0xa65df40, bool arg_0xa67c0a0, uint16_t arg_0xa67c1f8);
static  result_t FramerM$StdControl$init(void);
static  result_t FramerM$StdControl$start(void);
static  result_t FramerM$TokenReceiveMsg$ReflectToken(uint8_t arg_0xa681dd8);
static  TOS_MsgPtr FramerAckM$ReceiveMsg$receive(TOS_MsgPtr arg_0xa3e46d8);
static  TOS_MsgPtr FramerAckM$TokenReceiveMsg$receive(TOS_MsgPtr arg_0xa681678, uint8_t arg_0xa6817c0);
static   result_t UARTM$HPLUART$get(uint8_t arg_0xa4a6800);
static   result_t UARTM$HPLUART$putDone(void);
static   result_t UARTM$ByteComm$txByte(uint8_t arg_0xa65dab0);
static  result_t UARTM$Control$init(void);
static  result_t UARTM$Control$start(void);
static  
# 47 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/system/RealMain.nc"
result_t RealMain$hardwareInit(void);
static  
# 78 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/Pot.nc"
result_t RealMain$Pot$init(uint8_t arg_0xa38f058);
static  
# 63 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
result_t RealMain$StdControl$init(void);
static  





result_t RealMain$StdControl$start(void);
# 54 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/system/RealMain.nc"
int   main(void);
static  
# 74 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/HPLPot.nc"
result_t PotM$HPLPot$finalise(void);
static  
#line 59
result_t PotM$HPLPot$decrease(void);
static  






result_t PotM$HPLPot$increase(void);
# 91 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/system/PotM.nc"
uint8_t PotM$potSetting;
static inline 
void PotM$setPot(uint8_t value);
static inline  
#line 106
result_t PotM$Pot$init(uint8_t initialSetting);
static inline  
# 57 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/mica2/HPLPotC.nc"
result_t HPLPotC$Pot$decrease(void);
static inline  







result_t HPLPotC$Pot$increase(void);
static inline  







result_t HPLPotC$Pot$finalise(void);
static inline  
# 57 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/avrmote/HPLInit.nc"
result_t HPLInit$init(void);
static   
# 30 "SCSuartDBG.nc"
void testRFIDM$SCSuartDBG$UARTSend(uint8_t *arg_0xa3d23b0, uint8_t arg_0xa3d24f8);
static  
# 63 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
result_t testRFIDM$CommControl$init(void);
static  





result_t testRFIDM$CommControl$start(void);
static   
# 56 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/Leds.nc"
result_t testRFIDM$Leds$init(void);
static   
#line 106
result_t testRFIDM$Leds$greenToggle(void);
static   
#line 81
result_t testRFIDM$Leds$redToggle(void);
static  
# 63 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
result_t testRFIDM$SCSuartSTD$init(void);
static  





result_t testRFIDM$SCSuartSTD$start(void);
static   
# 4 "RFID_Control.nc"
void testRFIDM$RFID_Control$GetID_14443A(void);
static   void testRFIDM$RFID_Control$GetID_15693(void);
static   
void testRFIDM$RFID_Control$WData_15693(char arg_0xa3d9288, char *arg_0xa3d93e0, char arg_0xa3d9520);
static   
#line 6
void testRFIDM$RFID_Control$RData_15693(char arg_0xa3d8e78);
static 
# 17 "testRFIDM.nc"
void testRFIDM$Control_RFID(uint8_t comm, uint8_t block, uint8_t *wbuff);
char testRFIDM$OutputUartMsg[64];
char testRFIDM$Inc_Flag;
static inline  

result_t testRFIDM$StdControl$init(void);
static inline  
#line 47
result_t testRFIDM$StdControl$start(void);
static inline  
#line 60
result_t testRFIDM$Timer$fired(void);
static inline  



result_t testRFIDM$SendMsg$sendDone(TOS_MsgPtr sent, result_t success);
static inline  


TOS_MsgPtr testRFIDM$ReceiveMsg$receive(TOS_MsgPtr m);
static 
#line 81
void testRFIDM$Control_RFID(uint8_t comm, uint8_t block, uint8_t *buff);
#line 97
uint8_t testRFIDM$RecvBuff[8];
uint8_t testRFIDM$recv_num = 0;
bool testRFIDM$start_flag = 0;
static   
void testRFIDM$SCSuartDBGRecv$UARTRecv(uint8_t recv_Char);
static   
#line 125
void testRFIDM$RFID_Control$GetID_14443A_Done(char status, uint8_t *buff, char size);
static   
#line 148
void testRFIDM$RFID_Control$GetID_15693_Done(char status, uint8_t *buff, char size);
static   
#line 172
void testRFIDM$RFID_Control$RData_15693_Done(char status, uint8_t *buff, char size);
static   
#line 195
void testRFIDM$RFID_Control$WData_15693_Done(char status);
static   
# 41 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/PowerManagement.nc"
uint8_t TimerM$PowerManagement$adjustPower(void);
static   
# 105 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/Clock.nc"
void TimerM$Clock$setInterval(uint8_t arg_0xa4188c8);
static   
#line 153
uint8_t TimerM$Clock$readCounter(void);
static   
#line 96
result_t TimerM$Clock$setRate(char arg_0xa42fbf8, char arg_0xa42fd38);
static  
# 73 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/Timer.nc"
result_t TimerM$Timer$fired(
# 45 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/system/TimerM.nc"
uint8_t arg_0xa40a158);









uint32_t TimerM$mState;
uint8_t TimerM$setIntervalFlag;
uint8_t TimerM$mScale;
#line 57
uint8_t TimerM$mInterval;
int8_t TimerM$queue_head;
int8_t TimerM$queue_tail;
uint8_t TimerM$queue_size;
uint8_t TimerM$queue[NUM_TIMERS];

struct TimerM$timer_s {
  uint8_t type;
  int32_t ticks;
  int32_t ticksLeft;
} TimerM$mTimerList[NUM_TIMERS];

enum TimerM$__nesc_unnamed4265 {
  TimerM$maxTimerInterval = 230
};
static  result_t TimerM$StdControl$init(void);
static inline  








result_t TimerM$StdControl$start(void);
static inline  









result_t TimerM$Timer$start(uint8_t id, char type, 
uint32_t interval);
#line 116
static void TimerM$adjustInterval(void);
static inline   
#line 154
result_t TimerM$Timer$default$fired(uint8_t id);
static inline 


void TimerM$enqueue(uint8_t value);
static inline 






uint8_t TimerM$dequeue(void);
static inline  








void TimerM$signalOneTimer(void);
static inline  




void TimerM$HandleFire(void);
static inline   
#line 204
result_t TimerM$Clock$fire(void);
static   
# 180 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/Clock.nc"
result_t HPLClock$Clock$fire(void);
# 58 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/HPLClock.nc"
uint8_t HPLClock$set_flag;
uint8_t HPLClock$mscale;
#line 59
uint8_t HPLClock$nextScale;
#line 59
uint8_t HPLClock$minterval;
uint32_t HPLClock$systime;
static inline   
#line 93
void HPLClock$Clock$setInterval(uint8_t value);
static inline   
#line 140
uint8_t HPLClock$Clock$readCounter(void);
static inline   
#line 155
result_t HPLClock$Clock$setRate(char interval, char scale);
#line 210
void __attribute((signal))   __vector_15(void);
# 51 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/HPLPowerManagementM.nc"
bool HPLPowerManagementM$disabled = TRUE;

enum HPLPowerManagementM$__nesc_unnamed4266 {
  HPLPowerManagementM$IDLE = 0, 
  HPLPowerManagementM$ADC_NR = 1 << 3, 
  HPLPowerManagementM$POWER_DOWN = 1 << 4, 
  HPLPowerManagementM$POWER_SAVE = (1 << 3) + (1 << 4), 
  HPLPowerManagementM$STANDBY = (1 << 2) + (1 << 4), 
  HPLPowerManagementM$EXT_STANDBY = (1 << 3) + (1 << 4) + (1 << 2)
};
static inline 

uint8_t HPLPowerManagementM$getPowerLevel(void);
static inline  
#line 84
void HPLPowerManagementM$doAdjustment(void);
static   
#line 101
uint8_t HPLPowerManagementM$PowerManagement$adjustPower(void);
# 50 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/system/LedsC.nc"
uint8_t LedsC$ledsOn;

enum LedsC$__nesc_unnamed4267 {
  LedsC$RED_BIT = 1, 
  LedsC$GREEN_BIT = 2, 
  LedsC$YELLOW_BIT = 4
};
static inline   
result_t LedsC$Leds$init(void);
static inline   
#line 72
result_t LedsC$Leds$redOn(void);
static inline   







result_t LedsC$Leds$redOff(void);
static inline   







result_t LedsC$Leds$redToggle(void);
static inline   









result_t LedsC$Leds$greenOn(void);
static inline   







result_t LedsC$Leds$greenOff(void);
static   







result_t LedsC$Leds$greenToggle(void);
static   
# 80 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/HPLUART.nc"
result_t RFID_ControlM$UART$put(uint8_t arg_0xa4a6300);
static   
# 11 "RFID_Control.nc"
void RFID_ControlM$RFID_Control$RData_15693_Done(char arg_0xa3d66d0, uint8_t *arg_0xa3d6830, char arg_0xa3d6970);
static   
#line 9
void RFID_ControlM$RFID_Control$GetID_14443A_Done(char arg_0xa3d9940, uint8_t *arg_0xa3d9aa0, char arg_0xa3d9be0);
static   

void RFID_ControlM$RFID_Control$WData_15693_Done(char arg_0xa3d6d90);
static   
#line 10
void RFID_ControlM$RFID_Control$GetID_15693_Done(char arg_0xa3d6010, uint8_t *arg_0xa3d6170, char arg_0xa3d62b0);
static   
# 30 "SCSuartDBG.nc"
void RFID_ControlM$SCSuartDBG$UARTSend(uint8_t *arg_0xa3d23b0, uint8_t arg_0xa3d24f8);
static 
# 9 "RFID_ControlM.nc"
void RFID_ControlM$UARTSend(uint8_t *data, uint8_t len);
static inline void RFID_ControlM$processing_recvData(void);
static void RFID_ControlM$gen15693crc(uint8_t *data, uint8_t len, uint8_t *byte1, uint8_t *byte2);





bool RFID_ControlM$state = 0;
uint8_t RFID_ControlM$str_buff[128];
uint16_t RFID_ControlM$buff_start = 0;
uint16_t RFID_ControlM$buff_end = 0;

char RFID_ControlM$StartComm14443[9] = { 0x02, 0x07, 0xF1, 0x33, 0x48, 0x80, 0x02, 0x0F, 0x03 };
char RFID_ControlM$GetID14443[5] = { 0x02, 0x03, 0x6B, 0x68, 0x03 };

char RFID_ControlM$StartComm15693[9] = { 0x02, 0x07, 0xF1, 0x33, 0xEB, 0x80, 0x02, 0xAC, 0x03 };
char RFID_ControlM$GetID15693[11] = { 0x02, 0x09, 0x81, 0x08, 0x26, 0x01, 0x00, 0xF6, 0x0A, 0x5B, 0x03 };
char RFID_ControlM$ReadData15693[12] = { 0x02, 0x0A, 0x88, 0x38, 0x02, 0x23, 0x00, 0x00, 0xF7, 0x29, 0x45, 0x03 };


char RFID_ControlM$SetMemoryAsscess[10] = { 0x02, 0x08, 0xF2, 0x08, 0x33, 0xEB, 0x80, 0x02, 0xA8, 0x03 };

uint8_t RFID_ControlM$RFID_State;
uint8_t RFID_ControlM$RFID_CardType = 0;

enum RFID_ControlM$__nesc_unnamed4268 {
#line 35
  RFID_ControlM$RFID_NULL, RFID_ControlM$RFID14443A_GETID, RFID_ControlM$RFID15693_GETID, RFID_ControlM$RFID15693_RDATA, RFID_ControlM$RFID15693_WDATA, 
  RFID_ControlM$RFID14443A_GETID_START, RFID_ControlM$RFID15693_GETID_START, RFID_ControlM$RFID15693_RDATA_START, RFID_ControlM$RFID15693_WDATA_START, 
  RFID_ControlM$RFID15693_RDATA_MEM, RFID_ControlM$RFID15693_WDATA_MEM
};






uint8_t RFID_ControlM$RFID_Write_Buff[32];
uint8_t RFID_ControlM$RFID_Write_Buff_Size;

uint8_t RFID_ControlM$RecvBuff_From_RFID[32];
uint8_t RFID_ControlM$RBuff_Index;
uint8_t RFID_ControlM$CheckSum_Index;

uint8_t RFID_ControlM$UID15693[10][8];
uint8_t RFID_ControlM$UID15693Index = 0;





uint8_t RFID_ControlM$debug4F = 0xF3;




uint8_t RFID_ControlM$debug4S = 0xF7;
static inline   
void RFID_ControlM$RFID_Control$GetID_14443A(void);
static inline   
#line 81
void RFID_ControlM$RFID_Control$GetID_15693(void);
static inline   
#line 97
void RFID_ControlM$RFID_Control$RData_15693(char BlockAddr);
static inline   
#line 122
void RFID_ControlM$RFID_Control$WData_15693(char BlockAddr, char *buff, char size);
static   
#line 162
result_t RFID_ControlM$UART$get(uint8_t data);
static inline 
#line 183
void RFID_ControlM$processing_recvData(void);
static   
#line 336
result_t RFID_ControlM$UART$putDone(void);
static 
#line 358
void RFID_ControlM$UARTSend(uint8_t *data, uint8_t len);
static 
#line 383
void RFID_ControlM$gen15693crc(uint8_t *data, uint8_t len, uint8_t *byte1, uint8_t *byte2);
static   
# 88 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/HPLUART.nc"
result_t HPLUART1M$UART$get(uint8_t arg_0xa4a6800);
static   






result_t HPLUART1M$UART$putDone(void);
# 85 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/HPLUART1M.nc"
void __attribute((signal))   __vector_30(void);





void __attribute((interrupt))   __vector_32(void);
static inline   


result_t HPLUART1M$UART$put(uint8_t data);
static   
# 62 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/HPLUART.nc"
result_t SCSuartDBGM$HPLUART$init(void);
static   
#line 80
result_t SCSuartDBGM$HPLUART$put(uint8_t arg_0xa4a6300);
static   
# 30 "SCSuartDBGRecv.nc"
void SCSuartDBGM$SCSuartDBGRecv$UARTRecv(uint8_t arg_0xa3e5bc0);
# 43 "SCSuartDBGM.nc"
bool SCSuartDBGM$state;

uint8_t SCSuartDBGM$str_buff[128];
uint16_t SCSuartDBGM$buff_start;
uint16_t SCSuartDBGM$buff_end;
static inline  
result_t SCSuartDBGM$StdControl$init(void);
static inline  








result_t SCSuartDBGM$StdControl$start(void);
static inline   







result_t SCSuartDBGM$HPLUART$get(uint8_t data);
static   



result_t SCSuartDBGM$HPLUART$putDone(void);
static   
#line 94
void SCSuartDBGM$SCSuartDBG$UARTSend(uint8_t *data, uint8_t len);
static   
# 88 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/HPLUART.nc"
result_t HPLUART0M$UART$get(uint8_t arg_0xa4a6800);
static   






result_t HPLUART0M$UART$putDone(void);
static   
# 60 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/HPLUART0M.nc"
result_t HPLUART0M$UART$init(void);
#line 90
void __attribute((signal))   __vector_18(void);









void __attribute((interrupt))   __vector_20(void);
static inline   



result_t HPLUART0M$UART$put(uint8_t data);
static  
# 75 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/ReceiveMsg.nc"
TOS_MsgPtr AMStandard$ReceiveMsg$receive(
# 56 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/system/AMStandard.nc"
uint8_t arg_0xa50ccd0, 
# 75 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/ReceiveMsg.nc"
TOS_MsgPtr arg_0xa3e46d8);
static  
# 59 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/Timer.nc"
result_t AMStandard$ActivityTimer$start(char arg_0xa3c6b70, uint32_t arg_0xa3c6cc8);
static   
# 41 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/PowerManagement.nc"
uint8_t AMStandard$PowerManagement$adjustPower(void);
static  
# 63 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
result_t AMStandard$RadioControl$init(void);
static  





result_t AMStandard$RadioControl$start(void);
static  
#line 63
result_t AMStandard$TimerControl$init(void);
static  





result_t AMStandard$TimerControl$start(void);
static  
#line 63
result_t AMStandard$UARTControl$init(void);
static  





result_t AMStandard$UARTControl$start(void);
static  
# 65 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/system/AMStandard.nc"
result_t AMStandard$sendDone(void);
static  
# 49 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/SendMsg.nc"
result_t AMStandard$SendMsg$sendDone(
# 55 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/system/AMStandard.nc"
uint8_t arg_0xa50c718, 
# 49 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/SendMsg.nc"
TOS_MsgPtr arg_0xa3df138, result_t arg_0xa3df288);
# 81 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/system/AMStandard.nc"
bool AMStandard$state;

uint16_t AMStandard$lastCount;
uint16_t AMStandard$counter;
static inline  

bool AMStandard$Control$init(void);
static inline  
#line 103
bool AMStandard$Control$start(void);
static inline 
#line 132
void AMStandard$dbgPacket(TOS_MsgPtr data);
static 









result_t AMStandard$reportSendDone(TOS_MsgPtr msg, result_t success);
static inline  






result_t AMStandard$ActivityTimer$fired(void);
static inline   




result_t AMStandard$SendMsg$default$sendDone(uint8_t id, TOS_MsgPtr msg, result_t success);
static inline   

result_t AMStandard$default$sendDone(void);
static inline  
#line 207
result_t AMStandard$UARTSend$sendDone(TOS_MsgPtr msg, result_t success);
static inline  

result_t AMStandard$RadioSend$sendDone(TOS_MsgPtr msg, result_t success);




TOS_MsgPtr   received(TOS_MsgPtr packet);
static inline   
#line 242
TOS_MsgPtr AMStandard$ReceiveMsg$default$receive(uint8_t id, TOS_MsgPtr msg);
static inline  


TOS_MsgPtr AMStandard$UARTReceive$receive(TOS_MsgPtr packet);
static inline  




TOS_MsgPtr AMStandard$RadioReceive$receive(TOS_MsgPtr packet);
static   
# 6 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/TimerJiffyAsync.nc"
result_t CC2420RadioM$BackoffTimerJiffy$setOneShot(uint32_t arg_0xa567e68);
static   


bool CC2420RadioM$BackoffTimerJiffy$isSet(void);
static   
#line 8
result_t CC2420RadioM$BackoffTimerJiffy$stop(void);
static  
# 67 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/BareSendMsg.nc"
result_t CC2420RadioM$Send$sendDone(TOS_MsgPtr arg_0xa524e88, result_t arg_0xa524fd8);
static   
# 63 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/Random.nc"
uint16_t CC2420RadioM$Random$rand(void);
static   
#line 57
result_t CC2420RadioM$Random$init(void);
static  
# 63 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
result_t CC2420RadioM$TimerControl$init(void);
static  





result_t CC2420RadioM$TimerControl$start(void);
static  
# 75 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/ReceiveMsg.nc"
TOS_MsgPtr CC2420RadioM$Receive$receive(TOS_MsgPtr arg_0xa3e46d8);
static   
# 42 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/HPLCC2420.nc"
result_t CC2420RadioM$HPLChipcon$enableFIFOP(void);
static   
#line 66
uint16_t CC2420RadioM$HPLChipcon$read(uint8_t arg_0xa56b6a8);
static   
#line 52
uint8_t CC2420RadioM$HPLChipcon$cmd(uint8_t arg_0xa56ac58);
static  
# 63 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
result_t CC2420RadioM$CC2420StdControl$init(void);
static  





result_t CC2420RadioM$CC2420StdControl$start(void);
static   
# 27 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/HPLCC2420FIFO.nc"
result_t CC2420RadioM$HPLChipconFIFO$writeTXFIFO(uint8_t arg_0xa56f4e8, uint8_t *arg_0xa56f648);
static   
#line 17
result_t CC2420RadioM$HPLChipconFIFO$readRXFIFO(uint8_t arg_0xa56edb0, uint8_t *arg_0xa56ef10);
static   
# 187 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/CC2420Control.nc"
result_t CC2420RadioM$CC2420Control$enableAddrDecode(void);
static   
#line 173
result_t CC2420RadioM$CC2420Control$enableAutoAck(void);
static   
#line 144
result_t CC2420RadioM$CC2420Control$RxMode(void);
static   
# 75 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/MacBackoff.nc"
int16_t CC2420RadioM$MacBackoff$congestionBackoff(TOS_MsgPtr arg_0xa543aa0);
# 73 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/CC2420RadioM.nc"
enum CC2420RadioM$__nesc_unnamed4269 {
  CC2420RadioM$DISABLED_STATE = 0, 
  CC2420RadioM$IDLE_STATE, 
  CC2420RadioM$TX_STATE, 
  CC2420RadioM$PRE_TX_STATE, 
  CC2420RadioM$POST_TX_STATE, 
  CC2420RadioM$POST_TX_ACK_STATE, 
  CC2420RadioM$RX_STATE, 
  CC2420RadioM$POWER_DOWN_STATE, 

  CC2420RadioM$TIMER_INITIAL = 0, 
  CC2420RadioM$TIMER_BACKOFF, 
  CC2420RadioM$TIMER_ACK
};
 


uint8_t CC2420RadioM$countRetry;
uint8_t CC2420RadioM$stateRadio;
 uint8_t CC2420RadioM$stateTimer;
 uint8_t CC2420RadioM$currentDSN;
 bool CC2420RadioM$bAckEnable;
uint16_t CC2420RadioM$txlength;
uint16_t CC2420RadioM$rxlength;
 TOS_MsgPtr CC2420RadioM$txbufptr;
 TOS_MsgPtr CC2420RadioM$rxbufptr;
TOS_Msg CC2420RadioM$RxBuf;








volatile uint16_t CC2420RadioM$LocalAddr;
static inline  



void CC2420RadioM$sendDonetouplayer(void);
static 



void CC2420RadioM$sendFailed(void);
static 









__inline result_t CC2420RadioM$setBackoffTimer(uint16_t jiffy);
static 



__inline result_t CC2420RadioM$setAckTimer(uint16_t jiffy);
static inline  







void CC2420RadioM$PacketRcvd(void);
static  
#line 158
void CC2420RadioM$PacketSent(void);
static inline  
#line 175
result_t CC2420RadioM$StdControl$init(void);
static inline  
#line 203
result_t CC2420RadioM$StdControl$start(void);
static inline 
#line 224
void CC2420RadioM$sendPacket(void);
static inline  
#line 255
void CC2420RadioM$startSend(void);
static 









void CC2420RadioM$tryToSend(void);
static inline   
#line 292
result_t CC2420RadioM$BackoffTimerJiffy$fired(void);
static inline  
#line 359
void CC2420RadioM$delayedRXFIFO(void);
static   
#line 379
result_t CC2420RadioM$HPLChipcon$FIFOPIntr(void);
static inline   
#line 404
result_t CC2420RadioM$HPLChipconFIFO$RXFIFODone(uint8_t length, uint8_t *data);
static inline   
#line 450
result_t CC2420RadioM$HPLChipconFIFO$TXFIFODone(uint8_t length, uint8_t *data);
static inline   



void CC2420RadioM$MacControl$enableAck(void);
static inline    
#line 478
int16_t CC2420RadioM$MacBackoff$default$congestionBackoff(TOS_MsgPtr m);
static   
# 66 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/HPLCC2420.nc"
uint16_t CC2420ControlM$HPLChipcon$read(uint8_t arg_0xa56b6a8);
static   
#line 59
uint8_t CC2420ControlM$HPLChipcon$write(uint8_t arg_0xa56b0b0, uint16_t arg_0xa56b200);
static   
#line 52
uint8_t CC2420ControlM$HPLChipcon$cmd(uint8_t arg_0xa56ac58);
static  
# 63 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
result_t CC2420ControlM$HPLChipconControl$init(void);
static  





result_t CC2420ControlM$HPLChipconControl$start(void);
static   
# 47 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/HPLCC2420RAM.nc"
result_t CC2420ControlM$HPLChipconRAM$write(uint16_t arg_0xa5b4080, uint8_t arg_0xa5b41c8, uint8_t *arg_0xa5b4328);
 
# 62 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/CC2420ControlM.nc"
uint16_t CC2420ControlM$gCurrentParameters[14];
static inline 






bool CC2420ControlM$SetRegs(void);
static inline  
#line 102
result_t CC2420ControlM$StdControl$init(void);
static inline  
#line 173
result_t CC2420ControlM$StdControl$start(void);
static inline   
#line 204
result_t CC2420ControlM$CC2420Control$TunePreset(uint8_t chnl);
static inline   
#line 263
result_t CC2420ControlM$CC2420Control$RxMode(void);
static inline   
#line 288
result_t CC2420ControlM$CC2420Control$OscillatorOn(void);
static inline   
#line 313
result_t CC2420ControlM$CC2420Control$VREFOn(void);
static inline   









result_t CC2420ControlM$CC2420Control$enableAutoAck(void);
static inline   








result_t CC2420ControlM$CC2420Control$enableAddrDecode(void);
static inline  








result_t CC2420ControlM$CC2420Control$setShortAddress(uint16_t addr);
static inline   



result_t CC2420ControlM$HPLChipcon$FIFOPIntr(void);
static inline   






result_t CC2420ControlM$HPLChipconRAM$writeDone(uint16_t addr, uint8_t length, uint8_t *buffer);
static   
# 45 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/HPLCC2420.nc"
result_t HPLCC2420M$HPLCC2420$FIFOPIntr(void);
static   
# 49 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/HPLCC2420RAM.nc"
result_t HPLCC2420M$HPLCC2420RAM$writeDone(uint16_t arg_0xa5b47a8, uint8_t arg_0xa5b48f0, uint8_t *arg_0xa5b4a50);
 
# 52 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/HPLCC2420M.nc"
bool HPLCC2420M$bSpiAvail;
 uint8_t *HPLCC2420M$rambuf;
 uint8_t HPLCC2420M$ramlen;
 uint16_t HPLCC2420M$ramaddr;
static inline  






result_t HPLCC2420M$StdControl$init(void);
static inline  
#line 92
result_t HPLCC2420M$StdControl$start(void);
static inline   
#line 105
result_t HPLCC2420M$HPLCC2420$enableFIFOP(void);
static   
#line 125
uint8_t HPLCC2420M$HPLCC2420$cmd(uint8_t addr);
static   
#line 150
result_t HPLCC2420M$HPLCC2420$write(uint8_t addr, uint16_t data);
static   
#line 181
uint16_t HPLCC2420M$HPLCC2420$read(uint8_t addr);
#line 205
void __attribute((signal))   __vector_7(void);
static inline  
#line 235
void HPLCC2420M$signalRAMWr(void);
static inline   









result_t HPLCC2420M$HPLCC2420RAM$write(uint16_t addr, uint8_t length, uint8_t *buffer);
static   
# 48 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/HPLCC2420FIFO.nc"
result_t HPLCC2420FIFOM$HPLCC2420FIFO$TXFIFODone(uint8_t arg_0xa566210, uint8_t *arg_0xa566370);
static   
#line 37
result_t HPLCC2420FIFOM$HPLCC2420FIFO$RXFIFODone(uint8_t arg_0xa56fb68, uint8_t *arg_0xa56fcc8);
 
# 51 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/HPLCC2420FIFOM.nc"
bool HPLCC2420FIFOM$bSpiAvail;
 uint8_t *HPLCC2420FIFOM$txbuf;
#line 52
uint8_t *HPLCC2420FIFOM$rxbuf;
 uint8_t HPLCC2420FIFOM$txlength;
 
#line 53
uint8_t HPLCC2420FIFOM$rxlength;
static inline  
void HPLCC2420FIFOM$signalTXdone(void);
static inline  






void HPLCC2420FIFOM$signalRXdone(void);
static inline   
#line 77
result_t HPLCC2420FIFOM$HPLCC2420FIFO$writeTXFIFO(uint8_t len, uint8_t *msg);
static inline   
#line 114
result_t HPLCC2420FIFOM$HPLCC2420FIFO$readRXFIFO(uint8_t len, uint8_t *msg);
# 54 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/system/RandomLFSR.nc"
uint16_t RandomLFSR$shiftReg;
uint16_t RandomLFSR$initSeed;
uint16_t RandomLFSR$mask;
static inline   

result_t RandomLFSR$Random$init(void);
static   









uint16_t RandomLFSR$Random$rand(void);
static   
# 12 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/TimerJiffyAsync.nc"
result_t TimerJiffyAsyncM$TimerJiffyAsync$fired(void);
static   
# 148 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/Clock.nc"
result_t TimerJiffyAsyncM$Timer$setIntervalAndScale(uint8_t arg_0xa419c48, uint8_t arg_0xa419d90);
static   
#line 168
void TimerJiffyAsyncM$Timer$intDisable(void);
# 18 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/TimerJiffyAsyncM.nc"
uint32_t TimerJiffyAsyncM$jiffy;
bool TimerJiffyAsyncM$bSet;
static inline  

result_t TimerJiffyAsyncM$StdControl$init(void);
static inline  




result_t TimerJiffyAsyncM$StdControl$start(void);
static inline   
#line 44
result_t TimerJiffyAsyncM$Timer$fire(void);
static   
#line 61
result_t TimerJiffyAsyncM$TimerJiffyAsync$setOneShot(uint32_t _jiffy);
static inline   
#line 76
bool TimerJiffyAsyncM$TimerJiffyAsync$isSet(void);
static inline   



result_t TimerJiffyAsyncM$TimerJiffyAsync$stop(void);
static   
# 180 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/Clock.nc"
result_t HPLTimer2$Timer2$fire(void);
# 56 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/HPLTimer2.nc"
uint8_t HPLTimer2$set_flag;
uint8_t HPLTimer2$mscale;
#line 57
uint8_t HPLTimer2$nextScale;
#line 57
uint8_t HPLTimer2$minterval;
static   
#line 118
result_t HPLTimer2$Timer2$setIntervalAndScale(uint8_t interval, uint8_t scale);
static inline   
#line 165
void HPLTimer2$Timer2$intDisable(void);






void __attribute((interrupt))   __vector_9(void);
static  
# 75 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/ReceiveMsg.nc"
TOS_MsgPtr FramerM$ReceiveMsg$receive(TOS_MsgPtr arg_0xa3e46d8);
static   
# 55 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/ByteComm.nc"
result_t FramerM$ByteComm$txByte(uint8_t arg_0xa65dab0);
static  
# 63 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
result_t FramerM$ByteControl$init(void);
static  





result_t FramerM$ByteControl$start(void);
static  
# 67 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/BareSendMsg.nc"
result_t FramerM$BareSendMsg$sendDone(TOS_MsgPtr arg_0xa524e88, result_t arg_0xa524fd8);
static  
# 75 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/TokenReceiveMsg.nc"
TOS_MsgPtr FramerM$TokenReceiveMsg$receive(TOS_MsgPtr arg_0xa681678, uint8_t arg_0xa6817c0);
# 82 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/FramerM.nc"
enum FramerM$__nesc_unnamed4270 {
  FramerM$HDLC_QUEUESIZE = 2, 
  FramerM$HDLC_MTU = sizeof(TOS_Msg ), 
  FramerM$HDLC_FLAG_BYTE = 0x7e, 
  FramerM$HDLC_CTLESC_BYTE = 0x7d, 
  FramerM$PROTO_ACK = 64, 
  FramerM$PROTO_PACKET_ACK = 65, 
  FramerM$PROTO_PACKET_NOACK = 66, 
  FramerM$PROTO_UNKNOWN = 255
};

enum FramerM$__nesc_unnamed4271 {
  FramerM$RXSTATE_NOSYNC, 
  FramerM$RXSTATE_PROTO, 
  FramerM$RXSTATE_TOKEN, 
  FramerM$RXSTATE_INFO, 
  FramerM$RXSTATE_ESC
};

enum FramerM$__nesc_unnamed4272 {
  FramerM$TXSTATE_IDLE, 
  FramerM$TXSTATE_PROTO, 
  FramerM$TXSTATE_INFO, 
  FramerM$TXSTATE_ESC, 
  FramerM$TXSTATE_FCS1, 
  FramerM$TXSTATE_FCS2, 
  FramerM$TXSTATE_ENDFLAG, 
  FramerM$TXSTATE_FINISH, 
  FramerM$TXSTATE_ERROR
};

enum FramerM$__nesc_unnamed4273 {
  FramerM$FLAGS_TOKENPEND = 0x2, 
  FramerM$FLAGS_DATAPEND = 0x4, 
  FramerM$FLAGS_UNKNOWN = 0x8
};

TOS_Msg FramerM$gMsgRcvBuf[FramerM$HDLC_QUEUESIZE];

typedef struct FramerM$_MsgRcvEntry {
  uint8_t Proto;
  uint8_t Token;
  uint16_t Length;
  TOS_MsgPtr pMsg;
} FramerM$MsgRcvEntry_t;

FramerM$MsgRcvEntry_t FramerM$gMsgRcvTbl[FramerM$HDLC_QUEUESIZE];

uint8_t *FramerM$gpRxBuf;
uint8_t *FramerM$gpTxBuf;

uint8_t FramerM$gFlags;
 

uint8_t FramerM$gTxState;
 uint8_t FramerM$gPrevTxState;
 uint8_t FramerM$gTxProto;
 uint16_t FramerM$gTxByteCnt;
 uint16_t FramerM$gTxLength;
 uint16_t FramerM$gTxRunningCRC;


uint8_t FramerM$gRxState;
uint8_t FramerM$gRxHeadIndex;
uint8_t FramerM$gRxTailIndex;
uint16_t FramerM$gRxByteCnt;

uint16_t FramerM$gRxRunningCRC;

TOS_MsgPtr FramerM$gpTxMsg;
uint8_t FramerM$gTxTokenBuf;
uint8_t FramerM$gTxUnknownBuf;
 uint8_t FramerM$gTxEscByte;
static  
void FramerM$PacketSent(void);
static 
uint8_t FramerM$fRemapRxPos(uint8_t InPos);
static 






uint8_t FramerM$fRemapRxPos(uint8_t InPos);
static 








result_t FramerM$StartTx(void);
static inline  
#line 224
void FramerM$PacketUnknown(void);
static inline  






void FramerM$PacketRcvd(void);
static  
#line 268
void FramerM$PacketSent(void);
static 
#line 290
void FramerM$HDLCInitialize(void);
static inline  
#line 313
result_t FramerM$StdControl$init(void);
static inline  



result_t FramerM$StdControl$start(void);
static inline  
#line 350
result_t FramerM$TokenReceiveMsg$ReflectToken(uint8_t Token);
static   
#line 370
result_t FramerM$ByteComm$rxByteReady(uint8_t data, bool error, uint16_t strength);
static 
#line 495
result_t FramerM$TxArbitraryByte(uint8_t inByte);
static inline   
#line 508
result_t FramerM$ByteComm$txByteReady(bool LastByteSuccess);
static inline   
#line 581
result_t FramerM$ByteComm$txDone(void);
static  
# 75 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/ReceiveMsg.nc"
TOS_MsgPtr FramerAckM$ReceiveCombined$receive(TOS_MsgPtr arg_0xa3e46d8);
static  
# 88 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/TokenReceiveMsg.nc"
result_t FramerAckM$TokenReceiveMsg$ReflectToken(uint8_t arg_0xa681dd8);
# 72 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/system/FramerAckM.nc"
uint8_t FramerAckM$gTokenBuf;
static inline  
void FramerAckM$SendAckTask(void);
static inline  



TOS_MsgPtr FramerAckM$TokenReceiveMsg$receive(TOS_MsgPtr Msg, uint8_t token);
static inline  
#line 91
TOS_MsgPtr FramerAckM$ReceiveMsg$receive(TOS_MsgPtr Msg);
static   
# 62 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/HPLUART.nc"
result_t UARTM$HPLUART$init(void);
static   
#line 80
result_t UARTM$HPLUART$put(uint8_t arg_0xa4a6300);
static   
# 83 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/ByteComm.nc"
result_t UARTM$ByteComm$txDone(void);
static   
#line 75
result_t UARTM$ByteComm$txByteReady(bool arg_0xa67c728);
static   
#line 66
result_t UARTM$ByteComm$rxByteReady(uint8_t arg_0xa65df40, bool arg_0xa67c0a0, uint16_t arg_0xa67c1f8);
# 58 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/system/UARTM.nc"
bool UARTM$state;
static inline  
result_t UARTM$Control$init(void);
static inline  






result_t UARTM$Control$start(void);
static inline   







result_t UARTM$HPLUART$get(uint8_t data);
static   








result_t UARTM$HPLUART$putDone(void);
static   
#line 110
result_t UARTM$ByteComm$txByte(uint8_t data);
# 118 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/hardware.h"
static __inline void TOSH_SET_GREEN_LED_PIN(void)
#line 118
{
#line 118
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1B + 0x20) |= 1 << 1;
}

#line 119
static __inline void TOSH_SET_YELLOW_LED_PIN(void)
#line 119
{
#line 119
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1B + 0x20) |= 1 << 0;
}

#line 117
static __inline void TOSH_SET_RED_LED_PIN(void)
#line 117
{
#line 117
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1B + 0x20) |= 1 << 2;
}

#line 189
static __inline void TOSH_SET_FLASH_SELECT_PIN(void)
#line 189
{
#line 189
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1B + 0x20) |= 1 << 3;
}

#line 190
static __inline void TOSH_MAKE_FLASH_CLK_OUTPUT(void)
#line 190
{
#line 190
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x11 + 0x20) |= 1 << 5;
}

#line 191
static __inline void TOSH_MAKE_FLASH_OUT_OUTPUT(void)
#line 191
{
#line 191
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x11 + 0x20) |= 1 << 3;
}

#line 189
static __inline void TOSH_MAKE_FLASH_SELECT_OUTPUT(void)
#line 189
{
#line 189
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1A + 0x20) |= 1 << 3;
}

#line 130
static __inline void TOSH_CLR_SERIAL_ID_PIN(void)
#line 130
{
#line 130
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1B + 0x20) &= ~(1 << 4);
}

#line 130
static __inline void TOSH_MAKE_SERIAL_ID_INPUT(void)
#line 130
{
#line 130
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1A + 0x20) &= ~(1 << 4);
}

#line 161
static __inline void TOSH_MAKE_RADIO_CCA_INPUT(void)
#line 161
{
#line 161
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x11 + 0x20) &= ~(1 << 6);
}

#line 160
static __inline void TOSH_MAKE_CC_FIFO_INPUT(void)
#line 160
{
#line 160
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x17 + 0x20) &= ~(1 << 7);
}

#line 158
static __inline void TOSH_MAKE_CC_SFD_INPUT(void)
#line 158
{
#line 158
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x11 + 0x20) &= ~(1 << 4);
}

#line 157
static __inline void TOSH_MAKE_CC_CCA_INPUT(void)
#line 157
{
#line 157
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x11 + 0x20) &= ~(1 << 6);
}

#line 155
static __inline void TOSH_MAKE_CC_FIFOP1_INPUT(void)
#line 155
{
#line 155
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x02 + 0x20) &= ~(1 << 6);
}


static __inline void TOSH_MAKE_CC_CS_INPUT(void)
#line 159
{
#line 159
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x17 + 0x20) &= ~(1 << 0);
}

#line 153
static __inline void TOSH_MAKE_CC_VREN_OUTPUT(void)
#line 153
{
#line 153
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1A + 0x20) |= 1 << 5;
}

#line 152
static __inline void TOSH_MAKE_CC_RSTN_OUTPUT(void)
#line 152
{
#line 152
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1A + 0x20) |= 1 << 6;
}

#line 205
static __inline void TOSH_MAKE_SPI_SCK_OUTPUT(void)
#line 205
{
#line 205
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x17 + 0x20) |= 1 << 1;
}

#line 202
static __inline void TOSH_MAKE_MOSI_OUTPUT(void)
#line 202
{
#line 202
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x17 + 0x20) |= 1 << 2;
}

#line 203
static __inline void TOSH_MAKE_MISO_INPUT(void)
#line 203
{
#line 203
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x17 + 0x20) &= ~(1 << 3);
}







static __inline void TOSH_MAKE_PW0_OUTPUT(void)
#line 212
{
#line 212
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x14 + 0x20) |= 1 << 0;
}

#line 213
static __inline void TOSH_MAKE_PW1_OUTPUT(void)
#line 213
{
#line 213
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x14 + 0x20) |= 1 << 1;
}

#line 214
static __inline void TOSH_MAKE_PW2_OUTPUT(void)
#line 214
{
#line 214
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x14 + 0x20) |= 1 << 2;
}

#line 215
static __inline void TOSH_MAKE_PW3_OUTPUT(void)
#line 215
{
#line 215
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x14 + 0x20) |= 1 << 3;
}

#line 216
static __inline void TOSH_MAKE_PW4_OUTPUT(void)
#line 216
{
#line 216
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x14 + 0x20) |= 1 << 4;
}

#line 217
static __inline void TOSH_MAKE_PW5_OUTPUT(void)
#line 217
{
#line 217
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x14 + 0x20) |= 1 << 5;
}

#line 218
static __inline void TOSH_MAKE_PW6_OUTPUT(void)
#line 218
{
#line 218
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x14 + 0x20) |= 1 << 6;
}

#line 219
static __inline void TOSH_MAKE_PW7_OUTPUT(void)
#line 219
{
#line 219
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x14 + 0x20) |= 1 << 7;
}

#line 118
static __inline void TOSH_MAKE_GREEN_LED_OUTPUT(void)
#line 118
{
#line 118
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1A + 0x20) |= 1 << 1;
}

#line 119
static __inline void TOSH_MAKE_YELLOW_LED_OUTPUT(void)
#line 119
{
#line 119
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1A + 0x20) |= 1 << 0;
}

#line 117
static __inline void TOSH_MAKE_RED_LED_OUTPUT(void)
#line 117
{
#line 117
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1A + 0x20) |= 1 << 2;
}

static inline 
#line 268
void TOSH_SET_PIN_DIRECTIONS(void )
{







  TOSH_MAKE_RED_LED_OUTPUT();
  TOSH_MAKE_YELLOW_LED_OUTPUT();
  TOSH_MAKE_GREEN_LED_OUTPUT();


  TOSH_MAKE_PW7_OUTPUT();
  TOSH_MAKE_PW6_OUTPUT();
  TOSH_MAKE_PW5_OUTPUT();
  TOSH_MAKE_PW4_OUTPUT();
  TOSH_MAKE_PW3_OUTPUT();
  TOSH_MAKE_PW2_OUTPUT();
  TOSH_MAKE_PW1_OUTPUT();
  TOSH_MAKE_PW0_OUTPUT();


  TOSH_MAKE_MISO_INPUT();
  TOSH_MAKE_MOSI_OUTPUT();
  TOSH_MAKE_SPI_SCK_OUTPUT();
  TOSH_MAKE_CC_RSTN_OUTPUT();
  TOSH_MAKE_CC_VREN_OUTPUT();
  TOSH_MAKE_CC_CS_INPUT();
  TOSH_MAKE_CC_FIFOP1_INPUT();
  TOSH_MAKE_CC_CCA_INPUT();
  TOSH_MAKE_CC_SFD_INPUT();
  TOSH_MAKE_CC_FIFO_INPUT();

  TOSH_MAKE_RADIO_CCA_INPUT();






  TOSH_MAKE_SERIAL_ID_INPUT();
  TOSH_CLR_SERIAL_ID_PIN();

  TOSH_MAKE_FLASH_SELECT_OUTPUT();
  TOSH_MAKE_FLASH_OUT_OUTPUT();
  TOSH_MAKE_FLASH_CLK_OUTPUT();
  TOSH_SET_FLASH_SELECT_PIN();

  TOSH_SET_RED_LED_PIN();
  TOSH_SET_YELLOW_LED_PIN();
  TOSH_SET_GREEN_LED_PIN();
}

static inline  
# 57 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/avrmote/HPLInit.nc"
result_t HPLInit$init(void)
#line 57
{
  TOSH_SET_PIN_DIRECTIONS();
  return SUCCESS;
}

# 47 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/system/RealMain.nc"
inline static  result_t RealMain$hardwareInit(void){
#line 47
  unsigned char result;
#line 47

#line 47
  result = HPLInit$init();
#line 47

#line 47
  return result;
#line 47
}
#line 47
static inline  
# 75 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/mica2/HPLPotC.nc"
result_t HPLPotC$Pot$finalise(void)
#line 75
{


  return SUCCESS;
}

# 74 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/HPLPot.nc"
inline static  result_t PotM$HPLPot$finalise(void){
#line 74
  unsigned char result;
#line 74

#line 74
  result = HPLPotC$Pot$finalise();
#line 74

#line 74
  return result;
#line 74
}
#line 74
static inline  
# 66 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/mica2/HPLPotC.nc"
result_t HPLPotC$Pot$increase(void)
#line 66
{





  return SUCCESS;
}

# 67 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/HPLPot.nc"
inline static  result_t PotM$HPLPot$increase(void){
#line 67
  unsigned char result;
#line 67

#line 67
  result = HPLPotC$Pot$increase();
#line 67

#line 67
  return result;
#line 67
}
#line 67
static inline  
# 57 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/mica2/HPLPotC.nc"
result_t HPLPotC$Pot$decrease(void)
#line 57
{





  return SUCCESS;
}

# 59 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/HPLPot.nc"
inline static  result_t PotM$HPLPot$decrease(void){
#line 59
  unsigned char result;
#line 59

#line 59
  result = HPLPotC$Pot$decrease();
#line 59

#line 59
  return result;
#line 59
}
#line 59
static inline 
# 93 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/system/PotM.nc"
void PotM$setPot(uint8_t value)
#line 93
{
  uint8_t i;

#line 95
  for (i = 0; i < 151; i++) 
    PotM$HPLPot$decrease();

  for (i = 0; i < value; i++) 
    PotM$HPLPot$increase();

  PotM$HPLPot$finalise();

  PotM$potSetting = value;
}

static inline  result_t PotM$Pot$init(uint8_t initialSetting)
#line 106
{
  PotM$setPot(initialSetting);
  return SUCCESS;
}

# 78 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/Pot.nc"
inline static  result_t RealMain$Pot$init(uint8_t arg_0xa38f058){
#line 78
  unsigned char result;
#line 78

#line 78
  result = PotM$Pot$init(arg_0xa38f058);
#line 78

#line 78
  return result;
#line 78
}
#line 78
static inline 
# 83 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/system/sched.c"
void TOSH_sched_init(void )
{
  TOSH_sched_free = 0;
  TOSH_sched_full = 0;
}

static inline 
# 120 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/system/tos.h"
result_t rcombine(result_t r1, result_t r2)



{
  return r1 == FAIL ? FAIL : r2;
}

# 59 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/HPLCC2420.nc"
inline static   uint8_t CC2420ControlM$HPLChipcon$write(uint8_t arg_0xa56b0b0, uint16_t arg_0xa56b200){
#line 59
  unsigned char result;
#line 59

#line 59
  result = HPLCC2420M$HPLCC2420$write(arg_0xa56b0b0, arg_0xa56b200);
#line 59

#line 59
  return result;
#line 59
}
#line 59
static inline   
# 324 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/CC2420ControlM.nc"
result_t CC2420ControlM$CC2420Control$enableAutoAck(void)
#line 324
{
  CC2420ControlM$gCurrentParameters[CP_MDMCTRL0] |= 1 << 4;
  return CC2420ControlM$HPLChipcon$write(0x11, CC2420ControlM$gCurrentParameters[CP_MDMCTRL0]);
}

# 173 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/CC2420Control.nc"
inline static   result_t CC2420RadioM$CC2420Control$enableAutoAck(void){
#line 173
  unsigned char result;
#line 173

#line 173
  result = CC2420ControlM$CC2420Control$enableAutoAck();
#line 173

#line 173
  return result;
#line 173
}
#line 173
static inline   
# 334 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/CC2420ControlM.nc"
result_t CC2420ControlM$CC2420Control$enableAddrDecode(void)
#line 334
{
  CC2420ControlM$gCurrentParameters[CP_MDMCTRL0] |= 1 << 11;
  return CC2420ControlM$HPLChipcon$write(0x11, CC2420ControlM$gCurrentParameters[CP_MDMCTRL0]);
}

# 187 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/CC2420Control.nc"
inline static   result_t CC2420RadioM$CC2420Control$enableAddrDecode(void){
#line 187
  unsigned char result;
#line 187

#line 187
  result = CC2420ControlM$CC2420Control$enableAddrDecode();
#line 187

#line 187
  return result;
#line 187
}
#line 187
static inline   
# 455 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/CC2420RadioM.nc"
void CC2420RadioM$MacControl$enableAck(void)
#line 455
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 456
    CC2420RadioM$bAckEnable = TRUE;
#line 456
    __nesc_atomic_end(__nesc_atomic); }
  CC2420RadioM$CC2420Control$enableAddrDecode();
  CC2420RadioM$CC2420Control$enableAutoAck();
}

static inline   
# 59 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/system/RandomLFSR.nc"
result_t RandomLFSR$Random$init(void)
#line 59
{
  {
  }
#line 60
  ;
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 61
    {
      RandomLFSR$shiftReg = 119 * 119 * (TOS_LOCAL_ADDRESS + 1);
      RandomLFSR$initSeed = RandomLFSR$shiftReg;
      RandomLFSR$mask = 137 * 29 * (TOS_LOCAL_ADDRESS + 1);
    }
#line 65
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 57 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/Random.nc"
inline static   result_t CC2420RadioM$Random$init(void){
#line 57
  unsigned char result;
#line 57

#line 57
  result = RandomLFSR$Random$init();
#line 57

#line 57
  return result;
#line 57
}
#line 57
static inline  
# 22 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/TimerJiffyAsyncM.nc"
result_t TimerJiffyAsyncM$StdControl$init(void)
{

  return SUCCESS;
}

# 63 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
inline static  result_t CC2420RadioM$TimerControl$init(void){
#line 63
  unsigned char result;
#line 63

#line 63
  result = TimerJiffyAsyncM$StdControl$init();
#line 63

#line 63
  return result;
#line 63
}
#line 63
# 159 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/hardware.h"
static __inline void TOSH_MAKE_CC_CS_OUTPUT(void)
#line 159
{
#line 159
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x17 + 0x20) |= 1 << 0;
}

static inline  
# 63 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/HPLCC2420M.nc"
result_t HPLCC2420M$StdControl$init(void)
#line 63
{

  HPLCC2420M$bSpiAvail = TRUE;
  TOSH_MAKE_MISO_INPUT();
  TOSH_MAKE_MOSI_OUTPUT();
  TOSH_MAKE_SPI_SCK_OUTPUT();
  TOSH_MAKE_CC_RSTN_OUTPUT();
  TOSH_MAKE_CC_VREN_OUTPUT();
  TOSH_MAKE_CC_CS_OUTPUT();
  TOSH_MAKE_CC_FIFOP1_INPUT();
  TOSH_MAKE_CC_CCA_INPUT();
  TOSH_MAKE_CC_SFD_INPUT();
  TOSH_MAKE_CC_FIFO_INPUT();
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 76
    {
      TOSH_MAKE_SPI_SCK_OUTPUT();
      TOSH_MAKE_MISO_INPUT();
      TOSH_MAKE_MOSI_OUTPUT();
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0E + 0x20) |= 1 << 0;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0D + 0x20) |= 1 << 4;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0D + 0x20) &= ~(1 << 3);
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0D + 0x20) &= ~(1 << 2);
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0D + 0x20) &= ~(1 << 1);
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0D + 0x20) &= ~(1 << 0);

      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0D + 0x20) |= 1 << 6;
    }
#line 88
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 63 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
inline static  result_t CC2420ControlM$HPLChipconControl$init(void){
#line 63
  unsigned char result;
#line 63

#line 63
  result = HPLCC2420M$StdControl$init();
#line 63

#line 63
  return result;
#line 63
}
#line 63
static inline  
# 102 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/CC2420ControlM.nc"
result_t CC2420ControlM$StdControl$init(void)
#line 102
{

  CC2420ControlM$HPLChipconControl$init();


  CC2420ControlM$gCurrentParameters[CP_MAIN] = 0xf800;
  CC2420ControlM$gCurrentParameters[CP_MDMCTRL0] = ((((0 << 11) | (
  2 << 8)) | (3 << 6)) | (
  1 << 5)) | (2 << 0);

  CC2420ControlM$gCurrentParameters[CP_MDMCTRL1] = 20 << 6;

  CC2420ControlM$gCurrentParameters[CP_RSSI] = 0xE080;
  CC2420ControlM$gCurrentParameters[CP_SYNCWORD] = 0xA70F;
  CC2420ControlM$gCurrentParameters[CP_TXCTRL] = ((((1 << 14) | (
  1 << 13)) | (3 << 6)) | (
  1 << 5)) | (0x1f << 0);

  CC2420ControlM$gCurrentParameters[CP_RXCTRL0] = (((((1 << 12) | (
  2 << 8)) | (3 << 6)) | (
  2 << 4)) | (1 << 2)) | (
  1 << 0);

  CC2420ControlM$gCurrentParameters[CP_RXCTRL1] = (((((1 << 11) | (
  1 << 9)) | (1 << 6)) | (
  1 << 4)) | (1 << 2)) | (
  2 << 0);

  CC2420ControlM$gCurrentParameters[CP_FSCTRL] = (1 << 14) | ((
  357 + 5 * (11 - 11)) << 0);

  CC2420ControlM$gCurrentParameters[CP_SECCTRL0] = (((1 << 8) | (
  1 << 7)) | (1 << 6)) | (
  1 << 2);

  CC2420ControlM$gCurrentParameters[CP_SECCTRL1] = 0;
  CC2420ControlM$gCurrentParameters[CP_BATTMON] = 0;



  CC2420ControlM$gCurrentParameters[CP_IOCFG0] = (127 << 0) | (
  1 << 9);

  CC2420ControlM$gCurrentParameters[CP_IOCFG1] = 0;

  return SUCCESS;
}

# 63 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
inline static  result_t CC2420RadioM$CC2420StdControl$init(void){
#line 63
  unsigned char result;
#line 63

#line 63
  result = CC2420ControlM$StdControl$init();
#line 63

#line 63
  return result;
#line 63
}
#line 63
static inline  
# 175 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/CC2420RadioM.nc"
result_t CC2420RadioM$StdControl$init(void)
#line 175
{

  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 177
    {
      CC2420RadioM$stateRadio = CC2420RadioM$DISABLED_STATE;
      CC2420RadioM$currentDSN = 0;
      CC2420RadioM$bAckEnable = FALSE;
      CC2420RadioM$rxbufptr = &CC2420RadioM$RxBuf;
      CC2420RadioM$rxbufptr->length = 0;
      CC2420RadioM$rxlength = MSG_DATA_SIZE - 2;
    }
#line 184
    __nesc_atomic_end(__nesc_atomic); }

  CC2420RadioM$CC2420StdControl$init();
  CC2420RadioM$TimerControl$init();
  CC2420RadioM$Random$init();
  CC2420RadioM$MacControl$enableAck();
  CC2420RadioM$LocalAddr = TOS_LOCAL_ADDRESS;

  return SUCCESS;
}

# 63 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
inline static  result_t AMStandard$RadioControl$init(void){
#line 63
  unsigned char result;
#line 63

#line 63
  result = CC2420RadioM$StdControl$init();
#line 63

#line 63
  return result;
#line 63
}
#line 63
static inline  
# 60 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/system/UARTM.nc"
result_t UARTM$Control$init(void)
#line 60
{
  {
  }
#line 61
  ;
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 62
    {
      UARTM$state = FALSE;
    }
#line 64
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 63 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
inline static  result_t FramerM$ByteControl$init(void){
#line 63
  unsigned char result;
#line 63

#line 63
  result = UARTM$Control$init();
#line 63

#line 63
  return result;
#line 63
}
#line 63
static inline  
# 313 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/FramerM.nc"
result_t FramerM$StdControl$init(void)
#line 313
{
  FramerM$HDLCInitialize();
  return FramerM$ByteControl$init();
}

# 63 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
inline static  result_t AMStandard$UARTControl$init(void){
#line 63
  unsigned char result;
#line 63

#line 63
  result = FramerM$StdControl$init();
#line 63

#line 63
  return result;
#line 63
}
#line 63
inline static  result_t AMStandard$TimerControl$init(void){
#line 63
  unsigned char result;
#line 63

#line 63
  result = TimerM$StdControl$init();
#line 63

#line 63
  return result;
#line 63
}
#line 63
static inline  
# 87 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/system/AMStandard.nc"
bool AMStandard$Control$init(void)
#line 87
{
  result_t ok1;
#line 88
  result_t ok2;

  AMStandard$TimerControl$init();
  ok1 = AMStandard$UARTControl$init();
  ok2 = AMStandard$RadioControl$init();

  AMStandard$state = FALSE;
  AMStandard$lastCount = 0;
  AMStandard$counter = 0;
  {
  }
#line 97
  ;

  return rcombine(ok1, ok2);
}

# 63 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
inline static  result_t testRFIDM$CommControl$init(void){
#line 63
  unsigned char result;
#line 63

#line 63
  result = AMStandard$Control$init();
#line 63

#line 63
  return result;
#line 63
}
#line 63
static inline  
# 49 "SCSuartDBGM.nc"
result_t SCSuartDBGM$StdControl$init(void)
#line 49
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
    {
      SCSuartDBGM$state = 0;
      SCSuartDBGM$buff_start = 0;
      SCSuartDBGM$buff_end = 0;
    }
#line 55
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 63 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
inline static  result_t testRFIDM$SCSuartSTD$init(void){
#line 63
  unsigned char result;
#line 63

#line 63
  result = SCSuartDBGM$StdControl$init();
#line 63

#line 63
  return result;
#line 63
}
#line 63
static inline   
# 58 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/system/LedsC.nc"
result_t LedsC$Leds$init(void)
#line 58
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 59
    {
      LedsC$ledsOn = 0;
      {
      }
#line 61
      ;
      TOSH_MAKE_RED_LED_OUTPUT();
      TOSH_MAKE_YELLOW_LED_OUTPUT();
      TOSH_MAKE_GREEN_LED_OUTPUT();
      TOSH_SET_RED_LED_PIN();
      TOSH_SET_YELLOW_LED_PIN();
      TOSH_SET_GREEN_LED_PIN();
    }
#line 68
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 56 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/Leds.nc"
inline static   result_t testRFIDM$Leds$init(void){
#line 56
  unsigned char result;
#line 56

#line 56
  result = LedsC$Leds$init();
#line 56

#line 56
  return result;
#line 56
}
#line 56
static inline  
# 22 "testRFIDM.nc"
result_t testRFIDM$StdControl$init(void)
#line 22
{
  testRFIDM$Leds$init();
  testRFIDM$SCSuartSTD$init();
  testRFIDM$CommControl$init();

  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
    {
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)0x98 = 0;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)0x99 = 23;


      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)0x9B = 1 << 1;


      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)0x9D = (1 << 2) | (1 << 1);


      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)0x9A = (((1 << 7) | (1 << 6)) | (1 << 4)) | (1 << 3);

      testRFIDM$Inc_Flag = 1;
    }
#line 42
    __nesc_atomic_end(__nesc_atomic); }

  return SUCCESS;
}

# 63 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
inline static  result_t RealMain$StdControl$init(void){
#line 63
  unsigned char result;
#line 63

#line 63
  result = testRFIDM$StdControl$init();
#line 63
  result = rcombine(result, TimerM$StdControl$init());
#line 63

#line 63
  return result;
#line 63
}
#line 63
static inline   
# 155 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/HPLClock.nc"
result_t HPLClock$Clock$setRate(char interval, char scale)
#line 155
{
  scale &= 0x7;
  scale |= 0x8;
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 158
    {
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x37 + 0x20) &= ~(1 << 0);
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x37 + 0x20) &= ~(1 << 1);
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x30 + 0x20) |= 1 << 3;


      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x33 + 0x20) = scale;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x32 + 0x20) = 0;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x31 + 0x20) = interval;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x37 + 0x20) |= 1 << 1;
    }
#line 168
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 96 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/Clock.nc"
inline static   result_t TimerM$Clock$setRate(char arg_0xa42fbf8, char arg_0xa42fd38){
#line 96
  unsigned char result;
#line 96

#line 96
  result = HPLClock$Clock$setRate(arg_0xa42fbf8, arg_0xa42fd38);
#line 96

#line 96
  return result;
#line 96
}
#line 96
static inline  
# 82 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/system/TimerM.nc"
result_t TimerM$StdControl$start(void)
#line 82
{
  return SUCCESS;
}

static inline 
# 133 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/system/tos.h"
result_t rcombine4(result_t r1, result_t r2, result_t r3, 
result_t r4)
{
  return rcombine(r1, rcombine(r2, rcombine(r3, r4)));
}

# 41 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/PowerManagement.nc"
inline static   uint8_t AMStandard$PowerManagement$adjustPower(void){
#line 41
  unsigned char result;
#line 41

#line 41
  result = HPLPowerManagementM$PowerManagement$adjustPower();
#line 41

#line 41
  return result;
#line 41
}
#line 41
inline static   uint8_t TimerM$PowerManagement$adjustPower(void){
#line 41
  unsigned char result;
#line 41

#line 41
  result = HPLPowerManagementM$PowerManagement$adjustPower();
#line 41

#line 41
  return result;
#line 41
}
#line 41
static inline   
# 93 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/HPLClock.nc"
void HPLClock$Clock$setInterval(uint8_t value)
#line 93
{
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x31 + 0x20) = value;
}

# 105 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/Clock.nc"
inline static   void TimerM$Clock$setInterval(uint8_t arg_0xa4188c8){
#line 105
  HPLClock$Clock$setInterval(arg_0xa4188c8);
#line 105
}
#line 105
static inline   
# 140 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/HPLClock.nc"
uint8_t HPLClock$Clock$readCounter(void)
#line 140
{
  return * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x32 + 0x20);
}

# 153 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/Clock.nc"
inline static   uint8_t TimerM$Clock$readCounter(void){
#line 153
  unsigned char result;
#line 153

#line 153
  result = HPLClock$Clock$readCounter();
#line 153

#line 153
  return result;
#line 153
}
#line 153
static inline  
# 93 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/system/TimerM.nc"
result_t TimerM$Timer$start(uint8_t id, char type, 
uint32_t interval)
#line 94
{
  uint8_t diff;

#line 96
  if (id >= NUM_TIMERS) {
#line 96
    return FAIL;
    }
#line 97
  if (type > 1) {
#line 97
    return FAIL;
    }
#line 98
  TimerM$mTimerList[id].ticks = interval;
  TimerM$mTimerList[id].type = type;

  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 101
    {
      diff = TimerM$Clock$readCounter();
      interval += diff;
      TimerM$mTimerList[id].ticksLeft = interval;
      TimerM$mState |= 0x1L << id;
      if (interval < TimerM$mInterval) {
          TimerM$mInterval = interval;
          TimerM$Clock$setInterval(TimerM$mInterval);
          TimerM$setIntervalFlag = 0;
          TimerM$PowerManagement$adjustPower();
        }
    }
#line 112
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 59 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/Timer.nc"
inline static  result_t AMStandard$ActivityTimer$start(char arg_0xa3c6b70, uint32_t arg_0xa3c6cc8){
#line 59
  unsigned char result;
#line 59

#line 59
  result = TimerM$Timer$start(1, arg_0xa3c6b70, arg_0xa3c6cc8);
#line 59

#line 59
  return result;
#line 59
}
#line 59
static inline   
# 105 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/HPLCC2420M.nc"
result_t HPLCC2420M$HPLCC2420$enableFIFOP(void)
#line 105
{
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x3A + 0x20) &= ~(1 << 4);

  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x3A + 0x20) |= 1 << 5;
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x39 + 0x20) |= 1 << 6;
  return SUCCESS;
}

# 42 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/HPLCC2420.nc"
inline static   result_t CC2420RadioM$HPLChipcon$enableFIFOP(void){
#line 42
  unsigned char result;
#line 42

#line 42
  result = HPLCC2420M$HPLCC2420$enableFIFOP();
#line 42

#line 42
  return result;
#line 42
}
#line 42










inline static   uint8_t CC2420ControlM$HPLChipcon$cmd(uint8_t arg_0xa56ac58){
#line 52
  unsigned char result;
#line 52

#line 52
  result = HPLCC2420M$HPLCC2420$cmd(arg_0xa56ac58);
#line 52

#line 52
  return result;
#line 52
}
#line 52
static inline   
# 263 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/CC2420ControlM.nc"
result_t CC2420ControlM$CC2420Control$RxMode(void)
#line 263
{
  CC2420ControlM$HPLChipcon$cmd(0x03);
  return SUCCESS;
}

# 144 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/CC2420Control.nc"
inline static   result_t CC2420RadioM$CC2420Control$RxMode(void){
#line 144
  unsigned char result;
#line 144

#line 144
  result = CC2420ControlM$CC2420Control$RxMode();
#line 144

#line 144
  return result;
#line 144
}
#line 144
static inline   
# 204 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/CC2420ControlM.nc"
result_t CC2420ControlM$CC2420Control$TunePreset(uint8_t chnl)
#line 204
{
  int fsctrl;
  uint8_t status;

  fsctrl = 357 + 5 * (chnl - 11);
  CC2420ControlM$gCurrentParameters[CP_FSCTRL] = (CC2420ControlM$gCurrentParameters[CP_FSCTRL] & 0xfc00) | (fsctrl << 0);
  status = CC2420ControlM$HPLChipcon$write(0x18, CC2420ControlM$gCurrentParameters[CP_FSCTRL]);


  if (status & (1 << 6)) {
    CC2420ControlM$HPLChipcon$cmd(0x03);
    }
#line 215
  return SUCCESS;
}

static inline   
#line 357
result_t CC2420ControlM$HPLChipconRAM$writeDone(uint16_t addr, uint8_t length, uint8_t *buffer)
#line 357
{
  return SUCCESS;
}

# 49 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/HPLCC2420RAM.nc"
inline static   result_t HPLCC2420M$HPLCC2420RAM$writeDone(uint16_t arg_0xa5b47a8, uint8_t arg_0xa5b48f0, uint8_t *arg_0xa5b4a50){
#line 49
  unsigned char result;
#line 49

#line 49
  result = CC2420ControlM$HPLChipconRAM$writeDone(arg_0xa5b47a8, arg_0xa5b48f0, arg_0xa5b4a50);
#line 49

#line 49
  return result;
#line 49
}
#line 49
static inline  
# 235 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/HPLCC2420M.nc"
void HPLCC2420M$signalRAMWr(void)
#line 235
{
  HPLCC2420M$HPLCC2420RAM$writeDone(HPLCC2420M$ramaddr, HPLCC2420M$ramlen, HPLCC2420M$rambuf);
}

# 159 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/hardware.h"
static __inline void TOSH_CLR_CC_CS_PIN(void)
#line 159
{
#line 159
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x18 + 0x20) &= ~(1 << 0);
}

static inline   
# 246 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/HPLCC2420M.nc"
result_t HPLCC2420M$HPLCC2420RAM$write(uint16_t addr, uint8_t length, uint8_t *buffer)
#line 246
{
  uint8_t i = 0;
  uint8_t status;

  if (!HPLCC2420M$bSpiAvail) {
    return FALSE;
    }
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 253
    {
      HPLCC2420M$bSpiAvail = FALSE;
      HPLCC2420M$ramaddr = addr;
      HPLCC2420M$ramlen = length;
      HPLCC2420M$rambuf = buffer;
      TOSH_CLR_CC_CS_PIN();
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0F + 0x20) = (HPLCC2420M$ramaddr & 0x7F) | 0x80;
      while (!(* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0E + 0x20) & 0x80)) {
        }
#line 260
      ;
      status = * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0F + 0x20);
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0F + 0x20) = (HPLCC2420M$ramaddr >> 1) & 0xC0;
      while (!(* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0E + 0x20) & 0x80)) {
        }
#line 263
      ;
      status = * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0F + 0x20);

      for (i = 0; i < HPLCC2420M$ramlen; i++) {
          * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0F + 0x20) = HPLCC2420M$rambuf[i];

          while (!(* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0E + 0x20) & 0x80)) {
            }
#line 269
          ;
        }
    }
#line 271
    __nesc_atomic_end(__nesc_atomic); }
  HPLCC2420M$bSpiAvail = TRUE;
  return TOS_post(HPLCC2420M$signalRAMWr);
}

# 47 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/HPLCC2420RAM.nc"
inline static   result_t CC2420ControlM$HPLChipconRAM$write(uint16_t arg_0xa5b4080, uint8_t arg_0xa5b41c8, uint8_t *arg_0xa5b4328){
#line 47
  unsigned char result;
#line 47

#line 47
  result = HPLCC2420M$HPLCC2420RAM$write(arg_0xa5b4080, arg_0xa5b41c8, arg_0xa5b4328);
#line 47

#line 47
  return result;
#line 47
}
#line 47
static 
# 10 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/byteorder.h"
__inline int is_host_lsb(void)
{
  const uint8_t n[2] = { 1, 0 };

#line 13
  return * (uint16_t *)n == 1;
}

static __inline uint16_t toLSB16(uint16_t a)
{
  return is_host_lsb() ? a : (a << 8) | (a >> 8);
}

static inline  
# 344 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/CC2420ControlM.nc"
result_t CC2420ControlM$CC2420Control$setShortAddress(uint16_t addr)
#line 344
{
  addr = toLSB16(addr);
  return CC2420ControlM$HPLChipconRAM$write(0x16A, 2, (uint8_t *)&addr);
}

# 66 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/HPLCC2420.nc"
inline static   uint16_t CC2420ControlM$HPLChipcon$read(uint8_t arg_0xa56b6a8){
#line 66
  unsigned int result;
#line 66

#line 66
  result = HPLCC2420M$HPLCC2420$read(arg_0xa56b6a8);
#line 66

#line 66
  return result;
#line 66
}
#line 66
static inline 
# 70 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/CC2420ControlM.nc"
bool CC2420ControlM$SetRegs(void)
#line 70
{
  uint16_t data;

  CC2420ControlM$HPLChipcon$write(0x10, CC2420ControlM$gCurrentParameters[CP_MAIN]);
  CC2420ControlM$HPLChipcon$write(0x11, CC2420ControlM$gCurrentParameters[CP_MDMCTRL0]);
  data = CC2420ControlM$HPLChipcon$read(0x11);
  if (data != CC2420ControlM$gCurrentParameters[CP_MDMCTRL0]) {
#line 76
    return FALSE;
    }
  CC2420ControlM$HPLChipcon$write(0x12, CC2420ControlM$gCurrentParameters[CP_MDMCTRL1]);
  CC2420ControlM$HPLChipcon$write(0x13, CC2420ControlM$gCurrentParameters[CP_RSSI]);
  CC2420ControlM$HPLChipcon$write(0x14, CC2420ControlM$gCurrentParameters[CP_SYNCWORD]);
  CC2420ControlM$HPLChipcon$write(0x15, CC2420ControlM$gCurrentParameters[CP_TXCTRL]);
  CC2420ControlM$HPLChipcon$write(0x16, CC2420ControlM$gCurrentParameters[CP_RXCTRL0]);
  CC2420ControlM$HPLChipcon$write(0x17, CC2420ControlM$gCurrentParameters[CP_RXCTRL1]);
  CC2420ControlM$HPLChipcon$write(0x18, CC2420ControlM$gCurrentParameters[CP_FSCTRL]);

  CC2420ControlM$HPLChipcon$write(0x19, CC2420ControlM$gCurrentParameters[CP_SECCTRL0]);
  CC2420ControlM$HPLChipcon$write(0x1A, CC2420ControlM$gCurrentParameters[CP_SECCTRL1]);
  CC2420ControlM$HPLChipcon$write(0x1C, CC2420ControlM$gCurrentParameters[CP_IOCFG0]);
  CC2420ControlM$HPLChipcon$write(0x1D, CC2420ControlM$gCurrentParameters[CP_IOCFG1]);

  CC2420ControlM$HPLChipcon$cmd(0x09);
  CC2420ControlM$HPLChipcon$cmd(0x08);

  return TRUE;
}

static inline   
#line 288
result_t CC2420ControlM$CC2420Control$OscillatorOn(void)
#line 288
{
  uint16_t i;
  uint8_t status;
  bool bXoscOn = FALSE;

  i = 0;

  CC2420ControlM$HPLChipcon$cmd(0x01);

  while (i < 200 && bXoscOn == FALSE) {
      status = CC2420ControlM$HPLChipcon$cmd(0x00);
      status = status & (1 << 6);
      if (status) {
#line 300
        bXoscOn = TRUE;
        }
#line 301
      i++;
    }

  if (!bXoscOn) {
#line 304
    return FAIL;
    }
#line 305
  return SUCCESS;
}

static inline 
# 135 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/avrmote/avrhardware.h"
void TOSH_wait(void)
{
   __asm volatile ("nop");
   __asm volatile ("nop");}

# 152 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/hardware.h"
static __inline void TOSH_SET_CC_RSTN_PIN(void)
#line 152
{
#line 152
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1B + 0x20) |= 1 << 6;
}

#line 152
static __inline void TOSH_CLR_CC_RSTN_PIN(void)
#line 152
{
#line 152
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1B + 0x20) &= ~(1 << 6);
}

static 
#line 101
void __inline TOSH_uwait(int u_sec)
#line 101
{
  while (u_sec > 0) {
       __asm volatile ("nop");
       __asm volatile ("nop");
       __asm volatile ("nop");
       __asm volatile ("nop");
       __asm volatile ("nop");
       __asm volatile ("nop");
       __asm volatile ("nop");
       __asm volatile ("nop");
      u_sec--;
    }
}

#line 153
static __inline void TOSH_SET_CC_VREN_PIN(void)
#line 153
{
#line 153
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1B + 0x20) |= 1 << 5;
}

static inline   
# 313 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/CC2420ControlM.nc"
result_t CC2420ControlM$CC2420Control$VREFOn(void)
#line 313
{
  TOSH_SET_CC_VREN_PIN();
  TOSH_uwait(600);
  return SUCCESS;
}

static inline  
# 92 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/HPLCC2420M.nc"
result_t HPLCC2420M$StdControl$start(void)
#line 92
{
#line 92
  return SUCCESS;
}

# 70 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
inline static  result_t CC2420ControlM$HPLChipconControl$start(void){
#line 70
  unsigned char result;
#line 70

#line 70
  result = HPLCC2420M$StdControl$start();
#line 70

#line 70
  return result;
#line 70
}
#line 70
static inline  
# 173 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/CC2420ControlM.nc"
result_t CC2420ControlM$StdControl$start(void)
#line 173
{
  result_t status;

  CC2420ControlM$HPLChipconControl$start();

  CC2420ControlM$CC2420Control$VREFOn();

  TOSH_CLR_CC_RSTN_PIN();
  TOSH_wait();
  TOSH_SET_CC_RSTN_PIN();
  TOSH_wait();


  status = CC2420ControlM$CC2420Control$OscillatorOn();

  status = CC2420ControlM$SetRegs() && status;
  status = status && CC2420ControlM$CC2420Control$setShortAddress(TOS_LOCAL_ADDRESS);
  status = status && CC2420ControlM$CC2420Control$TunePreset(11);

  return status;
}

# 70 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
inline static  result_t CC2420RadioM$CC2420StdControl$start(void){
#line 70
  unsigned char result;
#line 70

#line 70
  result = CC2420ControlM$StdControl$start();
#line 70

#line 70
  return result;
#line 70
}
#line 70
static inline  
# 28 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/TimerJiffyAsyncM.nc"
result_t TimerJiffyAsyncM$StdControl$start(void)
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 30
    TimerJiffyAsyncM$bSet = FALSE;
#line 30
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 70 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
inline static  result_t CC2420RadioM$TimerControl$start(void){
#line 70
  unsigned char result;
#line 70

#line 70
  result = TimerJiffyAsyncM$StdControl$start();
#line 70

#line 70
  return result;
#line 70
}
#line 70
static inline  
# 203 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/CC2420RadioM.nc"
result_t CC2420RadioM$StdControl$start(void)
#line 203
{
  uint8_t chkstateRadio;

  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 206
    chkstateRadio = CC2420RadioM$stateRadio;
#line 206
    __nesc_atomic_end(__nesc_atomic); }

  if (chkstateRadio == CC2420RadioM$DISABLED_STATE) {
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 209
        {
          CC2420RadioM$countRetry = 0;
          CC2420RadioM$rxbufptr->length = 0;

          CC2420RadioM$TimerControl$start();
          CC2420RadioM$CC2420StdControl$start();
          CC2420RadioM$CC2420Control$RxMode();
          CC2420RadioM$HPLChipcon$enableFIFOP();

          CC2420RadioM$stateRadio = CC2420RadioM$IDLE_STATE;
        }
#line 219
        __nesc_atomic_end(__nesc_atomic); }
    }
  return SUCCESS;
}

# 70 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
inline static  result_t AMStandard$RadioControl$start(void){
#line 70
  unsigned char result;
#line 70

#line 70
  result = CC2420RadioM$StdControl$start();
#line 70

#line 70
  return result;
#line 70
}
#line 70
# 62 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/HPLUART.nc"
inline static   result_t UARTM$HPLUART$init(void){
#line 62
  unsigned char result;
#line 62

#line 62
  result = HPLUART0M$UART$init();
#line 62

#line 62
  return result;
#line 62
}
#line 62
static inline  
# 68 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/system/UARTM.nc"
result_t UARTM$Control$start(void)
#line 68
{
  return UARTM$HPLUART$init();
}

# 70 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
inline static  result_t FramerM$ByteControl$start(void){
#line 70
  unsigned char result;
#line 70

#line 70
  result = UARTM$Control$start();
#line 70

#line 70
  return result;
#line 70
}
#line 70
static inline  
# 318 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/FramerM.nc"
result_t FramerM$StdControl$start(void)
#line 318
{
  FramerM$HDLCInitialize();
  return FramerM$ByteControl$start();
}

# 70 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
inline static  result_t AMStandard$UARTControl$start(void){
#line 70
  unsigned char result;
#line 70

#line 70
  result = FramerM$StdControl$start();
#line 70

#line 70
  return result;
#line 70
}
#line 70
inline static  result_t AMStandard$TimerControl$start(void){
#line 70
  unsigned char result;
#line 70

#line 70
  result = TimerM$StdControl$start();
#line 70

#line 70
  return result;
#line 70
}
#line 70
static inline  
# 103 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/system/AMStandard.nc"
bool AMStandard$Control$start(void)
#line 103
{
  result_t ok0 = AMStandard$TimerControl$start();
  result_t ok1 = AMStandard$UARTControl$start();
  result_t ok2 = AMStandard$RadioControl$start();
  result_t ok3 = AMStandard$ActivityTimer$start(TIMER_REPEAT, 1000);



  AMStandard$state = FALSE;

  AMStandard$PowerManagement$adjustPower();

  return rcombine4(ok0, ok1, ok2, ok3);
}

# 70 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
inline static  result_t testRFIDM$CommControl$start(void){
#line 70
  unsigned char result;
#line 70

#line 70
  result = AMStandard$Control$start();
#line 70

#line 70
  return result;
#line 70
}
#line 70
# 62 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/HPLUART.nc"
inline static   result_t SCSuartDBGM$HPLUART$init(void){
#line 62
  unsigned char result;
#line 62

#line 62
  result = HPLUART0M$UART$init();
#line 62

#line 62
  return result;
#line 62
}
#line 62
static inline  
# 59 "SCSuartDBGM.nc"
result_t SCSuartDBGM$StdControl$start(void)
#line 59
{
  return SCSuartDBGM$HPLUART$init();
}

# 70 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
inline static  result_t testRFIDM$SCSuartSTD$start(void){
#line 70
  unsigned char result;
#line 70

#line 70
  result = SCSuartDBGM$StdControl$start();
#line 70

#line 70
  return result;
#line 70
}
#line 70
static inline  
# 47 "testRFIDM.nc"
result_t testRFIDM$StdControl$start(void)
#line 47
{
  testRFIDM$SCSuartSTD$start();
  testRFIDM$CommControl$start();

  return SUCCESS;
}

# 70 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/StdControl.nc"
inline static  result_t RealMain$StdControl$start(void){
#line 70
  unsigned char result;
#line 70

#line 70
  result = testRFIDM$StdControl$start();
#line 70
  result = rcombine(result, TimerM$StdControl$start());
#line 70

#line 70
  return result;
#line 70
}
#line 70
static inline 
# 63 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/HPLPowerManagementM.nc"
uint8_t HPLPowerManagementM$getPowerLevel(void)
#line 63
{
  uint8_t diff;

#line 65
  if (* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x37 + 0x20) & ~((1 << 1) | (1 << 0))) {

      return HPLPowerManagementM$IDLE;
    }
  else {
#line 68
    if (* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0D + 0x20) & (1 << 7)) {
        return HPLPowerManagementM$IDLE;
      }
    else {
      if (* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x06 + 0x20) & (1 << 7)) {
          return HPLPowerManagementM$ADC_NR;
        }
      else {
#line 74
        if (* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x37 + 0x20) & ((1 << 1) | (1 << 0))) {
            diff = * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x31 + 0x20) - * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x32 + 0x20);
            if (diff < 16) {
              return HPLPowerManagementM$EXT_STANDBY;
              }
#line 78
            return HPLPowerManagementM$POWER_SAVE;
          }
        else 
#line 79
          {
            return HPLPowerManagementM$POWER_DOWN;
          }
        }
      }
    }
}

static inline  
#line 84
void HPLPowerManagementM$doAdjustment(void)
#line 84
{
  uint8_t foo;
#line 85
  uint8_t mcu;

#line 86
  foo = HPLPowerManagementM$getPowerLevel();
  mcu = * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x35 + 0x20);
  mcu &= 0xe3;
  if (foo == HPLPowerManagementM$EXT_STANDBY || foo == HPLPowerManagementM$POWER_SAVE) {
      mcu |= HPLPowerManagementM$IDLE;
      while ((* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x30 + 0x20) & 0x7) != 0) {
           __asm volatile ("nop");}

      mcu &= 0xe3;
    }
  mcu |= foo;
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x35 + 0x20) = mcu;
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x35 + 0x20) |= 1 << 5;
}

static 
# 165 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/avrmote/avrhardware.h"
__inline void __nesc_enable_interrupt(void)
#line 165
{
   __asm volatile ("sei");}

static inline 
#line 141
void TOSH_sleep(void)
{

  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x35 + 0x20) |= 1 << 5;
   __asm volatile ("sleep");}

#line 160
__inline void  __nesc_atomic_end(__nesc_atomic_t oldSreg)
{
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x3F + 0x20) = oldSreg;
}

#line 153
__inline __nesc_atomic_t  __nesc_atomic_start(void )
{
  __nesc_atomic_t result = * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x3F + 0x20);

#line 156
   __asm volatile ("cli");
  return result;
}

static inline 
# 132 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/system/sched.c"
bool TOSH_run_next_task(void)
{
  __nesc_atomic_t fInterruptFlags;
  uint8_t old_full;
  void (*func)(void );

  fInterruptFlags = __nesc_atomic_start();
  old_full = TOSH_sched_full;
  func = TOSH_queue[old_full].tp;
  if (func == (void *)0) 
    {
      __nesc_atomic_end(fInterruptFlags);
      return 0;
    }

  TOSH_queue[old_full].tp = (void *)0;
  TOSH_sched_full = (old_full + 1) & TOSH_TASK_BITMASK;
  __nesc_atomic_end(fInterruptFlags);
  func();

  return 1;
}

static inline void TOSH_run_task(void)
#line 155
{
  while (TOSH_run_next_task()) 
    ;
  TOSH_sleep();
  TOSH_wait();
}

# 116 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/system/TimerM.nc"
static void TimerM$adjustInterval(void)
#line 116
{
  uint8_t i;
#line 117
  uint8_t val = TimerM$maxTimerInterval;

#line 118
  if (TimerM$mState) {
      for (i = 0; i < NUM_TIMERS; i++) {
          if (TimerM$mState & (0x1L << i) && TimerM$mTimerList[i].ticksLeft < val) {
              val = TimerM$mTimerList[i].ticksLeft;
            }
        }
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 124
        {
          TimerM$mInterval = val;
          TimerM$Clock$setInterval(TimerM$mInterval);
          TimerM$setIntervalFlag = 0;
        }
#line 128
        __nesc_atomic_end(__nesc_atomic); }
    }
  else {
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 131
        {
          TimerM$mInterval = TimerM$maxTimerInterval;
          TimerM$Clock$setInterval(TimerM$mInterval);
          TimerM$setIntervalFlag = 0;
        }
#line 135
        __nesc_atomic_end(__nesc_atomic); }
    }
  TimerM$PowerManagement$adjustPower();
}

# 117 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/hardware.h"
static __inline void TOSH_CLR_RED_LED_PIN(void)
#line 117
{
#line 117
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1B + 0x20) &= ~(1 << 2);
}

static inline   
# 72 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/system/LedsC.nc"
result_t LedsC$Leds$redOn(void)
#line 72
{
  {
  }
#line 73
  ;
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 74
    {
      TOSH_CLR_RED_LED_PIN();
      LedsC$ledsOn |= LedsC$RED_BIT;
    }
#line 77
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

static inline   result_t LedsC$Leds$redOff(void)
#line 81
{
  {
  }
#line 82
  ;
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 83
    {
      TOSH_SET_RED_LED_PIN();
      LedsC$ledsOn &= ~LedsC$RED_BIT;
    }
#line 86
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

static inline   result_t LedsC$Leds$redToggle(void)
#line 90
{
  result_t rval;

#line 92
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 92
    {
      if (LedsC$ledsOn & LedsC$RED_BIT) {
        rval = LedsC$Leds$redOff();
        }
      else {
#line 96
        rval = LedsC$Leds$redOn();
        }
    }
#line 98
    __nesc_atomic_end(__nesc_atomic); }
#line 98
  return rval;
}

# 81 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/Leds.nc"
inline static   result_t testRFIDM$Leds$redToggle(void){
#line 81
  unsigned char result;
#line 81

#line 81
  result = LedsC$Leds$redToggle();
#line 81

#line 81
  return result;
#line 81
}
#line 81
static inline  
# 60 "testRFIDM.nc"
result_t testRFIDM$Timer$fired(void)
#line 60
{
  testRFIDM$Leds$redToggle();
  return SUCCESS;
}

static inline  
# 151 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/system/AMStandard.nc"
result_t AMStandard$ActivityTimer$fired(void)
#line 151
{
  AMStandard$lastCount = AMStandard$counter;
  AMStandard$counter = 0;
  return SUCCESS;
}

static inline   
# 154 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/system/TimerM.nc"
result_t TimerM$Timer$default$fired(uint8_t id)
#line 154
{
  return SUCCESS;
}

# 73 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/Timer.nc"
inline static  result_t TimerM$Timer$fired(uint8_t arg_0xa40a158){
#line 73
  unsigned char result;
#line 73

#line 73
  switch (arg_0xa40a158) {
#line 73
    case 0:
#line 73
      result = testRFIDM$Timer$fired();
#line 73
      break;
#line 73
    case 1:
#line 73
      result = AMStandard$ActivityTimer$fired();
#line 73
      break;
#line 73
    default:
#line 73
      result = TimerM$Timer$default$fired(arg_0xa40a158);
#line 73
    }
#line 73

#line 73
  return result;
#line 73
}
#line 73
static inline 
# 166 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/system/TimerM.nc"
uint8_t TimerM$dequeue(void)
#line 166
{
  if (TimerM$queue_size == 0) {
    return NUM_TIMERS;
    }
#line 169
  if (TimerM$queue_head == NUM_TIMERS - 1) {
    TimerM$queue_head = -1;
    }
#line 171
  TimerM$queue_head++;
  TimerM$queue_size--;
  return TimerM$queue[(uint8_t )TimerM$queue_head];
}

static inline  void TimerM$signalOneTimer(void)
#line 176
{
  uint8_t itimer = TimerM$dequeue();

#line 178
  if (itimer < NUM_TIMERS) {
    TimerM$Timer$fired(itimer);
    }
}

static inline 
#line 158
void TimerM$enqueue(uint8_t value)
#line 158
{
  if (TimerM$queue_tail == NUM_TIMERS - 1) {
    TimerM$queue_tail = -1;
    }
#line 161
  TimerM$queue_tail++;
  TimerM$queue_size++;
  TimerM$queue[(uint8_t )TimerM$queue_tail] = value;
}

static inline  
#line 182
void TimerM$HandleFire(void)
#line 182
{
  uint8_t i;

#line 184
  TimerM$setIntervalFlag = 1;
  if (TimerM$mState) {
      for (i = 0; i < NUM_TIMERS; i++) {
          if (TimerM$mState & (0x1L << i)) {
              TimerM$mTimerList[i].ticksLeft -= TimerM$mInterval + 1;
              if (TimerM$mTimerList[i].ticksLeft <= 2) {
                  if (TimerM$mTimerList[i].type == TIMER_REPEAT) {
                      TimerM$mTimerList[i].ticksLeft += TimerM$mTimerList[i].ticks;
                    }
                  else 
#line 192
                    {
                      TimerM$mState &= ~(0x1L << i);
                    }
                  TimerM$enqueue(i);
                  TOS_post(TimerM$signalOneTimer);
                }
            }
        }
    }
  TimerM$adjustInterval();
}

static inline   result_t TimerM$Clock$fire(void)
#line 204
{
  TOS_post(TimerM$HandleFire);
  return SUCCESS;
}

# 180 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/Clock.nc"
inline static   result_t HPLClock$Clock$fire(void){
#line 180
  unsigned char result;
#line 180

#line 180
  result = TimerM$Clock$fire();
#line 180

#line 180
  return result;
#line 180
}
#line 180
# 88 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/HPLUART.nc"
inline static   result_t HPLUART1M$UART$get(uint8_t arg_0xa4a6800){
#line 88
  unsigned char result;
#line 88

#line 88
  result = RFID_ControlM$UART$get(arg_0xa4a6800);
#line 88

#line 88
  return result;
#line 88
}
#line 88
# 85 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/HPLUART1M.nc"
void __attribute((signal))   __vector_30(void)
#line 85
{
  if (* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)0x9B & (1 << 7)) {
    HPLUART1M$UART$get(* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)0x9C);
    }
}

# 12 "RFID_Control.nc"
inline static   void RFID_ControlM$RFID_Control$WData_15693_Done(char arg_0xa3d6d90){
#line 12
  testRFIDM$RFID_Control$WData_15693_Done(arg_0xa3d6d90);
#line 12
}
#line 12
# 30 "SCSuartDBG.nc"
inline static   void RFID_ControlM$SCSuartDBG$UARTSend(uint8_t *arg_0xa3d23b0, uint8_t arg_0xa3d24f8){
#line 30
  SCSuartDBGM$SCSuartDBG$UARTSend(arg_0xa3d23b0, arg_0xa3d24f8);
#line 30
}
#line 30
# 11 "RFID_Control.nc"
inline static   void RFID_ControlM$RFID_Control$RData_15693_Done(char arg_0xa3d66d0, uint8_t *arg_0xa3d6830, char arg_0xa3d6970){
#line 11
  testRFIDM$RFID_Control$RData_15693_Done(arg_0xa3d66d0, arg_0xa3d6830, arg_0xa3d6970);
#line 11
}
#line 11
#line 10
inline static   void RFID_ControlM$RFID_Control$GetID_15693_Done(char arg_0xa3d6010, uint8_t *arg_0xa3d6170, char arg_0xa3d62b0){
#line 10
  testRFIDM$RFID_Control$GetID_15693_Done(arg_0xa3d6010, arg_0xa3d6170, arg_0xa3d62b0);
#line 10
}
#line 10
#line 9
inline static   void RFID_ControlM$RFID_Control$GetID_14443A_Done(char arg_0xa3d9940, uint8_t *arg_0xa3d9aa0, char arg_0xa3d9be0){
#line 9
  testRFIDM$RFID_Control$GetID_14443A_Done(arg_0xa3d9940, arg_0xa3d9aa0, arg_0xa3d9be0);
#line 9
}
#line 9
static inline 
# 183 "RFID_ControlM.nc"
void RFID_ControlM$processing_recvData(void)
#line 183
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 184
    {
      RFID_ControlM$RBuff_Index = 0;
      switch (RFID_ControlM$RFID_State) {

          case RFID_ControlM$RFID14443A_GETID_START: 
            if (RFID_ControlM$RecvBuff_From_RFID[3] == 0) {
                RFID_ControlM$RFID_CardType = 1;
                RFID_ControlM$RFID_State = RFID_ControlM$RFID14443A_GETID;
                RFID_ControlM$UARTSend(RFID_ControlM$GetID14443, 5);
              }
            else 
#line 193
              {
                RFID_ControlM$RFID_CardType = 0;
                RFID_ControlM$RFID_State = RFID_ControlM$RFID_NULL;

                RFID_ControlM$RFID_Control$GetID_14443A_Done(3, (void *)0, 0);
              }
          break;

          case RFID_ControlM$RFID15693_GETID_START: 
            if (RFID_ControlM$RecvBuff_From_RFID[3] == 0) {
                RFID_ControlM$RFID_CardType = 2;
                RFID_ControlM$RFID_State = RFID_ControlM$RFID15693_GETID;
                RFID_ControlM$UID15693Index = 0;
                RFID_ControlM$UARTSend(RFID_ControlM$GetID15693, 11);
              }
            else 
#line 207
              {
                RFID_ControlM$RFID_CardType = 0;
                RFID_ControlM$RFID_State = RFID_ControlM$RFID_NULL;

                RFID_ControlM$RFID_Control$GetID_15693_Done(3, (void *)0, 0);
              }
          break;

          case RFID_ControlM$RFID15693_RDATA_START: 
            if (RFID_ControlM$RecvBuff_From_RFID[3] == 0) {
                RFID_ControlM$RFID_CardType = 2;
                RFID_ControlM$RFID_State = RFID_ControlM$RFID15693_RDATA_MEM;
                RFID_ControlM$UARTSend(RFID_ControlM$SetMemoryAsscess, 10);
              }
            else 
#line 220
              {
                RFID_ControlM$RFID_CardType = 0;
                RFID_ControlM$RFID_State = RFID_ControlM$RFID_NULL;

                RFID_ControlM$RFID_Control$RData_15693_Done(3, (void *)0, 0);
              }
          break;

          case RFID_ControlM$RFID15693_WDATA_START: 
            if (RFID_ControlM$RecvBuff_From_RFID[3] == 0) {
                RFID_ControlM$RFID_CardType = 2;
                RFID_ControlM$RFID_State = RFID_ControlM$RFID15693_WDATA_MEM;
                RFID_ControlM$UARTSend(RFID_ControlM$SetMemoryAsscess, 10);
              }
            else 
#line 233
              {
                RFID_ControlM$RFID_CardType = 0;
                RFID_ControlM$RFID_State = RFID_ControlM$RFID_NULL;

                RFID_ControlM$RFID_Control$WData_15693_Done(3);
              }
          break;



          case RFID_ControlM$RFID15693_RDATA_MEM: 
            if (RFID_ControlM$RecvBuff_From_RFID[3] == 0) {
                RFID_ControlM$RFID_CardType = 2;
                RFID_ControlM$RFID_State = RFID_ControlM$RFID15693_RDATA;
                RFID_ControlM$UARTSend(RFID_ControlM$ReadData15693, 12);
              }
            else 
#line 248
              {
                RFID_ControlM$RFID_CardType = 0;
                RFID_ControlM$RFID_State = RFID_ControlM$RFID_NULL;
                RFID_ControlM$RFID_Control$RData_15693_Done(4, (void *)0, 0);
              }
          break;

          case RFID_ControlM$RFID15693_WDATA_MEM: 
            if (RFID_ControlM$RecvBuff_From_RFID[3] == 0) {
                RFID_ControlM$RFID_CardType = 2;
                RFID_ControlM$RFID_State = RFID_ControlM$RFID15693_WDATA;
                RFID_ControlM$UARTSend(RFID_ControlM$RFID_Write_Buff, RFID_ControlM$RFID_Write_Buff_Size);
              }
            else 
#line 260
              {
                RFID_ControlM$RFID_CardType = 0;
                RFID_ControlM$RFID_State = RFID_ControlM$RFID_NULL;
                RFID_ControlM$RFID_Control$WData_15693_Done(4);
              }
          break;




          case RFID_ControlM$RFID14443A_GETID: 
            RFID_ControlM$RFID_State = RFID_ControlM$RFID_NULL;
          if (RFID_ControlM$RecvBuff_From_RFID[3] == 0) {

              RFID_ControlM$RFID_Control$GetID_14443A_Done(0, 
              RFID_ControlM$RecvBuff_From_RFID + 6, 15);
            }
          else 
#line 276
            {

              RFID_ControlM$RFID_Control$GetID_14443A_Done(1, (void *)0, 0);
            }
          break;

          case RFID_ControlM$RFID15693_GETID: 
            if (RFID_ControlM$RecvBuff_From_RFID[3] == 0 && RFID_ControlM$UID15693Index < 10) {
                uint8_t i;

#line 285
                {
                  for (i = 0; i < 8; i++) 
                    RFID_ControlM$UID15693[RFID_ControlM$UID15693Index][i] = RFID_ControlM$RecvBuff_From_RFID[10 + 8 - i - 1];
                  RFID_ControlM$UID15693Index++;
                }
              }
            else {
#line 291
              if (RFID_ControlM$RecvBuff_From_RFID[3] == 0xFE) {
                  if (RFID_ControlM$UID15693Index > 0) {
                    RFID_ControlM$RFID_Control$GetID_15693_Done(0, RFID_ControlM$UID15693[0], RFID_ControlM$UID15693Index);
                    }
                  else {
#line 295
                    RFID_ControlM$RFID_Control$GetID_15693_Done(1, (void *)0, 0);
                    }
#line 296
                  RFID_ControlM$RFID_State = RFID_ControlM$RFID_NULL;
                  RFID_ControlM$UID15693Index = 0;
                }
              else {

                  RFID_ControlM$RFID_Control$GetID_15693_Done(1, (void *)0, 0);
                  RFID_ControlM$UID15693Index = 0;
                }
              }
          break;

          case RFID_ControlM$RFID15693_RDATA: 
            RFID_ControlM$RFID_State = RFID_ControlM$RFID_NULL;
          if (RFID_ControlM$RecvBuff_From_RFID[3] == 0) {

              RFID_ControlM$RFID_Control$RData_15693_Done(0, RFID_ControlM$RecvBuff_From_RFID + 4, 5);
            }
          else 
#line 312
            {

              RFID_ControlM$RFID_Control$RData_15693_Done(1, (void *)0, 0);
            }
          break;

          case RFID_ControlM$RFID15693_WDATA: 
            RFID_ControlM$RFID_State = RFID_ControlM$RFID_NULL;
          if (RFID_ControlM$RecvBuff_From_RFID[3] == 0) {
              RFID_ControlM$SCSuartDBG$UARTSend(&RFID_ControlM$debug4S, 1);
              RFID_ControlM$RFID_Control$WData_15693_Done(0);
            }
          else 
#line 323
            {
              RFID_ControlM$SCSuartDBG$UARTSend(&RFID_ControlM$debug4F, 1);
              RFID_ControlM$RFID_Control$WData_15693_Done(1);
            }
          break;

          default: 
            break;
        }
      RFID_ControlM$RBuff_Index = 0;
    }
#line 333
    __nesc_atomic_end(__nesc_atomic); }
}

static inline   
# 95 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/HPLUART1M.nc"
result_t HPLUART1M$UART$put(uint8_t data)
#line 95
{
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)0x9B |= 1 << 6;
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)0x9C = data;
  return SUCCESS;
}

# 80 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/HPLUART.nc"
inline static   result_t RFID_ControlM$UART$put(uint8_t arg_0xa4a6300){
#line 80
  unsigned char result;
#line 80

#line 80
  result = HPLUART1M$UART$put(arg_0xa4a6300);
#line 80

#line 80
  return result;
#line 80
}
#line 80
static inline   
# 105 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/HPLUART0M.nc"
result_t HPLUART0M$UART$put(uint8_t data)
#line 105
{
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0C + 0x20) = data;
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0B + 0x20) |= 1 << 6;

  return SUCCESS;
}

# 80 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/HPLUART.nc"
inline static   result_t SCSuartDBGM$HPLUART$put(uint8_t arg_0xa4a6300){
#line 80
  unsigned char result;
#line 80

#line 80
  result = HPLUART0M$UART$put(arg_0xa4a6300);
#line 80

#line 80
  return result;
#line 80
}
#line 80
#line 96
inline static   result_t HPLUART1M$UART$putDone(void){
#line 96
  unsigned char result;
#line 96

#line 96
  result = RFID_ControlM$UART$putDone();
#line 96

#line 96
  return result;
#line 96
}
#line 96
# 91 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/HPLUART1M.nc"
void __attribute((interrupt))   __vector_32(void)
#line 91
{
  HPLUART1M$UART$putDone();
}

# 30 "SCSuartDBGRecv.nc"
inline static   void SCSuartDBGM$SCSuartDBGRecv$UARTRecv(uint8_t arg_0xa3e5bc0){
#line 30
  testRFIDM$SCSuartDBGRecv$UARTRecv(arg_0xa3e5bc0);
#line 30
}
#line 30
static inline   
# 68 "SCSuartDBGM.nc"
result_t SCSuartDBGM$HPLUART$get(uint8_t data)
#line 68
{
  SCSuartDBGM$SCSuartDBGRecv$UARTRecv(data);
  return SUCCESS;
}

# 66 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/ByteComm.nc"
inline static   result_t UARTM$ByteComm$rxByteReady(uint8_t arg_0xa65df40, bool arg_0xa67c0a0, uint16_t arg_0xa67c1f8){
#line 66
  unsigned char result;
#line 66

#line 66
  result = FramerM$ByteComm$rxByteReady(arg_0xa65df40, arg_0xa67c0a0, arg_0xa67c1f8);
#line 66

#line 66
  return result;
#line 66
}
#line 66
static inline   
# 77 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/system/UARTM.nc"
result_t UARTM$HPLUART$get(uint8_t data)
#line 77
{




  UARTM$ByteComm$rxByteReady(data, FALSE, 0);
  {
  }
#line 83
  ;
  return SUCCESS;
}

# 88 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/HPLUART.nc"
inline static   result_t HPLUART0M$UART$get(uint8_t arg_0xa4a6800){
#line 88
  unsigned char result;
#line 88

#line 88
  result = UARTM$HPLUART$get(arg_0xa4a6800);
#line 88
  result = rcombine(result, SCSuartDBGM$HPLUART$get(arg_0xa4a6800));
#line 88

#line 88
  return result;
#line 88
}
#line 88
# 90 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/HPLUART0M.nc"
void __attribute((signal))   __vector_18(void)
#line 90
{
  if (* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0B + 0x20) & (1 << 7)) {
    HPLUART0M$UART$get(* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0C + 0x20));
    }
}

static inline  
# 224 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/FramerM.nc"
void FramerM$PacketUnknown(void)
#line 224
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 225
    {
      FramerM$gFlags |= FramerM$FLAGS_UNKNOWN;
    }
#line 227
    __nesc_atomic_end(__nesc_atomic); }

  FramerM$StartTx();
}

static inline  
# 246 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/system/AMStandard.nc"
TOS_MsgPtr AMStandard$UARTReceive$receive(TOS_MsgPtr packet)
#line 246
{


  packet->group = TOS_AM_GROUP;
  return received(packet);
}

# 75 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/ReceiveMsg.nc"
inline static  TOS_MsgPtr FramerAckM$ReceiveCombined$receive(TOS_MsgPtr arg_0xa3e46d8){
#line 75
  struct TOS_Msg *result;
#line 75

#line 75
  result = AMStandard$UARTReceive$receive(arg_0xa3e46d8);
#line 75

#line 75
  return result;
#line 75
}
#line 75
static inline  
# 91 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/system/FramerAckM.nc"
TOS_MsgPtr FramerAckM$ReceiveMsg$receive(TOS_MsgPtr Msg)
#line 91
{
  TOS_MsgPtr pBuf;

  pBuf = FramerAckM$ReceiveCombined$receive(Msg);

  return pBuf;
}

# 75 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/ReceiveMsg.nc"
inline static  TOS_MsgPtr FramerM$ReceiveMsg$receive(TOS_MsgPtr arg_0xa3e46d8){
#line 75
  struct TOS_Msg *result;
#line 75

#line 75
  result = FramerAckM$ReceiveMsg$receive(arg_0xa3e46d8);
#line 75

#line 75
  return result;
#line 75
}
#line 75
static inline  
# 350 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/FramerM.nc"
result_t FramerM$TokenReceiveMsg$ReflectToken(uint8_t Token)
#line 350
{
  result_t Result = SUCCESS;

  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 353
    {
      if (!(FramerM$gFlags & FramerM$FLAGS_TOKENPEND)) {
          FramerM$gFlags |= FramerM$FLAGS_TOKENPEND;
          FramerM$gTxTokenBuf = Token;
        }
      else {
          Result = FAIL;
        }
    }
#line 361
    __nesc_atomic_end(__nesc_atomic); }

  if (Result == SUCCESS) {
      Result = FramerM$StartTx();
    }

  return Result;
}

# 88 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/TokenReceiveMsg.nc"
inline static  result_t FramerAckM$TokenReceiveMsg$ReflectToken(uint8_t arg_0xa681dd8){
#line 88
  unsigned char result;
#line 88

#line 88
  result = FramerM$TokenReceiveMsg$ReflectToken(arg_0xa681dd8);
#line 88

#line 88
  return result;
#line 88
}
#line 88
static inline  
# 74 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/system/FramerAckM.nc"
void FramerAckM$SendAckTask(void)
#line 74
{

  FramerAckM$TokenReceiveMsg$ReflectToken(FramerAckM$gTokenBuf);
}

static inline  TOS_MsgPtr FramerAckM$TokenReceiveMsg$receive(TOS_MsgPtr Msg, uint8_t token)
#line 79
{
  TOS_MsgPtr pBuf;

  FramerAckM$gTokenBuf = token;

  TOS_post(FramerAckM$SendAckTask);

  pBuf = FramerAckM$ReceiveCombined$receive(Msg);

  return pBuf;
}

# 75 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/TokenReceiveMsg.nc"
inline static  TOS_MsgPtr FramerM$TokenReceiveMsg$receive(TOS_MsgPtr arg_0xa681678, uint8_t arg_0xa6817c0){
#line 75
  struct TOS_Msg *result;
#line 75

#line 75
  result = FramerAckM$TokenReceiveMsg$receive(arg_0xa681678, arg_0xa6817c0);
#line 75

#line 75
  return result;
#line 75
}
#line 75
static inline  
# 232 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/FramerM.nc"
void FramerM$PacketRcvd(void)
#line 232
{
  FramerM$MsgRcvEntry_t *pRcv = &FramerM$gMsgRcvTbl[FramerM$gRxTailIndex];
  TOS_MsgPtr pBuf = pRcv->pMsg;


  if (pRcv->Length >= (size_t )& ((struct TOS_Msg *)0)->data) {

      switch (pRcv->Proto) {
          case FramerM$PROTO_ACK: 
            break;
          case FramerM$PROTO_PACKET_ACK: 
            pBuf->crc = 1;
          pBuf = FramerM$TokenReceiveMsg$receive(pBuf, pRcv->Token);
          break;
          case FramerM$PROTO_PACKET_NOACK: 
            pBuf->crc = 1;
          pBuf = FramerM$ReceiveMsg$receive(pBuf);
          break;
          default: 
            FramerM$gTxUnknownBuf = pRcv->Proto;
          TOS_post(FramerM$PacketUnknown);
          break;
        }
    }

  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 257
    {
      if (pBuf) {
          pRcv->pMsg = pBuf;
        }
      pRcv->Length = 0;
      pRcv->Token = 0;
      FramerM$gRxTailIndex++;
      FramerM$gRxTailIndex %= FramerM$HDLC_QUEUESIZE;
    }
#line 265
    __nesc_atomic_end(__nesc_atomic); }
}

# 80 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/HPLUART.nc"
inline static   result_t UARTM$HPLUART$put(uint8_t arg_0xa4a6300){
#line 80
  unsigned char result;
#line 80

#line 80
  result = HPLUART0M$UART$put(arg_0xa4a6300);
#line 80

#line 80
  return result;
#line 80
}
#line 80
static inline  
# 207 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/system/AMStandard.nc"
result_t AMStandard$UARTSend$sendDone(TOS_MsgPtr msg, result_t success)
#line 207
{
  return AMStandard$reportSendDone(msg, success);
}

# 67 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/BareSendMsg.nc"
inline static  result_t FramerM$BareSendMsg$sendDone(TOS_MsgPtr arg_0xa524e88, result_t arg_0xa524fd8){
#line 67
  unsigned char result;
#line 67

#line 67
  result = AMStandard$UARTSend$sendDone(arg_0xa524e88, arg_0xa524fd8);
#line 67

#line 67
  return result;
#line 67
}
#line 67
static inline  
# 65 "testRFIDM.nc"
result_t testRFIDM$SendMsg$sendDone(TOS_MsgPtr sent, result_t success)
#line 65
{
  return SUCCESS;
}

static inline   
# 157 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/system/AMStandard.nc"
result_t AMStandard$SendMsg$default$sendDone(uint8_t id, TOS_MsgPtr msg, result_t success)
#line 157
{
  return SUCCESS;
}

# 49 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/SendMsg.nc"
inline static  result_t AMStandard$SendMsg$sendDone(uint8_t arg_0xa50c718, TOS_MsgPtr arg_0xa3df138, result_t arg_0xa3df288){
#line 49
  unsigned char result;
#line 49

#line 49
  switch (arg_0xa50c718) {
#line 49
    case 7:
#line 49
      result = testRFIDM$SendMsg$sendDone(arg_0xa3df138, arg_0xa3df288);
#line 49
      break;
#line 49
    default:
#line 49
      result = AMStandard$SendMsg$default$sendDone(arg_0xa50c718, arg_0xa3df138, arg_0xa3df288);
#line 49
    }
#line 49

#line 49
  return result;
#line 49
}
#line 49
static inline   
# 160 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/system/AMStandard.nc"
result_t AMStandard$default$sendDone(void)
#line 160
{
  return SUCCESS;
}

#line 65
inline static  result_t AMStandard$sendDone(void){
#line 65
  unsigned char result;
#line 65

#line 65
  result = AMStandard$default$sendDone();
#line 65

#line 65
  return result;
#line 65
}
#line 65
static inline 
#line 132
void AMStandard$dbgPacket(TOS_MsgPtr data)
#line 132
{
  uint8_t i;

  for (i = 0; i < sizeof(TOS_Msg ); i++) 
    {
      {
      }
#line 137
      ;
    }
  {
  }
#line 139
  ;
}

# 30 "SCSuartDBG.nc"
inline static   void testRFIDM$SCSuartDBG$UARTSend(uint8_t *arg_0xa3d23b0, uint8_t arg_0xa3d24f8){
#line 30
  SCSuartDBGM$SCSuartDBG$UARTSend(arg_0xa3d23b0, arg_0xa3d24f8);
#line 30
}
#line 30
# 106 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/Leds.nc"
inline static   result_t testRFIDM$Leds$greenToggle(void){
#line 106
  unsigned char result;
#line 106

#line 106
  result = LedsC$Leds$greenToggle();
#line 106

#line 106
  return result;
#line 106
}
#line 106
static inline  
# 69 "testRFIDM.nc"
TOS_MsgPtr testRFIDM$ReceiveMsg$receive(TOS_MsgPtr m)
#line 69
{
  if (m->addr == 0) 
    {
      struct RFID_COMM_MSG *pack = (struct RFID_COMM_MSG *)m->data;

#line 73
      testRFIDM$Leds$greenToggle();
      sprintf(testRFIDM$OutputUartMsg, "Recv Packet from PC(C:%d, B:%d)\r\n", pack->comm, pack->block);
      testRFIDM$SCSuartDBG$UARTSend(testRFIDM$OutputUartMsg, strlen(testRFIDM$OutputUartMsg));
      testRFIDM$Control_RFID(pack->comm, pack->block, pack->wbuff);
    }
  return m;
}

static inline   
# 242 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/system/AMStandard.nc"
TOS_MsgPtr AMStandard$ReceiveMsg$default$receive(uint8_t id, TOS_MsgPtr msg)
#line 242
{
  return msg;
}

# 75 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/ReceiveMsg.nc"
inline static  TOS_MsgPtr AMStandard$ReceiveMsg$receive(uint8_t arg_0xa50ccd0, TOS_MsgPtr arg_0xa3e46d8){
#line 75
  struct TOS_Msg *result;
#line 75

#line 75
  switch (arg_0xa50ccd0) {
#line 75
    case 7:
#line 75
      result = testRFIDM$ReceiveMsg$receive(arg_0xa3e46d8);
#line 75
      break;
#line 75
    default:
#line 75
      result = AMStandard$ReceiveMsg$default$receive(arg_0xa50ccd0, arg_0xa3e46d8);
#line 75
    }
#line 75

#line 75
  return result;
#line 75
}
#line 75
static inline   
# 110 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/system/LedsC.nc"
result_t LedsC$Leds$greenOff(void)
#line 110
{
  {
  }
#line 111
  ;
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 112
    {
      TOSH_SET_GREEN_LED_PIN();
      LedsC$ledsOn &= ~LedsC$GREEN_BIT;
    }
#line 115
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 118 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/hardware.h"
static __inline void TOSH_CLR_GREEN_LED_PIN(void)
#line 118
{
#line 118
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x1B + 0x20) &= ~(1 << 1);
}

static inline   
# 101 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/system/LedsC.nc"
result_t LedsC$Leds$greenOn(void)
#line 101
{
  {
  }
#line 102
  ;
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 103
    {
      TOSH_CLR_GREEN_LED_PIN();
      LedsC$ledsOn |= LedsC$GREEN_BIT;
    }
#line 106
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

static inline   
# 66 "RFID_ControlM.nc"
void RFID_ControlM$RFID_Control$GetID_14443A(void)
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 68
    {
      if (RFID_ControlM$RFID_CardType == 1) {
          RFID_ControlM$RFID_State = RFID_ControlM$RFID14443A_GETID;
          RFID_ControlM$RBuff_Index = 0;
          RFID_ControlM$UARTSend(RFID_ControlM$GetID14443, 5);
        }
      else 
#line 73
        {
          RFID_ControlM$RFID_State = RFID_ControlM$RFID14443A_GETID_START;
          RFID_ControlM$RBuff_Index = 0;
          RFID_ControlM$UARTSend(RFID_ControlM$StartComm14443, 9);
        }
    }
#line 78
    __nesc_atomic_end(__nesc_atomic); }
}

# 4 "RFID_Control.nc"
inline static   void testRFIDM$RFID_Control$GetID_14443A(void){
#line 4
  RFID_ControlM$RFID_Control$GetID_14443A();
#line 4
}
#line 4
static inline   
# 81 "RFID_ControlM.nc"
void RFID_ControlM$RFID_Control$GetID_15693(void)
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 83
    {
      if (RFID_ControlM$RFID_CardType == 2) {
          RFID_ControlM$RBuff_Index = 0;
          RFID_ControlM$RFID_State = RFID_ControlM$RFID15693_GETID;
          RFID_ControlM$UID15693Index = 0;
          RFID_ControlM$UARTSend(RFID_ControlM$GetID15693, 11);
        }
      else 
#line 89
        {
          RFID_ControlM$RFID_State = RFID_ControlM$RFID15693_GETID_START;
          RFID_ControlM$RBuff_Index = 0;
          RFID_ControlM$UARTSend(RFID_ControlM$StartComm15693, 9);
        }
    }
#line 94
    __nesc_atomic_end(__nesc_atomic); }
}

# 5 "RFID_Control.nc"
inline static   void testRFIDM$RFID_Control$GetID_15693(void){
#line 5
  RFID_ControlM$RFID_Control$GetID_15693();
#line 5
}
#line 5
static inline   
# 97 "RFID_ControlM.nc"
void RFID_ControlM$RFID_Control$RData_15693(char BlockAddr)
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 99
    {
      uint8_t i;
#line 100
      uint8_t Check_Sum_ = 0;
#line 100
      uint8_t CrcL;
#line 100
      uint8_t CrcH;

#line 100
      ;

      RFID_ControlM$ReadData15693[6] = BlockAddr;
      RFID_ControlM$gen15693crc(RFID_ControlM$ReadData15693 + 4, 4, &CrcL, &CrcH);
      RFID_ControlM$ReadData15693[8] = CrcL;
      RFID_ControlM$ReadData15693[9] = CrcH;
      for (i = 1; i < 10; i++) 
        Check_Sum_ = Check_Sum_ ^ RFID_ControlM$ReadData15693[i];
      RFID_ControlM$ReadData15693[10] = Check_Sum_;

      if (RFID_ControlM$RFID_CardType == 2) {
          RFID_ControlM$RBuff_Index = 0;
          RFID_ControlM$RFID_State = RFID_ControlM$RFID15693_RDATA_MEM;
          RFID_ControlM$UARTSend(RFID_ControlM$SetMemoryAsscess, 10);
        }
      else 
#line 114
        {
          RFID_ControlM$RFID_State = RFID_ControlM$RFID15693_RDATA_START;
          RFID_ControlM$RBuff_Index = 0;
          RFID_ControlM$UARTSend(RFID_ControlM$StartComm15693, 9);
        }
    }
#line 119
    __nesc_atomic_end(__nesc_atomic); }
}

# 6 "RFID_Control.nc"
inline static   void testRFIDM$RFID_Control$RData_15693(char arg_0xa3d8e78){
#line 6
  RFID_ControlM$RFID_Control$RData_15693(arg_0xa3d8e78);
#line 6
}
#line 6
static inline   
# 122 "RFID_ControlM.nc"
void RFID_ControlM$RFID_Control$WData_15693(char BlockAddr, char *buff, char size)
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 124
    {
      uint8_t i;
#line 125
      uint8_t Check_Sum_ = 0;
#line 125
      uint8_t CrcL;
#line 125
      uint8_t CrcH;

      RFID_ControlM$RFID_Write_Buff[0] = 0x02;
      RFID_ControlM$RFID_Write_Buff[1] = 0x0F;
      RFID_ControlM$RFID_Write_Buff[2] = 0x90;
      RFID_ControlM$RFID_Write_Buff[3] = 0x18;
      RFID_ControlM$RFID_Write_Buff[4] = 0x00;
      RFID_ControlM$RFID_Write_Buff[5] = 0x11;
      RFID_ControlM$RFID_Write_Buff[6] = 0x02;
      RFID_ControlM$RFID_Write_Buff[7] = 0x21;
      RFID_ControlM$RFID_Write_Buff[8] = BlockAddr;
      for (i = 9; i < 13; i++) 
        RFID_ControlM$RFID_Write_Buff[i] = (uint8_t )buff[i - 9];

      RFID_ControlM$gen15693crc(RFID_ControlM$RFID_Write_Buff + 6, 7, &CrcL, &CrcH);
      RFID_ControlM$RFID_Write_Buff[i++] = CrcL;
      RFID_ControlM$RFID_Write_Buff[i++] = CrcH;
      RFID_ControlM$RFID_Write_Buff_Size = i;

      for (i = 1; i < RFID_ControlM$RFID_Write_Buff_Size; i++) 
        Check_Sum_ = Check_Sum_ ^ RFID_ControlM$RFID_Write_Buff[i];
      RFID_ControlM$RFID_Write_Buff[i++] = Check_Sum_;
      RFID_ControlM$RFID_Write_Buff[i++] = 0x03;
      RFID_ControlM$RFID_Write_Buff_Size = i;

      if (RFID_ControlM$RFID_CardType == 2) {
          RFID_ControlM$RBuff_Index = 0;
          RFID_ControlM$RFID_State = RFID_ControlM$RFID15693_WDATA_MEM;
          RFID_ControlM$UARTSend(RFID_ControlM$SetMemoryAsscess, 10);
        }
      else 
#line 154
        {
          RFID_ControlM$RFID_State = RFID_ControlM$RFID15693_WDATA_START;
          RFID_ControlM$RBuff_Index = 0;
          RFID_ControlM$UARTSend(RFID_ControlM$StartComm15693, 9);
        }
    }
#line 159
    __nesc_atomic_end(__nesc_atomic); }
}

# 7 "RFID_Control.nc"
inline static   void testRFIDM$RFID_Control$WData_15693(char arg_0xa3d9288, char *arg_0xa3d93e0, char arg_0xa3d9520){
#line 7
  RFID_ControlM$RFID_Control$WData_15693(arg_0xa3d9288, arg_0xa3d93e0, arg_0xa3d9520);
#line 7
}
#line 7
# 96 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/HPLUART.nc"
inline static   result_t HPLUART0M$UART$putDone(void){
#line 96
  unsigned char result;
#line 96

#line 96
  result = UARTM$HPLUART$putDone();
#line 96
  result = rcombine(result, SCSuartDBGM$HPLUART$putDone());
#line 96

#line 96
  return result;
#line 96
}
#line 96
# 100 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/HPLUART0M.nc"
void __attribute((interrupt))   __vector_20(void)
#line 100
{
  HPLUART0M$UART$putDone();
}

static inline   
# 581 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/FramerM.nc"
result_t FramerM$ByteComm$txDone(void)
#line 581
{

  if (FramerM$gTxState == FramerM$TXSTATE_FINISH) {
      FramerM$gTxState = FramerM$TXSTATE_IDLE;
      TOS_post(FramerM$PacketSent);
    }

  return SUCCESS;
}

# 83 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/ByteComm.nc"
inline static   result_t UARTM$ByteComm$txDone(void){
#line 83
  unsigned char result;
#line 83

#line 83
  result = FramerM$ByteComm$txDone();
#line 83

#line 83
  return result;
#line 83
}
#line 83
#line 55
inline static   result_t FramerM$ByteComm$txByte(uint8_t arg_0xa65dab0){
#line 55
  unsigned char result;
#line 55

#line 55
  result = UARTM$ByteComm$txByte(arg_0xa65dab0);
#line 55

#line 55
  return result;
#line 55
}
#line 55
static inline 
# 66 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/avrmote/crc.h"
uint16_t crcByte(uint16_t oldCrc, uint8_t byte)
{

  uint16_t *table = crcTable;
  uint16_t newCrc;

   __asm ("eor %1,%B3\n"
  "\tlsl %1\n"
  "\tadc %B2, __zero_reg__\n"
  "\tadd %A2, %1\n"
  "\tadc %B2, __zero_reg__\n"
  "\tlpm\n"
  "\tmov %B0, %A3\n"
  "\tmov %A0, r0\n"
  "\tadiw r30,1\n"
  "\tlpm\n"
  "\teor %B0, r0" : 
  "=r"(newCrc), "+r"(byte), "+z"(table) : "r"(oldCrc));
  return newCrc;
}

static inline   
# 508 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/FramerM.nc"
result_t FramerM$ByteComm$txByteReady(bool LastByteSuccess)
#line 508
{
  result_t TxResult = SUCCESS;
  uint8_t nextByte;

  if (LastByteSuccess != TRUE) {
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 513
        FramerM$gTxState = FramerM$TXSTATE_ERROR;
#line 513
        __nesc_atomic_end(__nesc_atomic); }
      TOS_post(FramerM$PacketSent);
      return SUCCESS;
    }

  switch (FramerM$gTxState) {

      case FramerM$TXSTATE_PROTO: 
        FramerM$gTxState = FramerM$TXSTATE_INFO;
      FramerM$gTxRunningCRC = crcByte(FramerM$gTxRunningCRC, FramerM$gTxProto);
      TxResult = FramerM$ByteComm$txByte(FramerM$gTxProto);
      break;

      case FramerM$TXSTATE_INFO: 
        nextByte = FramerM$gpTxBuf[FramerM$gTxByteCnt];
      FramerM$gTxRunningCRC = crcByte(FramerM$gTxRunningCRC, nextByte);
      FramerM$gTxByteCnt++;
      if (FramerM$gTxByteCnt == 10) {
        FramerM$gTxByteCnt = 0;
        }
#line 532
      if (FramerM$gTxByteCnt == 1) {
        FramerM$gTxByteCnt = 10;
        }
#line 534
      if (FramerM$gTxByteCnt >= FramerM$gTxLength) {
          FramerM$gTxState = FramerM$TXSTATE_FCS1;
        }

      TxResult = FramerM$TxArbitraryByte(nextByte);
      break;

      case FramerM$TXSTATE_ESC: 

        TxResult = FramerM$ByteComm$txByte(FramerM$gTxEscByte ^ 0x20);
      FramerM$gTxState = FramerM$gPrevTxState;
      break;

      case FramerM$TXSTATE_FCS1: 
        nextByte = (uint8_t )(FramerM$gTxRunningCRC & 0xff);
      FramerM$gTxState = FramerM$TXSTATE_FCS2;
      TxResult = FramerM$TxArbitraryByte(nextByte);
      break;

      case FramerM$TXSTATE_FCS2: 
        nextByte = (uint8_t )((FramerM$gTxRunningCRC >> 8) & 0xff);
      FramerM$gTxState = FramerM$TXSTATE_ENDFLAG;
      TxResult = FramerM$TxArbitraryByte(nextByte);
      break;

      case FramerM$TXSTATE_ENDFLAG: 
        FramerM$gTxState = FramerM$TXSTATE_FINISH;
      TxResult = FramerM$ByteComm$txByte(FramerM$HDLC_FLAG_BYTE);

      break;

      case FramerM$TXSTATE_FINISH: 
        case FramerM$TXSTATE_ERROR: 

          default: 
            break;
    }


  if (TxResult != SUCCESS) {
      FramerM$gTxState = FramerM$TXSTATE_ERROR;
      TOS_post(FramerM$PacketSent);
    }

  return SUCCESS;
}

# 75 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/ByteComm.nc"
inline static   result_t UARTM$ByteComm$txByteReady(bool arg_0xa67c728){
#line 75
  unsigned char result;
#line 75

#line 75
  result = FramerM$ByteComm$txByteReady(arg_0xa67c728);
#line 75

#line 75
  return result;
#line 75
}
#line 75
static inline   
# 349 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/CC2420ControlM.nc"
result_t CC2420ControlM$HPLChipcon$FIFOPIntr(void)
#line 349
{
  return SUCCESS;
}

# 45 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/HPLCC2420.nc"
inline static   result_t HPLCC2420M$HPLCC2420$FIFOPIntr(void){
#line 45
  unsigned char result;
#line 45

#line 45
  result = CC2420RadioM$HPLChipcon$FIFOPIntr();
#line 45
  result = rcombine(result, CC2420ControlM$HPLChipcon$FIFOPIntr());
#line 45

#line 45
  return result;
#line 45
}
#line 45
# 205 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/HPLCC2420M.nc"
void __attribute((signal))   __vector_7(void)
#line 205
{
#line 218
  HPLCC2420M$HPLCC2420$FIFOPIntr();
}

static inline   
# 76 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/TimerJiffyAsyncM.nc"
bool TimerJiffyAsyncM$TimerJiffyAsync$isSet(void)
{
  return TimerJiffyAsyncM$bSet;
}

# 10 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/TimerJiffyAsync.nc"
inline static   bool CC2420RadioM$BackoffTimerJiffy$isSet(void){
#line 10
  unsigned char result;
#line 10

#line 10
  result = TimerJiffyAsyncM$TimerJiffyAsync$isSet();
#line 10

#line 10
  return result;
#line 10
}
#line 10
static inline   
# 165 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/HPLTimer2.nc"
void HPLTimer2$Timer2$intDisable(void)
#line 165
{
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x37 + 0x20) &= ~(1 << 7);
}

# 168 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/Clock.nc"
inline static   void TimerJiffyAsyncM$Timer$intDisable(void){
#line 168
  HPLTimer2$Timer2$intDisable();
#line 168
}
#line 168
static inline   
# 81 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/TimerJiffyAsyncM.nc"
result_t TimerJiffyAsyncM$TimerJiffyAsync$stop(void)
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 83
    {
      TimerJiffyAsyncM$bSet = FALSE;
      TimerJiffyAsyncM$Timer$intDisable();
    }
#line 86
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

# 8 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/TimerJiffyAsync.nc"
inline static   result_t CC2420RadioM$BackoffTimerJiffy$stop(void){
#line 8
  unsigned char result;
#line 8

#line 8
  result = TimerJiffyAsyncM$TimerJiffyAsync$stop();
#line 8

#line 8
  return result;
#line 8
}
#line 8
static inline  
# 252 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/system/AMStandard.nc"
TOS_MsgPtr AMStandard$RadioReceive$receive(TOS_MsgPtr packet)
#line 252
{
  return received(packet);
}

# 75 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/ReceiveMsg.nc"
inline static  TOS_MsgPtr CC2420RadioM$Receive$receive(TOS_MsgPtr arg_0xa3e46d8){
#line 75
  struct TOS_Msg *result;
#line 75

#line 75
  result = AMStandard$RadioReceive$receive(arg_0xa3e46d8);
#line 75

#line 75
  return result;
#line 75
}
#line 75
static inline  
# 143 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/CC2420RadioM.nc"
void CC2420RadioM$PacketRcvd(void)
#line 143
{
  TOS_MsgPtr pBuf;

  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 146
    {
      CC2420RadioM$rxbufptr->time = 0;
      pBuf = CC2420RadioM$rxbufptr;
    }
#line 149
    __nesc_atomic_end(__nesc_atomic); }
  pBuf = CC2420RadioM$Receive$receive((TOS_MsgPtr )pBuf);
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 151
    {
      if (pBuf) {
#line 152
        CC2420RadioM$rxbufptr = pBuf;
        }
#line 153
      CC2420RadioM$rxbufptr->length = 0;
    }
#line 154
    __nesc_atomic_end(__nesc_atomic); }
}

static 
# 21 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/byteorder.h"
__inline uint16_t fromLSB16(uint16_t a)
{
  return is_host_lsb() ? a : (a << 8) | (a >> 8);
}

# 52 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/HPLCC2420.nc"
inline static   uint8_t CC2420RadioM$HPLChipcon$cmd(uint8_t arg_0xa56ac58){
#line 52
  unsigned char result;
#line 52

#line 52
  result = HPLCC2420M$HPLCC2420$cmd(arg_0xa56ac58);
#line 52

#line 52
  return result;
#line 52
}
#line 52
#line 66
inline static   uint16_t CC2420RadioM$HPLChipcon$read(uint8_t arg_0xa56b6a8){
#line 66
  unsigned int result;
#line 66

#line 66
  result = HPLCC2420M$HPLCC2420$read(arg_0xa56b6a8);
#line 66

#line 66
  return result;
#line 66
}
#line 66
# 160 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/hardware.h"
static __inline int TOSH_READ_CC_FIFO_PIN(void)
#line 160
{
#line 160
  return (* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x16 + 0x20) & (1 << 7)) != 0;
}

static inline   
# 404 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/CC2420RadioM.nc"
result_t CC2420RadioM$HPLChipconFIFO$RXFIFODone(uint8_t length, uint8_t *data)
#line 404
{
  uint8_t currentstate;

#line 406
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 406
    currentstate = CC2420RadioM$stateRadio;
#line 406
    __nesc_atomic_end(__nesc_atomic); }

  if (TOSH_READ_CC_FIFO_PIN()) {
      CC2420RadioM$HPLChipcon$read(0x3F);
      CC2420RadioM$HPLChipcon$cmd(0x08);
    }


  if (length > MSG_DATA_SIZE) {
    return SUCCESS;
    }
  CC2420RadioM$rxbufptr = (TOS_MsgPtr )data;


  if (
#line 419
  CC2420RadioM$bAckEnable && currentstate == CC2420RadioM$POST_TX_STATE && (
  CC2420RadioM$rxbufptr->fcfhi & 0x03) == 0x02 && 
  CC2420RadioM$rxbufptr->dsn == CC2420RadioM$currentDSN) {
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 422
        {
          CC2420RadioM$txbufptr->ack = 1;
          CC2420RadioM$txbufptr->strength = data[length - 2];
          CC2420RadioM$txbufptr->lqi = data[length - 1] & 0x7F;
          currentstate = CC2420RadioM$POST_TX_ACK_STATE;
        }
#line 427
        __nesc_atomic_end(__nesc_atomic); }
      TOS_post(CC2420RadioM$PacketSent);
    }

  if ((CC2420RadioM$rxbufptr->fcfhi & 0x03) != 0x01) {
    return SUCCESS;
    }
  CC2420RadioM$rxbufptr->length = CC2420RadioM$rxbufptr->length - MSG_HEADER_SIZE - MSG_FOOTER_SIZE;

  CC2420RadioM$rxbufptr->addr = fromLSB16(CC2420RadioM$rxbufptr->addr);


  CC2420RadioM$rxbufptr->crc = data[length - 1] >> 7;

  CC2420RadioM$rxbufptr->strength = data[length - 2];

  CC2420RadioM$rxbufptr->lqi = data[length - 1] & 0x7F;

  TOS_post(CC2420RadioM$PacketRcvd);

  return SUCCESS;
}

# 37 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/HPLCC2420FIFO.nc"
inline static   result_t HPLCC2420FIFOM$HPLCC2420FIFO$RXFIFODone(uint8_t arg_0xa56fb68, uint8_t *arg_0xa56fcc8){
#line 37
  unsigned char result;
#line 37

#line 37
  result = CC2420RadioM$HPLChipconFIFO$RXFIFODone(arg_0xa56fb68, arg_0xa56fcc8);
#line 37

#line 37
  return result;
#line 37
}
#line 37
static inline  
# 63 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/HPLCC2420FIFOM.nc"
void HPLCC2420FIFOM$signalRXdone(void)
#line 63
{
  uint8_t *curren_rxbuf;

#line 65
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 65
    curren_rxbuf = HPLCC2420FIFOM$rxbuf;
#line 65
    __nesc_atomic_end(__nesc_atomic); }
  HPLCC2420FIFOM$HPLCC2420FIFO$RXFIFODone(HPLCC2420FIFOM$rxlength, curren_rxbuf);
}

# 159 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/hardware.h"
static __inline void TOSH_SET_CC_CS_PIN(void)
#line 159
{
#line 159
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x18 + 0x20) |= 1 << 0;
}

static inline   
# 114 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/HPLCC2420FIFOM.nc"
result_t HPLCC2420FIFOM$HPLCC2420FIFO$readRXFIFO(uint8_t len, uint8_t *msg)
#line 114
{
  uint8_t status;
#line 115
  uint8_t i;



  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 119
    {
      HPLCC2420FIFOM$bSpiAvail = FALSE;
      HPLCC2420FIFOM$rxbuf = msg;
      TOSH_CLR_CC_CS_PIN();
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0F + 0x20) = 0x3F | 0x40;
      while (!(* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0E + 0x20) & 0x80)) {
        }
#line 124
      ;
      status = * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0F + 0x20);
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0F + 0x20) = 0;
      while (!(* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0E + 0x20) & 0x80)) {
        }
#line 127
      ;
      HPLCC2420FIFOM$rxlength = * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0F + 0x20);
      if (HPLCC2420FIFOM$rxlength > 0) {
          HPLCC2420FIFOM$rxbuf[0] = HPLCC2420FIFOM$rxlength;

          HPLCC2420FIFOM$rxlength++;

          if (HPLCC2420FIFOM$rxlength > len) {
#line 134
            HPLCC2420FIFOM$rxlength = len;
            }
          for (i = 1; i < HPLCC2420FIFOM$rxlength; i++) {
              * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0F + 0x20) = 0;
              while (!(* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0E + 0x20) & 0x80)) {
                }
#line 138
              ;
              HPLCC2420FIFOM$rxbuf[i] = * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0F + 0x20);
            }
        }

      HPLCC2420FIFOM$bSpiAvail = TRUE;
    }
#line 144
    __nesc_atomic_end(__nesc_atomic); }
  TOSH_SET_CC_CS_PIN();

  if (HPLCC2420FIFOM$rxlength > 0) {
      return TOS_post(HPLCC2420FIFOM$signalRXdone);
    }
  else {
      return FAIL;
    }
}

# 17 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/HPLCC2420FIFO.nc"
inline static   result_t CC2420RadioM$HPLChipconFIFO$readRXFIFO(uint8_t arg_0xa56edb0, uint8_t *arg_0xa56ef10){
#line 17
  unsigned char result;
#line 17

#line 17
  result = HPLCC2420FIFOM$HPLCC2420FIFO$readRXFIFO(arg_0xa56edb0, arg_0xa56ef10);
#line 17

#line 17
  return result;
#line 17
}
#line 17
static inline  
# 359 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/CC2420RadioM.nc"
void CC2420RadioM$delayedRXFIFO(void)
#line 359
{
  uint8_t len = MSG_DATA_SIZE;

#line 361
  CC2420RadioM$HPLChipconFIFO$readRXFIFO(len, (uint8_t *)CC2420RadioM$rxbufptr);
}

# 148 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/Clock.nc"
inline static   result_t TimerJiffyAsyncM$Timer$setIntervalAndScale(uint8_t arg_0xa419c48, uint8_t arg_0xa419d90){
#line 148
  unsigned char result;
#line 148

#line 148
  result = HPLTimer2$Timer2$setIntervalAndScale(arg_0xa419c48, arg_0xa419d90);
#line 148

#line 148
  return result;
#line 148
}
#line 148
static inline   
# 450 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/CC2420RadioM.nc"
result_t CC2420RadioM$HPLChipconFIFO$TXFIFODone(uint8_t length, uint8_t *data)
#line 450
{
  CC2420RadioM$tryToSend();
  return SUCCESS;
}

# 48 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/HPLCC2420FIFO.nc"
inline static   result_t HPLCC2420FIFOM$HPLCC2420FIFO$TXFIFODone(uint8_t arg_0xa566210, uint8_t *arg_0xa566370){
#line 48
  unsigned char result;
#line 48

#line 48
  result = CC2420RadioM$HPLChipconFIFO$TXFIFODone(arg_0xa566210, arg_0xa566370);
#line 48

#line 48
  return result;
#line 48
}
#line 48
static inline  
# 55 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/HPLCC2420FIFOM.nc"
void HPLCC2420FIFOM$signalTXdone(void)
#line 55
{
  HPLCC2420FIFOM$HPLCC2420FIFO$TXFIFODone(HPLCC2420FIFOM$txlength, HPLCC2420FIFOM$txbuf);
}

static inline   
#line 77
result_t HPLCC2420FIFOM$HPLCC2420FIFO$writeTXFIFO(uint8_t len, uint8_t *msg)
#line 77
{
  uint8_t i = 0;
  uint8_t status;



  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 83
    {
      HPLCC2420FIFOM$bSpiAvail = FALSE;
      HPLCC2420FIFOM$txlength = len;
      HPLCC2420FIFOM$txbuf = msg;
      TOSH_CLR_CC_CS_PIN();
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0F + 0x20) = 0x3E;
      while (!(* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0E + 0x20) & 0x80)) {
        }
#line 89
      ;
      status = * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0F + 0x20);
      for (i = 0; i < HPLCC2420FIFOM$txlength; i++) {
          * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0F + 0x20) = *HPLCC2420FIFOM$txbuf;
          HPLCC2420FIFOM$txbuf++;
          while (!(* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0E + 0x20) & 0x80)) {
            }
#line 94
          ;
        }
      HPLCC2420FIFOM$bSpiAvail = TRUE;
    }
#line 97
    __nesc_atomic_end(__nesc_atomic); }
  TOSH_SET_CC_CS_PIN();
  TOS_post(HPLCC2420FIFOM$signalTXdone);
  return status;
}

# 27 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/HPLCC2420FIFO.nc"
inline static   result_t CC2420RadioM$HPLChipconFIFO$writeTXFIFO(uint8_t arg_0xa56f4e8, uint8_t *arg_0xa56f648){
#line 27
  unsigned char result;
#line 27

#line 27
  result = HPLCC2420FIFOM$HPLCC2420FIFO$writeTXFIFO(arg_0xa56f4e8, arg_0xa56f648);
#line 27

#line 27
  return result;
#line 27
}
#line 27
static inline  
# 255 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/CC2420RadioM.nc"
void CC2420RadioM$startSend(void)
#line 255
{
  if (!CC2420RadioM$HPLChipcon$cmd(0x09)) {
      CC2420RadioM$sendFailed();
      return;
    }
  if (!CC2420RadioM$HPLChipconFIFO$writeTXFIFO(CC2420RadioM$txlength + 1, (uint8_t *)CC2420RadioM$txbufptr)) {
      CC2420RadioM$sendFailed();
      return;
    }
}

static inline   
#line 292
result_t CC2420RadioM$BackoffTimerJiffy$fired(void)
#line 292
{
  uint8_t currentstate;

#line 294
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 294
    currentstate = CC2420RadioM$stateRadio;
#line 294
    __nesc_atomic_end(__nesc_atomic); }

  switch (CC2420RadioM$stateTimer) {
      case CC2420RadioM$TIMER_INITIAL: 
        if (!TOS_post(CC2420RadioM$startSend)) {
            CC2420RadioM$sendFailed();
          }
      break;
      case CC2420RadioM$TIMER_BACKOFF: 
        CC2420RadioM$tryToSend();
      break;
      case CC2420RadioM$TIMER_ACK: 
        if (currentstate == CC2420RadioM$POST_TX_STATE) {
            CC2420RadioM$txbufptr->ack = 0;
            TOS_post(CC2420RadioM$PacketSent);
          }
      break;
    }
  return SUCCESS;
}

# 12 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/TimerJiffyAsync.nc"
inline static   result_t TimerJiffyAsyncM$TimerJiffyAsync$fired(void){
#line 12
  unsigned char result;
#line 12

#line 12
  result = CC2420RadioM$BackoffTimerJiffy$fired();
#line 12

#line 12
  return result;
#line 12
}
#line 12
static inline   
# 44 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/TimerJiffyAsyncM.nc"
result_t TimerJiffyAsyncM$Timer$fire(void)
#line 44
{
  uint16_t localjiffy;

#line 46
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 46
    localjiffy = TimerJiffyAsyncM$jiffy;
#line 46
    __nesc_atomic_end(__nesc_atomic); }
  if (localjiffy < 0xFF) {
      TimerJiffyAsyncM$Timer$intDisable();
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 49
        TimerJiffyAsyncM$bSet = FALSE;
#line 49
        __nesc_atomic_end(__nesc_atomic); }
      TimerJiffyAsyncM$TimerJiffyAsync$fired();
    }
  else {

      localjiffy = localjiffy >> 8;
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 55
        TimerJiffyAsyncM$jiffy = localjiffy;
#line 55
        __nesc_atomic_end(__nesc_atomic); }
      TimerJiffyAsyncM$Timer$setIntervalAndScale(localjiffy, 0x4);
    }
  return SUCCESS;
}

# 180 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/Clock.nc"
inline static   result_t HPLTimer2$Timer2$fire(void){
#line 180
  unsigned char result;
#line 180

#line 180
  result = TimerJiffyAsyncM$Timer$fire();
#line 180

#line 180
  return result;
#line 180
}
#line 180
static inline  
# 210 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/system/AMStandard.nc"
result_t AMStandard$RadioSend$sendDone(TOS_MsgPtr msg, result_t success)
#line 210
{
  return AMStandard$reportSendDone(msg, success);
}

# 67 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/BareSendMsg.nc"
inline static  result_t CC2420RadioM$Send$sendDone(TOS_MsgPtr arg_0xa524e88, result_t arg_0xa524fd8){
#line 67
  unsigned char result;
#line 67

#line 67
  result = AMStandard$RadioSend$sendDone(arg_0xa524e88, arg_0xa524fd8);
#line 67

#line 67
  return result;
#line 67
}
#line 67
static inline  
# 113 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/CC2420RadioM.nc"
void CC2420RadioM$sendDonetouplayer(void)
{
  CC2420RadioM$Send$sendDone(CC2420RadioM$txbufptr, FAIL);
}

# 161 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/hardware.h"
static __inline int TOSH_READ_RADIO_CCA_PIN(void)
#line 161
{
#line 161
  return (* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x10 + 0x20) & (1 << 6)) != 0;
}

# 63 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/interfaces/Random.nc"
inline static   uint16_t CC2420RadioM$Random$rand(void){
#line 63
  unsigned int result;
#line 63

#line 63
  result = RandomLFSR$Random$rand();
#line 63

#line 63
  return result;
#line 63
}
#line 63
static inline    
# 478 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/CC2420RadioM.nc"
int16_t CC2420RadioM$MacBackoff$default$congestionBackoff(TOS_MsgPtr m)
#line 478
{
  return (CC2420RadioM$Random$rand() & 0xF) + 1;
}

# 75 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/MacBackoff.nc"
inline static   int16_t CC2420RadioM$MacBackoff$congestionBackoff(TOS_MsgPtr arg_0xa543aa0){
#line 75
  int result;
#line 75

#line 75
  result = CC2420RadioM$MacBackoff$default$congestionBackoff(arg_0xa543aa0);
#line 75

#line 75
  return result;
#line 75
}
#line 75
# 6 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/TimerJiffyAsync.nc"
inline static   result_t CC2420RadioM$BackoffTimerJiffy$setOneShot(uint32_t arg_0xa567e68){
#line 6
  unsigned char result;
#line 6

#line 6
  result = TimerJiffyAsyncM$TimerJiffyAsync$setOneShot(arg_0xa567e68);
#line 6

#line 6
  return result;
#line 6
}
#line 6
static 
# 129 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/CC2420RadioM.nc"
__inline result_t CC2420RadioM$setBackoffTimer(uint16_t jiffy)
#line 129
{
  CC2420RadioM$stateTimer = CC2420RadioM$TIMER_BACKOFF;
  return CC2420RadioM$BackoffTimerJiffy$setOneShot(jiffy);
}

static __inline result_t CC2420RadioM$setAckTimer(uint16_t jiffy)
#line 134
{
  CC2420RadioM$stateTimer = CC2420RadioM$TIMER_ACK;
  return CC2420RadioM$BackoffTimerJiffy$setOneShot(jiffy);
}

# 158 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/hardware.h"
static __inline int TOSH_READ_CC_SFD_PIN(void)
#line 158
{
#line 158
  return (* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x10 + 0x20) & (1 << 4)) != 0;
}

static inline 
# 224 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/CC2420RadioM.nc"
void CC2420RadioM$sendPacket(void)
#line 224
{
  uint8_t status;

  CC2420RadioM$HPLChipcon$cmd(0x05);
  status = CC2420RadioM$HPLChipcon$cmd(0x00);


  if ((status >> 3) & 0x01) {
      TOSH_uwait(450);

      while (TOSH_READ_CC_SFD_PIN()) {
        }
#line 234
      ;


      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 237
        CC2420RadioM$stateRadio = CC2420RadioM$POST_TX_STATE;
#line 237
        __nesc_atomic_end(__nesc_atomic); }
      if (CC2420RadioM$bAckEnable) {
          if (!CC2420RadioM$setAckTimer(40)) {
            CC2420RadioM$sendFailed();
            }
        }
      else 
#line 242
        {
          if (!TOS_post(CC2420RadioM$PacketSent)) {
            CC2420RadioM$sendFailed();
            }
        }
    }
  else 
#line 247
    {
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 248
        CC2420RadioM$stateRadio = CC2420RadioM$PRE_TX_STATE;
#line 248
        __nesc_atomic_end(__nesc_atomic); }
      if (!CC2420RadioM$setBackoffTimer(CC2420RadioM$MacBackoff$congestionBackoff(CC2420RadioM$txbufptr) * 10)) {
          CC2420RadioM$sendFailed();
        }
    }
}

# 100 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/system/sched.c"
bool  TOS_post(void (*tp)(void))
#line 100
{
  __nesc_atomic_t fInterruptFlags;
  uint8_t tmp;



  fInterruptFlags = __nesc_atomic_start();

  tmp = TOSH_sched_free;

  if (TOSH_queue[tmp].tp == (void *)0) {
      TOSH_sched_free = (tmp + 1) & TOSH_TASK_BITMASK;
      TOSH_queue[tmp].tp = tp;
      __nesc_atomic_end(fInterruptFlags);

      return TRUE;
    }
  else {
      __nesc_atomic_end(fInterruptFlags);

      return FALSE;
    }
}

# 54 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/system/RealMain.nc"
int   main(void)
#line 54
{
  RealMain$hardwareInit();
  RealMain$Pot$init(10);
  TOSH_sched_init();

  RealMain$StdControl$init();
  RealMain$StdControl$start();
  __nesc_enable_interrupt();

  while (1) {
      TOSH_run_task();
    }
}

static  
# 72 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/system/TimerM.nc"
result_t TimerM$StdControl$init(void)
#line 72
{
  TimerM$mState = 0;
  TimerM$setIntervalFlag = 0;
  TimerM$queue_head = TimerM$queue_tail = -1;
  TimerM$queue_size = 0;
  TimerM$mScale = 3;
  TimerM$mInterval = TimerM$maxTimerInterval;
  return TimerM$Clock$setRate(TimerM$mInterval, TimerM$mScale);
}

static 
# 290 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/FramerM.nc"
void FramerM$HDLCInitialize(void)
#line 290
{
  int i;

#line 292
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 292
    {
      for (i = 0; i < FramerM$HDLC_QUEUESIZE; i++) {
          FramerM$gMsgRcvTbl[i].pMsg = &FramerM$gMsgRcvBuf[i];
          FramerM$gMsgRcvTbl[i].Length = 0;
          FramerM$gMsgRcvTbl[i].Token = 0;
        }
      FramerM$gTxState = FramerM$TXSTATE_IDLE;
      FramerM$gTxByteCnt = 0;
      FramerM$gTxLength = 0;
      FramerM$gTxRunningCRC = 0;
      FramerM$gpTxMsg = (void *)0;

      FramerM$gRxState = FramerM$RXSTATE_NOSYNC;
      FramerM$gRxHeadIndex = 0;
      FramerM$gRxTailIndex = 0;
      FramerM$gRxByteCnt = 0;
      FramerM$gRxRunningCRC = 0;
      FramerM$gpRxBuf = (uint8_t *)FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].pMsg;
    }
#line 310
    __nesc_atomic_end(__nesc_atomic); }
}

static   
# 150 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/HPLCC2420M.nc"
result_t HPLCC2420M$HPLCC2420$write(uint8_t addr, uint16_t data)
#line 150
{
  uint8_t status;



  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 155
    {
      HPLCC2420M$bSpiAvail = FALSE;
      TOSH_CLR_CC_CS_PIN();
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0F + 0x20) = addr;
      while (!(* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0E + 0x20) & 0x80)) {
        }
#line 159
      ;
      status = * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0F + 0x20);
      if (addr > 0x0E) {
          * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0F + 0x20) = data >> 8;
          while (!(* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0E + 0x20) & 0x80)) {
            }
#line 163
          ;
          * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0F + 0x20) = data & 0xff;
          while (!(* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0E + 0x20) & 0x80)) {
            }
#line 165
          ;
        }
      HPLCC2420M$bSpiAvail = TRUE;
    }
#line 168
    __nesc_atomic_end(__nesc_atomic); }
  TOSH_SET_CC_CS_PIN();
  return status;
}

static   
# 60 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/HPLUART0M.nc"
result_t HPLUART0M$UART$init(void)
#line 60
{





  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)0x90 = 0;
  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x09 + 0x20) = 15;


  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0B + 0x20) = 1 << 1;


  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)0x95 = (1 << 2) | (1 << 1);


  * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0A + 0x20) = (((1 << 7) | (1 << 6)) | (1 << 4)) | (1 << 3);


  return SUCCESS;
}

static   
# 125 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/HPLCC2420M.nc"
uint8_t HPLCC2420M$HPLCC2420$cmd(uint8_t addr)
#line 125
{
  uint8_t status;

  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 128
    {
      TOSH_CLR_CC_CS_PIN();
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0F + 0x20) = addr;
      while (!(* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0E + 0x20) & 0x80)) {
        }
#line 131
      ;
      status = * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0F + 0x20);
    }
#line 133
    __nesc_atomic_end(__nesc_atomic); }
  TOSH_SET_CC_CS_PIN();
  return status;
}

static   
#line 181
uint16_t HPLCC2420M$HPLCC2420$read(uint8_t addr)
#line 181
{

  uint16_t data = 0;
  uint8_t status;


  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 187
    {
      HPLCC2420M$bSpiAvail = FALSE;
      TOSH_CLR_CC_CS_PIN();
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0F + 0x20) = addr | 0x40;
      while (!(* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0E + 0x20) & 0x80)) {
        }
#line 191
      ;
      status = * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0F + 0x20);
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0F + 0x20) = 0;
      while (!(* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0E + 0x20) & 0x80)) {
        }
#line 194
      ;
      data = * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0F + 0x20);
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0F + 0x20) = 0;
      while (!(* (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0E + 0x20) & 0x80)) {
        }
#line 197
      ;
      data = (data << 8) | * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x0F + 0x20);
      TOSH_SET_CC_CS_PIN();
      HPLCC2420M$bSpiAvail = TRUE;
    }
#line 201
    __nesc_atomic_end(__nesc_atomic); }
  return data;
}

static   
# 101 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/HPLPowerManagementM.nc"
uint8_t HPLPowerManagementM$PowerManagement$adjustPower(void)
#line 101
{
  uint8_t mcu;

#line 103
  if (!HPLPowerManagementM$disabled) {
    TOS_post(HPLPowerManagementM$doAdjustment);
    }
  else 
#line 105
    {
      mcu = * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x35 + 0x20);
      mcu &= 0xe3;
      mcu |= HPLPowerManagementM$IDLE;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x35 + 0x20) = mcu;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x35 + 0x20) |= 1 << 5;
    }
  return 0;
}

# 210 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/HPLClock.nc"
void __attribute((signal))   __vector_15(void)
#line 210
{
  if (HPLClock$set_flag) {
      HPLClock$mscale = HPLClock$nextScale;
      HPLClock$nextScale |= 0x8;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x33 + 0x20) = HPLClock$nextScale;

      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x31 + 0x20) = HPLClock$minterval;
      HPLClock$set_flag = 0;
    }
  HPLClock$systime += * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x31 + 0x20);
  HPLClock$systime += 1;
  HPLClock$Clock$fire();
}

static   
# 162 "RFID_ControlM.nc"
result_t RFID_ControlM$UART$get(uint8_t data)
#line 162
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 163
    {
      if (RFID_ControlM$RBuff_Index == 0) {
          if (data == 0x02) {
              RFID_ControlM$RecvBuff_From_RFID[RFID_ControlM$RBuff_Index++] = data;
            }
        }
      else {
          if (RFID_ControlM$RBuff_Index == 1) {
            RFID_ControlM$CheckSum_Index = data;
            }
          RFID_ControlM$RecvBuff_From_RFID[RFID_ControlM$RBuff_Index++] = data;


          if (RFID_ControlM$CheckSum_Index + 2 == RFID_ControlM$RBuff_Index) {
            RFID_ControlM$processing_recvData();
            }
        }
    }
#line 180
    __nesc_atomic_end(__nesc_atomic); }
#line 180
  return SUCCESS;
}

static 
#line 358
void RFID_ControlM$UARTSend(uint8_t *data, uint8_t len)
{
  uint8_t i;

#line 361
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 361
    {
      if (RFID_ControlM$buff_start > 128 - 1) {
        RFID_ControlM$buff_start = 0;
        }
      for (i = 0; i < len; i++) 
        {
          if (RFID_ControlM$buff_end > 128 - 1) {
            RFID_ControlM$buff_end = 0;
            }
#line 369
          RFID_ControlM$str_buff[RFID_ControlM$buff_end] = data[i];
          RFID_ControlM$buff_end++;
        }

      if (RFID_ControlM$state == 0) 
        {
          RFID_ControlM$state = 1;
          RFID_ControlM$UART$put(RFID_ControlM$str_buff[RFID_ControlM$buff_start]);
        }
    }
#line 378
    __nesc_atomic_end(__nesc_atomic); }
}

static   
# 125 "testRFIDM.nc"
void testRFIDM$RFID_Control$GetID_14443A_Done(char status, uint8_t *buff, char size)
#line 125
{

  if (status == 0) {
      char i;

      sprintf(testRFIDM$OutputUartMsg, "Recv 14443A ID: [");
      testRFIDM$SCSuartDBG$UARTSend(testRFIDM$OutputUartMsg, strlen(testRFIDM$OutputUartMsg));

      for (i = 0; i < 5; i++) {
          sprintf(testRFIDM$OutputUartMsg, "%X ", buff[i]);
          testRFIDM$SCSuartDBG$UARTSend(testRFIDM$OutputUartMsg, strlen(testRFIDM$OutputUartMsg));
        }

      sprintf(testRFIDM$OutputUartMsg, "]\r\n");
      testRFIDM$SCSuartDBG$UARTSend(testRFIDM$OutputUartMsg, strlen(testRFIDM$OutputUartMsg));
    }
  else {
      sprintf(testRFIDM$OutputUartMsg, "14443A GetID Error: %d\r\n", status);
      testRFIDM$SCSuartDBG$UARTSend(testRFIDM$OutputUartMsg, strlen(testRFIDM$OutputUartMsg));
    }
}

static   
# 94 "SCSuartDBGM.nc"
void SCSuartDBGM$SCSuartDBG$UARTSend(uint8_t *data, uint8_t len)
{
  uint8_t i;

#line 97
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 97
    {
      if (SCSuartDBGM$buff_start > 128 - 1) {
        SCSuartDBGM$buff_start = 0;
        }
      for (i = 0; i < len; i++) 
        {
          if (SCSuartDBGM$buff_end > 128 - 1) {
            SCSuartDBGM$buff_end = 0;
            }
#line 105
          SCSuartDBGM$str_buff[SCSuartDBGM$buff_end] = data[i];
          SCSuartDBGM$buff_end++;
        }

      if (SCSuartDBGM$state == 0) 
        {
          SCSuartDBGM$state = 1;
          SCSuartDBGM$HPLUART$put(SCSuartDBGM$str_buff[SCSuartDBGM$buff_start]);
        }
    }
#line 114
    __nesc_atomic_end(__nesc_atomic); }
}

static   
# 148 "testRFIDM.nc"
void testRFIDM$RFID_Control$GetID_15693_Done(char status, uint8_t *buff, char size)
#line 148
{

  if (status == 0) {
      char i;
#line 151
      char j;

#line 152
      for (j = 0; j < size; j++) 
        {

          testRFIDM$SCSuartDBG$UARTSend(testRFIDM$OutputUartMsg, strlen(testRFIDM$OutputUartMsg));

          for (i = j * 8; i < j * 8 + 8; i++) {
              sprintf(testRFIDM$OutputUartMsg, "%X ", buff[i]);
              testRFIDM$SCSuartDBG$UARTSend(testRFIDM$OutputUartMsg, strlen(testRFIDM$OutputUartMsg));
            }

          sprintf(testRFIDM$OutputUartMsg, "\r\n");
          testRFIDM$SCSuartDBG$UARTSend(testRFIDM$OutputUartMsg, strlen(testRFIDM$OutputUartMsg));
        }
    }
}

static   



void testRFIDM$RFID_Control$RData_15693_Done(char status, uint8_t *buff, char size)
#line 172
{

  if (status == 0) {
      char i;

      sprintf(testRFIDM$OutputUartMsg, "Read data from 15693: Data[");
      testRFIDM$SCSuartDBG$UARTSend(testRFIDM$OutputUartMsg, strlen(testRFIDM$OutputUartMsg));

      for (i = 1; i < 5; i++) {
          sprintf(testRFIDM$OutputUartMsg, "%X ", buff[i]);
          testRFIDM$SCSuartDBG$UARTSend(testRFIDM$OutputUartMsg, strlen(testRFIDM$OutputUartMsg));
        }

      sprintf(testRFIDM$OutputUartMsg, "]\r\n");
      testRFIDM$SCSuartDBG$UARTSend(testRFIDM$OutputUartMsg, strlen(testRFIDM$OutputUartMsg));
    }
}

static   




void testRFIDM$RFID_Control$WData_15693_Done(char status)
#line 195
{

  if (status == 0) {
      sprintf(testRFIDM$OutputUartMsg, "Write data to 15693 SUCCESS!!!\r\n");
      testRFIDM$SCSuartDBG$UARTSend(testRFIDM$OutputUartMsg, strlen(testRFIDM$OutputUartMsg));
    }
  else 
#line 200
    {
      sprintf(testRFIDM$OutputUartMsg, "Write data to 15693 Error: %d\r\n", status);
      testRFIDM$SCSuartDBG$UARTSend(testRFIDM$OutputUartMsg, strlen(testRFIDM$OutputUartMsg));
    }
}

static   
# 336 "RFID_ControlM.nc"
result_t RFID_ControlM$UART$putDone(void)
#line 336
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 337
    {
      RFID_ControlM$buff_start++;

      if (RFID_ControlM$buff_start > 128 - 1) {
        RFID_ControlM$buff_start = 0;
        }
      if (RFID_ControlM$buff_end > 128 - 1) {
        RFID_ControlM$buff_end = 0;
        }
      if (RFID_ControlM$buff_start != RFID_ControlM$buff_end) 
        {
          RFID_ControlM$UART$put(RFID_ControlM$str_buff[RFID_ControlM$buff_start]);
        }
      else 
        {
          RFID_ControlM$state = 0;
        }
    }
#line 354
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

static   
# 370 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/FramerM.nc"
result_t FramerM$ByteComm$rxByteReady(uint8_t data, bool error, uint16_t strength)
#line 370
{

  switch (FramerM$gRxState) {

      case FramerM$RXSTATE_NOSYNC: 
        if (data == FramerM$HDLC_FLAG_BYTE && FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].Length == 0) {
            FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].Token = 0;
            FramerM$gRxByteCnt = FramerM$gRxRunningCRC = 0;
            FramerM$gpRxBuf = (uint8_t *)FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].pMsg;
            FramerM$gRxState = FramerM$RXSTATE_PROTO;
          }
      break;

      case FramerM$RXSTATE_PROTO: 
        if (data == FramerM$HDLC_FLAG_BYTE) {
            break;
          }
      FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].Proto = data;
      FramerM$gRxRunningCRC = crcByte(FramerM$gRxRunningCRC, data);
      switch (data) {
          case FramerM$PROTO_PACKET_ACK: 
            FramerM$gRxState = FramerM$RXSTATE_TOKEN;
          break;
          case FramerM$PROTO_PACKET_NOACK: 
            FramerM$gRxState = FramerM$RXSTATE_INFO;
          break;
          default: 
            FramerM$gRxState = FramerM$RXSTATE_NOSYNC;
          break;
        }
      break;

      case FramerM$RXSTATE_TOKEN: 
        if (data == FramerM$HDLC_FLAG_BYTE) {
            FramerM$gRxState = FramerM$RXSTATE_NOSYNC;
          }
        else {
#line 406
          if (data == FramerM$HDLC_CTLESC_BYTE) {
              FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].Token = 0x20;
            }
          else {
              FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].Token ^= data;
              FramerM$gRxRunningCRC = crcByte(FramerM$gRxRunningCRC, FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].Token);
              FramerM$gRxState = FramerM$RXSTATE_INFO;
            }
          }
#line 414
      break;


      case FramerM$RXSTATE_INFO: 
        if (FramerM$gRxByteCnt > FramerM$HDLC_MTU) {
            FramerM$gRxByteCnt = FramerM$gRxRunningCRC = 0;
            FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].Length = 0;
            FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].Token = 0;
            FramerM$gRxState = FramerM$RXSTATE_NOSYNC;
          }
        else {
#line 424
          if (data == FramerM$HDLC_CTLESC_BYTE) {
              FramerM$gRxState = FramerM$RXSTATE_ESC;
            }
          else {
#line 427
            if (data == FramerM$HDLC_FLAG_BYTE) {
                if (FramerM$gRxByteCnt >= 2) {

                    uint16_t usRcvdCRC = FramerM$gpRxBuf[FramerM$fRemapRxPos(FramerM$gRxByteCnt - 1)] & 0xff;

#line 431
                    usRcvdCRC = (usRcvdCRC << 8) | FramerM$fRemapRxPos(FramerM$gpRxBuf[FramerM$gRxByteCnt - 2] & 0xff);

                    FramerM$gRxRunningCRC = usRcvdCRC;

                    if (usRcvdCRC == FramerM$gRxRunningCRC) {
                        FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].Length = FramerM$gRxByteCnt - 2;
                        TOS_post(FramerM$PacketRcvd);
                        FramerM$gRxHeadIndex++;
#line 438
                        FramerM$gRxHeadIndex %= FramerM$HDLC_QUEUESIZE;
                      }
                    else {
                        FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].Length = 0;
                        FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].Token = 0;
                      }
                    if (FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].Length == 0) {
                        FramerM$gpRxBuf = (uint8_t *)FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].pMsg;
                        FramerM$gRxState = FramerM$RXSTATE_PROTO;
                      }
                    else {
                        FramerM$gRxState = FramerM$RXSTATE_NOSYNC;
                      }
                  }
                else {
                    FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].Length = 0;
                    FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].Token = 0;
                    FramerM$gRxState = FramerM$RXSTATE_NOSYNC;
                  }
                FramerM$gRxByteCnt = FramerM$gRxRunningCRC = 0;
              }
            else {
                FramerM$gpRxBuf[FramerM$fRemapRxPos(FramerM$gRxByteCnt)] = data;
                if (FramerM$gRxByteCnt >= 2) {
                    FramerM$gRxRunningCRC = crcByte(FramerM$gRxRunningCRC, FramerM$gpRxBuf[FramerM$gRxByteCnt - 2]);
                  }
                FramerM$gRxByteCnt++;
              }
            }
          }
#line 466
      break;

      case FramerM$RXSTATE_ESC: 
        if (data == FramerM$HDLC_FLAG_BYTE) {

            FramerM$gRxByteCnt = FramerM$gRxRunningCRC = 0;
            FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].Length = 0;
            FramerM$gMsgRcvTbl[FramerM$gRxHeadIndex].Token = 0;
            FramerM$gRxState = FramerM$RXSTATE_NOSYNC;
          }
        else {
            data = data ^ 0x20;
            FramerM$gpRxBuf[FramerM$fRemapRxPos(FramerM$gRxByteCnt)] = data;
            if (FramerM$gRxByteCnt >= 2) {
                FramerM$gRxRunningCRC = crcByte(FramerM$gRxRunningCRC, FramerM$gpRxBuf[FramerM$gRxByteCnt - 2]);
              }
            FramerM$gRxByteCnt++;
            FramerM$gRxState = FramerM$RXSTATE_INFO;
          }
      break;

      default: 
        FramerM$gRxState = FramerM$RXSTATE_NOSYNC;
      break;
    }

  return SUCCESS;
}

static 
#line 166
uint8_t FramerM$fRemapRxPos(uint8_t InPos)
#line 166
{

  if (InPos < 4) {
    return InPos + (size_t )& ((struct TOS_Msg *)0)->addr;
    }
  else {
#line 170
    if (InPos == 4) {
      return (size_t )& ((struct TOS_Msg *)0)->length;
      }
    else {
#line 172
      return InPos + (size_t )& ((struct TOS_Msg *)0)->data - 5;
      }
    }
}

static 
#line 176
result_t FramerM$StartTx(void)
#line 176
{
  result_t Result = SUCCESS;
  bool fInitiate = FALSE;

  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 180
    {
      if (FramerM$gTxState == FramerM$TXSTATE_IDLE) {
          if (FramerM$gFlags & FramerM$FLAGS_TOKENPEND) {
              FramerM$gpTxBuf = (uint8_t *)&FramerM$gTxTokenBuf;
              FramerM$gTxProto = FramerM$PROTO_ACK;
              FramerM$gTxLength = sizeof FramerM$gTxTokenBuf;
              fInitiate = TRUE;
              FramerM$gTxState = FramerM$TXSTATE_PROTO;
            }
          else {
#line 189
            if (FramerM$gFlags & FramerM$FLAGS_DATAPEND) {
                FramerM$gpTxBuf = (uint8_t *)FramerM$gpTxMsg;
                FramerM$gTxProto = FramerM$PROTO_PACKET_NOACK;


                FramerM$gTxLength = FramerM$gpTxMsg->length + TOS_HEADER_SIZE + 2 + 3;
                fInitiate = TRUE;
                FramerM$gTxState = FramerM$TXSTATE_PROTO;
              }
            else {
#line 198
              if (FramerM$gFlags & FramerM$FLAGS_UNKNOWN) {
                  FramerM$gpTxBuf = (uint8_t *)&FramerM$gTxUnknownBuf;
                  FramerM$gTxProto = FramerM$PROTO_UNKNOWN;
                  FramerM$gTxLength = sizeof FramerM$gTxUnknownBuf;
                  fInitiate = TRUE;
                  FramerM$gTxState = FramerM$TXSTATE_PROTO;
                }
              }
            }
        }
    }
#line 208
    __nesc_atomic_end(__nesc_atomic); }
#line 208
  if (fInitiate) {
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 209
        {
          FramerM$gTxRunningCRC = 0;

          FramerM$gTxByteCnt = (size_t )& ((struct TOS_Msg *)0)->addr;
        }
#line 213
        __nesc_atomic_end(__nesc_atomic); }
      Result = FramerM$ByteComm$txByte(FramerM$HDLC_FLAG_BYTE);
      if (Result != SUCCESS) {
          { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 216
            FramerM$gTxState = FramerM$TXSTATE_ERROR;
#line 216
            __nesc_atomic_end(__nesc_atomic); }
          TOS_post(FramerM$PacketSent);
        }
    }

  return Result;
}

static   
# 110 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/system/UARTM.nc"
result_t UARTM$ByteComm$txByte(uint8_t data)
#line 110
{
  bool oldState;

  {
  }
#line 113
  ;

  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 115
    {
      oldState = UARTM$state;
      UARTM$state = TRUE;
    }
#line 118
    __nesc_atomic_end(__nesc_atomic); }
  if (oldState) {
    return FAIL;
    }
  UARTM$HPLUART$put(data);

  return SUCCESS;
}

static  
# 268 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/FramerM.nc"
void FramerM$PacketSent(void)
#line 268
{
  result_t TxResult = SUCCESS;

  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 271
    {
      if (FramerM$gTxState == FramerM$TXSTATE_ERROR) {
          TxResult = FAIL;
          FramerM$gTxState = FramerM$TXSTATE_IDLE;
        }
    }
#line 276
    __nesc_atomic_end(__nesc_atomic); }
  if (FramerM$gTxProto == FramerM$PROTO_ACK) {
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 278
        FramerM$gFlags ^= FramerM$FLAGS_TOKENPEND;
#line 278
        __nesc_atomic_end(__nesc_atomic); }
    }
  else {
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 281
        FramerM$gFlags ^= FramerM$FLAGS_DATAPEND;
#line 281
        __nesc_atomic_end(__nesc_atomic); }
      FramerM$BareSendMsg$sendDone((TOS_MsgPtr )FramerM$gpTxMsg, TxResult);
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 283
        FramerM$gpTxMsg = (void *)0;
#line 283
        __nesc_atomic_end(__nesc_atomic); }
    }


  FramerM$StartTx();
}

static 
# 143 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/system/AMStandard.nc"
result_t AMStandard$reportSendDone(TOS_MsgPtr msg, result_t success)
#line 143
{
  AMStandard$state = FALSE;
  AMStandard$SendMsg$sendDone(msg->type, msg, success);
  AMStandard$sendDone();

  return SUCCESS;
}

#line 215
TOS_MsgPtr   received(TOS_MsgPtr packet)
#line 215
{
  uint16_t addr = TOS_LOCAL_ADDRESS;

#line 217
  AMStandard$counter++;
  {
  }
#line 218
  ;


  if (
#line 220
  packet->crc == 1 && 
  packet->group == TOS_AM_GROUP && (
  packet->addr == TOS_BCAST_ADDR || 
  packet->addr == addr)) 
    {

      uint8_t type = packet->type;
      TOS_MsgPtr tmp;

      {
      }
#line 229
      ;
      AMStandard$dbgPacket(packet);
      {
      }
#line 231
      ;


      tmp = AMStandard$ReceiveMsg$receive(type, packet);
      if (tmp) {
        packet = tmp;
        }
    }
#line 238
  return packet;
}

static   
# 119 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/system/LedsC.nc"
result_t LedsC$Leds$greenToggle(void)
#line 119
{
  result_t rval;

#line 121
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 121
    {
      if (LedsC$ledsOn & LedsC$GREEN_BIT) {
        rval = LedsC$Leds$greenOff();
        }
      else {
#line 125
        rval = LedsC$Leds$greenOn();
        }
    }
#line 127
    __nesc_atomic_end(__nesc_atomic); }
#line 127
  return rval;
}

static 
# 81 "testRFIDM.nc"
void testRFIDM$Control_RFID(uint8_t comm, uint8_t block, uint8_t *buff)
#line 81
{
  if (comm == 1) {
      testRFIDM$RFID_Control$GetID_14443A();
    }
  else {
#line 85
    if (comm == 2) {
        testRFIDM$RFID_Control$GetID_15693();
      }
    else {
#line 88
      if (comm == 3) {
          testRFIDM$RFID_Control$RData_15693(block);
        }
      else {
#line 91
        if (comm == 4) {
            testRFIDM$RFID_Control$WData_15693(block, buff, 4);
          }
        }
      }
    }
}

static 
# 383 "RFID_ControlM.nc"
void RFID_ControlM$gen15693crc(uint8_t *data, uint8_t len, uint8_t *byte1, uint8_t *byte2)
#line 383
{
  const uint16_t POLYNOMIAL = 0x8408;
  const uint16_t PRESET_VALUE = 0xFFFF;

  uint16_t current_crc_value = PRESET_VALUE;

  int i;
#line 389
  int j;

#line 390
  for (i = 0; i < len; i++) {
      current_crc_value = current_crc_value ^ (unsigned int )data[i];

      for (j = 0; j < 8; j++) {
          if (current_crc_value & 0x0001) {
              current_crc_value = (current_crc_value >> 1) ^ POLYNOMIAL;
            }
          else 
#line 396
            {
              current_crc_value = current_crc_value >> 1;
            }
        }
    }
  current_crc_value = ~current_crc_value & 0xFFFF;

  *byte1 = (uint8_t )(current_crc_value & 0xFF);
  *byte2 = (uint8_t )(current_crc_value >> 8);
}

static   
# 101 "testRFIDM.nc"
void testRFIDM$SCSuartDBGRecv$UARTRecv(uint8_t recv_Char)
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 103
    {
      if (recv_Char == 0x7E && testRFIDM$start_flag == 0) 
        {
          testRFIDM$recv_num = 0;
          testRFIDM$start_flag = 1;
        }
      else {
#line 109
        if (testRFIDM$recv_num < 8 && testRFIDM$start_flag == 1) {
            testRFIDM$RecvBuff[testRFIDM$recv_num] = recv_Char;
            testRFIDM$recv_num++;

            if (testRFIDM$recv_num == 8) {
                struct RFID_COMM_MSG *pack = (struct RFID_COMM_MSG *)testRFIDM$RecvBuff;

#line 115
                testRFIDM$start_flag = 0;
                testRFIDM$Leds$greenToggle();


                testRFIDM$Control_RFID(pack->comm, pack->block, pack->wbuff);
              }
          }
        }
    }
#line 123
    __nesc_atomic_end(__nesc_atomic); }
}

static   
# 87 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/system/UARTM.nc"
result_t UARTM$HPLUART$putDone(void)
#line 87
{
  bool oldState;

  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 90
    {
      {
      }
#line 91
      ;
      oldState = UARTM$state;
      UARTM$state = FALSE;
    }
#line 94
    __nesc_atomic_end(__nesc_atomic); }








  if (oldState) {
      UARTM$ByteComm$txDone();
      UARTM$ByteComm$txByteReady(TRUE);
    }
  return SUCCESS;
}

static 
# 495 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/FramerM.nc"
result_t FramerM$TxArbitraryByte(uint8_t inByte)
#line 495
{
  if (inByte == FramerM$HDLC_FLAG_BYTE || inByte == FramerM$HDLC_CTLESC_BYTE) {
      { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 497
        {
          FramerM$gPrevTxState = FramerM$gTxState;
          FramerM$gTxState = FramerM$TXSTATE_ESC;
          FramerM$gTxEscByte = inByte;
        }
#line 501
        __nesc_atomic_end(__nesc_atomic); }
      inByte = FramerM$HDLC_CTLESC_BYTE;
    }

  return FramerM$ByteComm$txByte(inByte);
}

static   
# 73 "SCSuartDBGM.nc"
result_t SCSuartDBGM$HPLUART$putDone(void)
#line 73
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 74
    {
      SCSuartDBGM$buff_start++;

      if (SCSuartDBGM$buff_start > 128 - 1) {
        SCSuartDBGM$buff_start = 0;
        }
      if (SCSuartDBGM$buff_end > 128 - 1) {
        SCSuartDBGM$buff_end = 0;
        }
      if (SCSuartDBGM$buff_start != SCSuartDBGM$buff_end) 
        {
          SCSuartDBGM$HPLUART$put(SCSuartDBGM$str_buff[SCSuartDBGM$buff_start]);
        }
      else {
          SCSuartDBGM$state = 0;
        }
    }
#line 90
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

static   
# 379 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/CC2420RadioM.nc"
result_t CC2420RadioM$HPLChipcon$FIFOPIntr(void)
#line 379
{




  if (CC2420RadioM$bAckEnable && CC2420RadioM$stateRadio == CC2420RadioM$PRE_TX_STATE) {
      if (CC2420RadioM$BackoffTimerJiffy$isSet()) {
          CC2420RadioM$BackoffTimerJiffy$stop();
          CC2420RadioM$BackoffTimerJiffy$setOneShot(CC2420RadioM$MacBackoff$congestionBackoff(CC2420RadioM$txbufptr) * 10 + 40);
        }
    }



  if (!TOSH_READ_CC_FIFO_PIN()) {
      CC2420RadioM$HPLChipcon$read(0x3F);
      CC2420RadioM$HPLChipcon$cmd(0x08);
      return SUCCESS;
    }

  TOS_post(CC2420RadioM$delayedRXFIFO);

  return SUCCESS;
}

static   
# 61 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/TimerJiffyAsyncM.nc"
result_t TimerJiffyAsyncM$TimerJiffyAsync$setOneShot(uint32_t _jiffy)
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 63
    {
      TimerJiffyAsyncM$jiffy = _jiffy;
      TimerJiffyAsyncM$bSet = TRUE;
    }
#line 66
    __nesc_atomic_end(__nesc_atomic); }
  if (_jiffy > 0xFF) {
      TimerJiffyAsyncM$Timer$setIntervalAndScale(0xFF, 0x4);
    }
  else {
      TimerJiffyAsyncM$Timer$setIntervalAndScale(_jiffy, 0x4);
    }
  return SUCCESS;
}

static   
# 118 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/HPLTimer2.nc"
result_t HPLTimer2$Timer2$setIntervalAndScale(uint8_t interval, uint8_t scale)
#line 118
{

  if (scale > 7) {
#line 120
    return FAIL;
    }
#line 121
  scale |= 0x8;
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 122
    {
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x25 + 0x20) = 0;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x37 + 0x20) &= ~(1 << 7);
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x37 + 0x20) &= ~(1 << 6);
      HPLTimer2$mscale = scale;
      HPLTimer2$minterval = interval;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x24 + 0x20) = 0;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x23 + 0x20) = interval;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x36 + 0x20) |= 1 << 7;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x37 + 0x20) |= 1 << 7;
      * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x25 + 0x20) = scale;
    }
#line 133
    __nesc_atomic_end(__nesc_atomic); }
  return SUCCESS;
}

static   
# 70 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/system/RandomLFSR.nc"
uint16_t RandomLFSR$Random$rand(void)
#line 70
{
  bool endbit;
  uint16_t tmpShiftReg;

#line 73
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 73
    {
      tmpShiftReg = RandomLFSR$shiftReg;
      endbit = (tmpShiftReg & 0x8000) != 0;
      tmpShiftReg <<= 1;
      if (endbit) {
        tmpShiftReg ^= 0x100b;
        }
#line 79
      tmpShiftReg++;
      RandomLFSR$shiftReg = tmpShiftReg;
      tmpShiftReg = tmpShiftReg ^ RandomLFSR$mask;
    }
#line 82
    __nesc_atomic_end(__nesc_atomic); }
  return tmpShiftReg;
}

static  
# 158 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/CC2420RadioM.nc"
void CC2420RadioM$PacketSent(void)
#line 158
{
  TOS_MsgPtr pBuf;

  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 161
    {
      CC2420RadioM$stateRadio = CC2420RadioM$IDLE_STATE;
      CC2420RadioM$txbufptr->time = 0;
      pBuf = CC2420RadioM$txbufptr;
      pBuf->length = pBuf->length - MSG_HEADER_SIZE - MSG_FOOTER_SIZE;
    }
#line 166
    __nesc_atomic_end(__nesc_atomic); }

  CC2420RadioM$Send$sendDone(pBuf, SUCCESS);
}

# 172 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/platform/zigbex/HPLTimer2.nc"
void __attribute((interrupt))   __vector_9(void)
#line 172
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 173
    {
      if (HPLTimer2$set_flag) {
          HPLTimer2$mscale = HPLTimer2$nextScale;
          HPLTimer2$nextScale |= 0x8;
          * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x33 + 0x20) = HPLTimer2$nextScale;
          * (volatile unsigned char *)(unsigned int )& * (volatile unsigned char *)(0x31 + 0x20) = HPLTimer2$minterval;
          HPLTimer2$set_flag = 0;
        }
    }
#line 181
    __nesc_atomic_end(__nesc_atomic); }
  HPLTimer2$Timer2$fire();
}

static 
# 118 "C:/PROGRA~1/UCB/cygwin/opt/tinyos-1.x/tos/lib/CC2420Radio/CC2420RadioM.nc"
void CC2420RadioM$sendFailed(void)
#line 118
{
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 119
    CC2420RadioM$stateRadio = CC2420RadioM$IDLE_STATE;
#line 119
    __nesc_atomic_end(__nesc_atomic); }
  CC2420RadioM$txbufptr->length = CC2420RadioM$txbufptr->length - MSG_HEADER_SIZE - MSG_FOOTER_SIZE;
  TOS_post(CC2420RadioM$sendDonetouplayer);
}

static 
#line 266
void CC2420RadioM$tryToSend(void)
#line 266
{
  uint8_t currentstate;

#line 268
  { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 268
    currentstate = CC2420RadioM$stateRadio;
#line 268
    __nesc_atomic_end(__nesc_atomic); }


  if (currentstate == CC2420RadioM$PRE_TX_STATE) {
      if (TOSH_READ_RADIO_CCA_PIN()) {
          { __nesc_atomic_t __nesc_atomic = __nesc_atomic_start();
#line 273
            CC2420RadioM$stateRadio = CC2420RadioM$TX_STATE;
#line 273
            __nesc_atomic_end(__nesc_atomic); }
          CC2420RadioM$sendPacket();
        }
      else {
          if (CC2420RadioM$countRetry-- <= 0) {
              CC2420RadioM$HPLChipcon$read(0x3F);
              CC2420RadioM$HPLChipcon$cmd(0x08);
              CC2420RadioM$HPLChipcon$read(0x3F);
              CC2420RadioM$HPLChipcon$cmd(0x08);
              CC2420RadioM$sendFailed();
              return;
            }
          if (!CC2420RadioM$setBackoffTimer(CC2420RadioM$MacBackoff$congestionBackoff(CC2420RadioM$txbufptr) * 10)) {
              CC2420RadioM$sendFailed();
            }
        }
    }
}

