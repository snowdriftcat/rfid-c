
#ifndef RFID_CONTROL_H
#define RFID_CONTROL_H

#include <stdio.h>

#define RFID_SUCCESS 0
#define RFID_FAIL 1
#define RFID_START_SUCCESS 2
#define RFID_START_FAIL 3
#define RFID_MEM_FAIL 4
#define RFID_BUSY 5


#define RFID14443A_UID_INDEX 6
#define RFID14443A_UID_SIZE	15

#define RFID15693_UID_INDEX	10
#define RFID15693_UID_SIZE	8

#define RFID_ACK_INDEX	3
#define RFID_ACK_SUCC	0

#define RFID15693_Read_Block_Index	6

struct RFID_COMM_MSG
{
	uint8_t comm;
	uint8_t block;
	uint8_t reserved1; 
	uint8_t reserved2; 
	uint8_t wbuff[4];
};

#endif
